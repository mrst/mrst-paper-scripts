%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

mrstModule add mimetic
mrstModule add mpfa
mrstModule add streamlines
mrstModule add ad-fi
mrstModule add dfm
mrstModule add ad-props

% new ad-fi
mrstModule add ad-core
mrstModule add ad-blackoil
%
mrstModule add libgeometry 
mrstModule add coarsegrid; % for processPartition
mrstModule add co2lab; % to read formation grids
gravity on;
