function [faces, nodes] = boundaryFacesAndNodes(G, sides)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   if isfield(G, 'cartDims')
      [faces, nodes] = boundary_faces_and_nodes_cartesian(G, sides);
   else
      [faces, nodes] = boundary_faces_and_nodes_general(G, sides);
   end

end


% ----------------------------------------------------------------------------

function [faces, nodes] = boundary_faces_and_nodes_cartesian(G, sides)   
   side_labels = {'xmin', 'xmax', 'ymin', 'ymax', 'zmin', 'zmax'};
   faces = [];
   inodes = [];
   for s = side_labels(logical(sides))
      bc = pside([], G, s{:}, 0);
      if isempty(bc)
         continue;
      end
      faces = [faces; bc.face];%#ok
      inodes = [inodes; mcolon(G.faces.nodePos(bc.face), ...
                               G.faces.nodePos(bc.face+1)-1)']; %#ok
   end
   nodes = unique(G.faces.nodes(inodes));
end

% ----------------------------------------------------------------------------

function [faces, nodes] = boundary_faces_and_nodes_general(G, sides)   

   faces = [];
   inodes = [];
   for s = find(sides)
      
      face = G.cells.faces(:, 2) == s;
      face = G.cells.faces(face, 1);
      face = find(prod(G.faces.neighbors(face, :), 2) == 0);
      
      faces = [faces; face];%#ok
      inodes = [inodes; mcolon(G.faces.nodePos(face), ...
                               G.faces.nodePos(face+1)-1)']; %#ok
   end
   nodes = unique(G.faces.nodes(inodes));
end