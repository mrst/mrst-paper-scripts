function w  = createGridMappings(g)

%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
   %% Create mapping from sub-half-face to cell, node, face, half-face and
   %% sub-face
   cellno   = rldecode(1:g.cells.num, diff(g.cells.facePos), 2) .';
   col      = 1+(cellno == g.faces.neighbors(g.cells.faces(:,1), 2));
   nhfaces  = g.cells.facePos(end)-1;
   hfaces   = accumarray([g.cells.faces(:,1), col], 1:nhfaces);
   hfaces   = rldecode(hfaces, diff(g.faces.nodePos));

   
   cells    =  rldecode(g.faces.neighbors, diff(g.faces.nodePos));
   nodes    =  repmat(g.faces.nodes, [2,1]);
   faces    =  repmat(rldecode(1:g.faces.num, diff(g.faces.nodePos),2)', [2,1]);
   %subfaces =  repmat((1:size(g.faces.nodes,1))', [2,1]);
   i        =  cells~=0;
   w        =  [cells(i), nodes(i), hfaces(i), faces(i)];% subfaces(i)];
   w        =  double(sortrows(w));
  
end
