function res = states_diff(s1, s2)
%
% Compute a series of states representing the differences between two
% compatible series of states.
%
% SYNOPSIS:
%   function res = states_diff(s1, s2)
%
% PARAMETERS:
%   s1 - first series of states
%   s2 - second series of states (can also be a single state)
%
% RETURNS:
%   res - series of states representing the difference between the input
%   series of states
%
% EXAMPLE:
%
% SEE ALSO:
%
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   res = {};
   for i = 1:numel(s1)
      s1_i = s1{i};
      if iscell(s2)
         s2_i = s2{i};
      else
         assert(isstruct(s2));
         s2_i = s2;
      end
      
      res = {res{:}, combine_struct(s1_i, s2_i, @minus)}';
   end
end
