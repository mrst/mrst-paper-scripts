function [pvMultR, cr] = constructPvMultR(alpha, E, nu, poro, p_ref)
%
% SYNOPSIS:
%   function pvMultR = constructPvMultR(alpha, E, nu)
%
% DESCRIPTION:
%
%   Construct a pore volume multiplier function that is consistent with the
%   provided rock properties. 
%
% PARAMETERS:
%   alpha - biot's coefficient
%   E     - Young's module
%   nu    - Poisson's parameter
%
% RETURNS:
%   pvMultR - 
%
% EXAMPLE:
%
% SEE ALSO:
%
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   % Deducing grain compressibility
   cr = 3 * (1 - 2 * nu) .* (1 - alpha) ./ E;
   
   % Constructing pore volume multiplier function 
   pvMultR = @(p) 1 + cr .* (alpha./poro - 1) .* (p - p_ref);
   
end
