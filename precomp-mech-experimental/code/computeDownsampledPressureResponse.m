function div = computeDownsampledPressureResponse(model, cutoff, pbasis, irmap, varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   error('needs to be fixed to support multiple layers');
   opt.tol = 1e-6;
   opt = merge_options(opt, varargin{:});
   
   % Determining cells for which to explicitly compute response
   border_pos  = is_bordercell(model.G);
   coarser_pos = at_coarser_pos(model.G);
   
   comp_cells = coarser_pos | border_pos;
   
   interp_hor_cells = logical(shiftField(model.G, double(coarser_pos), [1 0 0], false)) & ~border_pos;
   interp_ver_cells = logical(shiftField(model.G, double(coarser_pos), [0 1 0], false)) & ~border_pos;

   interp_mid_cells = ~(comp_cells | interp_hor_cells | interp_ver_cells);
   
   div = computeSelectedPressureResponses(model, find(comp_cells), cutoff, pbasis, 'tol', opt.tol);
   
   % do interpolation here
   div = interpolate_responses(div, model.G, find(interp_hor_cells), [1 2]);
   div = interpolate_responses(div, model.G, find(interp_ver_cells), [3 4]);
   div = interpolate_responses(div, model.G, find(interp_mid_cells), [1 2 3 4]);
end

% ----------------------------------------------------------------------------

function div = interpolate_responses(div, G, cells, neighs)

   assert(G.griddim == 3); %@@ only implemented in 3D for now
   n = G.cells.num;
   ixs = [-1 0; 1 0; 0 -1; 0 1]; % index shift
   cmap = nan(prod(G.cartDims), 1);
   cmap(G.cells.indexMap) = 1:n;

   for c = cells(:)'
      assert(nnz(div(:,c)) == 0); % this column should be untouched until now
      count_missing = zeros(n, 1);
      for dir = neighs % west, east, south, north
         [i, j, k] = ind2sub(G.cartDims, G.cells.indexMap(c));
         neigh_ix_global = sub2ind(G.cartDims, i + ixs(dir, 1), j + ixs(dir, 2), k);
         neigh_ix = cmap(neigh_ix_global);
         
         sfield = shiftField(G, div(:, neigh_ix), [-ixs(dir,:), 0], NaN);
         missing_vals = isnan(sfield);
         count_missing(missing_vals) = count_missing(missing_vals) + 1;
         
         div(~missing_vals,c) = div(~missing_vals,c) + sfield(~missing_vals);
      end
      div(:,c) = div(:,c) ./ (numel(neighs) - count_missing);
      % In some edge cases, it may happen that denominateor becomes zero
      % (none of the considered neighbor functions, when shifted, have a
      % value that covers a particular cell).  If so happens, replace with
      % zero by lack of a better alternative. @@
      fix = isnan(div(:,c));
      div(fix,c) = 0;
   end
end

% ----------------------------------------------------------------------------

function ind = is_bordercell(G)
% Identify all cells with lateral boundary
   
   % @@ For the movement, we consider lateral boundaries to be all boundary
   % faces with normals with zero z-component

   ind = false(G.cells.num, 1);
   ind(unique(sum(G.faces.neighbors(prod(G.faces.neighbors,2)==0 & ...
                                    (G.faces.normals(:,3)==0), :), 2))) = true;
end

% ----------------------------------------------------------------------------

function ind = at_coarser_pos(G)
   assert(G.griddim == 3); %@@ only implemented in 3D for now

   ind = false(G.cells.num, 1);
   
   [ii, jj, kk] = meshgrid(1:2:G.cartDims(1), 1:2:G.cartDims(2), 1:G.cartDims(3));
   
   glob_to_loc = nan(prod(G.cartDims), 1);
   glob_to_loc(G.cells.indexMap) = 1:numel(G.cells.indexMap);
   
   cell_ix = glob_to_loc(sub2ind(G.cartDims, ii, jj, kk));
   cell_ix = cell_ix(~isnan(cell_ix));
   ind(cell_ix) = true;
end
