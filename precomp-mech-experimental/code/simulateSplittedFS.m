function [wellsols, states] = simulateSplittedFS(initState, coupled_model, ...
                                                 schedule, varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

  opt.nf_faces = []; % no-flow faces
  [opt, rest] = merge_options(opt, varargin{:});
   
  num_steps = numel(schedule.step.val);
  
  fcells = coupled_model.Gmap.gc; % cells with flow
  
  % Determining key aquifer rock parameters
  phi = coupled_model.rock.poro;
  b   = coupled_model.rock.alpha; 
  E   = coupled_model.mech.Ev(fcells);
  nu  = coupled_model.mech.nuv(fcells);
  K   = E ./ (3 * (1 - 2 * nu));
  
  cp_fs = compute_fixed_stress_compressibility(phi, b, K);
                                               
  % Setting up solution structures
  wellsols = cell(num_steps, 1);
  states = cell(num_steps+1, 1);
  states{1} = initState; % Will be removed at end of function
  
  for step = 1:num_steps
     
     cur_control = schedule.control(schedule.step.control(step));
     dt = schedule.step.val(step);

     fprintf('------ Now solving timestep: %i -------\n', step);
     
     [wellsols{step}, states{step+1}] = ...
         solve_timestep(coupled_model , ...
                        states{step}  , ...
                        states{max(step-1, 1)}, ... % previous step
                        cur_control   , ...
                        dt            , ...
                        cp_fs, ...
                        opt.nf_faces);
  end
  
  states = states(2:end);

end

% ----------------------------------------------------------------------------

function [wsol, state] = solve_timestep(model, cur_state, prev_state, control, dt, cp, nf_faces)

   fcells = model.Gmap.gc;
   % Tolerances and iteration limits
   MAX_IT = 300;
   TOL = 1e-10;%5e-13;%1e-14; %16; %1e-16;%1e-10; % Tolerance used for outer loop stop criterion
   NONLINEAR_TOL = 1e-12;%1e-16;%1e-12; % Tolerance used when solving the WaterModel
   LINSOLVE_TOL = 5e-12;%1e-12; % Tolerance used when solving the elastic system

   % Parameters relevant for solving flow system
   b   = model.rock.alpha;
   phi = model.rock.poro;
   E   = model.mech.Ev(fcells);
   nu  = model.mech.nuv(fcells);
   K   = E ./ (3 * (1 - 2 * nu));

   % Set rock compressibility
   fluid = setfield(model.fluid, ...
                    'pvMultR', ...
                    @(p) model.rock.pvMultR(p) + cp(:, 2) .* p);
   
   % Additional parameters and operators relevant for solving the mechanical
   % system
   A = model.operators.mech.A;
   gP = model.operators.mech.gradP;
   rhs = model.operators.mech.rhs - model.addBcFromFluid(control.bc);
   L = model.operators.extra.precond;

   % Keeping track of the changing state of the total stress
   assert(model.GMech.griddim == 3); % @@ otherwise, a few details must be changed below
   mean_stress = [1; 1; 1; 0; 0; 0] ./ 3;  
   p      =  cur_state.pressure;
   p_prev = prev_state.pressure; 
   sigma_orig = cur_state.stress * mean_stress; 
   sigma_prev = prev_state.stress * mean_stress;
   sigma_orig = sigma_orig(fcells) - b .* p;
   sigma_prev = sigma_prev(fcells) - b .* p_prev;
   sigma_new  = sigma_orig + (sigma_orig - sigma_prev); % fixed rate of stress change
   x_prev = cur_state.x; % initial guess for mech solver
   bW0 = fluid.bW(p); % @@@
   
   for i = 1:MAX_IT

      % Computing correction term due to change in total stress.  Implemented
      % as a modification of the source term
      bW = fluid.bW(p);
      correction = -b./K .* (sigma_new.*bW - sigma_orig.*bW0) .* model.G.cells.volumes ./ dt; 
      
      assert(~isfield(control, 'src')); % @@@ Support for preexisting sources
                                        % not implemented
      control_modif = ...
          setfield(control, 'src', struct('sat', ones(size(sigma_new)), ...
                                          'cell', [1:numel(sigma_new)]', ...
                                          'rate', correction));

      % Just to be clear: if the rock object comes with its own pore volume
      % multiplier, it will not be used. 
      rock = rmfield(model.rock, 'pvMultR'); 
                                             
      % Solving flow equation
      wat_model = WaterModel(model.G, rock, fluid, 'nonlinearTolerance', NONLINEAR_TOL);
      if ~isempty(nf_faces)
         wat_model.operators.T_all(nf_faces) = 0;
         int_faces = prod(wat_model.G.faces.neighbors,2)~=0;
         wat_model.operators.T = wat_model.operators.T_all(int_faces);
      end
      [wsol, state] = ...
          simulateScheduleAD(cur_state, wat_model, ...
                             struct('control', control_modif, ...
                                    'step', struct('control', 1, 'val', dt)));
      state = state{1};
      p_old = p;
      p = state.pressure;
      
      % Compute mechanical response

      state.x = itersolve_JP(A, rhs + gP * (b .* p), 'solver', @pcg, ...
                             'M1', L, 'M2', L', 'x0', x_prev, 'tol', LINSOLVE_TOL);
      % state.x = A\(rhs+gP*(b.*p));
      
      x_prev = state.x;
      state = model.addDerivedQuantities(state);
      
      % Update stress field for the present iteration
      sigma_old = sigma_new;
      sigma_new = state.stress * mean_stress;
      sigma_new = sigma_new(fcells) - b .* p;
         
      % Quit if we have converged in stress
      vol_change = sum(cp,2) .* (p - p_old) + (b ./ K) .* (sigma_new - sigma_old);
      
      fprintf('Current volume change: %e\n', max(abs(vol_change)));
      if max(abs(vol_change)) < TOL
         break;
      end

   end
   if i == MAX_IT
      error('Reached max iterations without converging.  Aborting.');
   end
   %state = struct('x', x, 'pressure', p);
   %state = model.addDerivedQuantities(state);
   fprintf('Finished in %i iterations\n', i);
   
end

% ----------------------------------------------------------------------------

function cp = compute_fixed_stress_compressibility(phi, alpha, K)

   cp = bsxfun(@times, 1./phi,  [(alpha-phi).*(1-alpha)./K, alpha.^2./K]);
   
   %   cp = 1./phi .* ((alpha-phi).*(1-alpha)./K + alpha.^2./K);
   %cp = 1./phi .* (alpha.^2./K); % @@@@
   % equivalent
   %cp = 1./phi .* (alpha./K - phi .* (1-alpha) ./ K);
   
   % equivalent
   %cp = (alpha - phi .* (1 - alpha))./(K .* phi);
   
end


