function eqs = equationsMechanicBiot(x, fluidp,  G, rock, operators)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   s = operators.mech;
   alpha = rock.alpha;

   eqs{1} = s.A * x - s.gradP * (alpha .* fluidp) - s.rhs;

   fac =  1 / (1e6 * mean(G.cells.volumes));
   eqs{1} = eqs{1} * fac;

end