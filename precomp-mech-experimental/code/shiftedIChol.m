function L = shiftedIChol(A, shift, varargin)
   
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   opt.type = 'nofill'; % type can be 'nofill', or 'ict'
   opt.droptol = 1e-5;  % only relevant when type is 'ict'
   opt = merge_options(opt, varargin{:});
   % Seeking an incomplete choleski factorization, adding diagonal component if
   % necessary to prevent nonpositive pivot
   finished = false;
   MAX_REPEAT = 100;
   repeat_count = 0;
   while ~finished
      try
         if repeat_count == 0
            L = ichol(A, struct('michol' , 'off', ...
                                'type'   , opt.type, ...
                                'droptol', opt.droptol));
         else         
            L = ichol(A, struct('diagcomp', shift * 2^(repeat_count - 1), ...
                                'michol'  , 'off', ...
                                'droptol' , opt.droptol));
         end
         finished = true;
      catch
         % ichol did not work. Assuming nonpositive pivot encountered
         dispif(mrstVerbose(), ['Incomplete factorization failed; retry with ' ...
                             'new shift.\n']);
         repeat_count = repeat_count + 1;
         if repeat_count > MAX_REPEAT
            error('Max repeat count reached in shiftedIChol');
         end
      end
   end
end      

