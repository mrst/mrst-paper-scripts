function [x, flag, relres, iter, resvec] = itersolve_JP(A, b, varargin)
% Conjugent gradient method preconditioned with simple Jacobi (diagonal) preconditioner
%
% SYNOPSIS:
%   function res = itersolve_JP(A, b, varargin)
%
% PARAMETERS:
%   A        - System matrix
%   b        - right hand side
%   varargin - optional arguments:
%              * 'tol'   - tolerance (Default: 1e-6)
%              * 'maxit' - max. number of iterations (default 1000)
%              * 'solver' - iterative solver to se
%              * 'precond' - options are: 'jacobi', 'milu', 'none'
%
% RETURNS:
%   x - the computed solution to the linear equation system
%
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
   opt.tol = 1e-6;
   opt.maxit = 1000;
   opt.x0 = [];
   opt.solver = @bicgstab; %@gmres; %minres; %@bicgstab; %@minres;
   opt.precond = 'jacobi';
   opt.M1 = []; % if preconditioners are specifically provided by M1, M2, 
   opt.M2 = []; % then 'opt.precond' does not matter
   opt.gmres_restart = 200;
   opt.droptol = 1e-2;
   opt.udiag = 0;
   opt.thresh = 0;
   opt = merge_options(opt, varargin{:});
   %opt.tol = 1e-11;%%@@@@@x
   
   failed_preconditioner = false;
   if ~isempty(opt.M1) || ~isempty(opt.M2)
      % Preconditioners directly provided
      M1 = opt.M1;
      M2 = opt.M2;
   else
      % Compute preconditioners based on user's choice
      n = size(A,1);
      switch opt.precond
        case 'jacobi'
          M1 = spdiags(diag(A), 0, n, n);
          M2 = [];
        case 'milu'
          [M1, M2] = ilu(A, struct('milu', 'row'));
        case 'ilu'
          try 
             [M1, M2] = ilu(A, struct('milu', 'off'));
          catch
             fprintf(['Zero diagonal element encountered.  Could not make a ' ...
                      'good preconditioner.\n']);
                      
             failed_preconditioner = true;
          end
        case 'combi' %@@@ experimental.  Might not provide any benefit
          D = spdiags(diag(A), 0, n, n);
          A = D\A;
          b = D\b;
          [M1, M2] = ilu(A, struct('milu', 'row'));
        case 'ilutp'
          try
             [M1, M2] = ilu(A, struct('type', 'ilutp', 'droptol', opt.droptol, ...
                                      'thresh', opt.thresh,...
                                      'udiag', opt.udiag));
          catch
             fprintf(['Zero diagonal element encountered.  Could not make a ' ...
                      'good preconditioner\n.']);
             failed_preconditioner = true;
          end
        case 'none'
          M1 = [];
          M2 = [];
        otherwise
          error('unrecognized preconditioning option');
      end
   end
   
   flag = 0;
   if ~failed_preconditioner
      % try iterative solver
      if strcmpi(func2str(opt.solver), 'gmres')
         maxit = min(opt.maxit, size(A,1));
         [x, flag, relres, iter, resvec] = opt.solver(A, b, opt.gmres_restart, opt.tol, opt.maxit, ...
                                                      M1, M2, opt.x0);
      else
         [x, flag, relres, iter, resvec] = opt.solver(A, b, opt.tol, opt.maxit, M1, M2, opt.x0);   
      end
   end
   
   if failed_preconditioner || (flag ~= 0)
      if mrstVerbose
         % Sending warning message 
         switch(flag)
           case 1
             fprintf('Max number of iterations (%i) was reached without converging. ', opt.maxit);
           case 2
             fprintf('Preconditioner was ill-conditioned.  ');
           case 3 
             fprintf('Iterative solver stagnated.  ');
           case 4 
             fprintf('Too large or small scalar quantity reached in iterative solver.  ');
         end
         fprintf('Falling back on backslash solver.\n');
      end
      x = A\b;
   end
end
% minres seems to work better
% for all the iterative solvers, the following line benefits from
% preconditioner:
% minres(A, gP * (alpha .* result.states{i}.pressure), 1e-6, 1000);
% but the following line doesn't:
% minres(A, gP * (alpha .* result.states{i}.pressure - alpha .* initState.pressure), 1e-6, 1000)
