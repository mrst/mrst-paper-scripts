function [model, initState, schedule, results] = compare_solvers(scenario, varargin)
% Test the provided scenario on one or more solvers
% 
% SYNOPSIS:
%   function [initState, schedule, results] = compare_solvers(scenario, varargin)
%
% DESCRIPTION:
%
% PARAMETERS:
%   scenario - a structure containing the fields 'model', 'initState' and 'schedule'
%   varargin - key/value pairs to override other simulation parameters
%
% RETURNS:
%   initState - initial state (common to all results)
%   schedule  - control schedule (common to all results)
%   results   - cell array, one entry per run.  Fields are:
%               * states  - solution states
%               * t_flow  - time required to solve flow problem
%               * t_mech  - time required to solve mech problem
%               * t_total - iime required to solve full problem
%
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   opt.approaches = {'full', ...
                     'uncoupled flow only', ...
                     'uncoupled flow full mech', ...
                     'iterative flow', ...
                     'fixed stress'};

   opt.aifun_tol = 1e-4;
   opt.ppr_cutoff = 0; %1e-3;
   opt.compute_mech = false;
   
   % Following options only relevant for 'full ppr'
   opt.ppr_savefile = [];        % Savefile for pressure reponses
   opt.ppr_batch_compute = true; % Whether to compute multiple responses at a time
   opt.renormalize = true;       % rescale remaining part of response to
                                 % compensate for cut-away part
   opt.ppr_itersolve = true;     % if true, use iterative solver (bicgstab)
   
   [opt, rest] = merge_options(opt, varargin{:});
   
   %% Running tests
   model = scenario.model;
   initState = scenario.initState;
   schedule = scenario.schedule;
   
   results = [];
   for run = opt.approaches
      switch run{:}
        case 'full'
          fprintf('Running full case...\n');
          tic; cur_res = run_full_case(model, initState, schedule); t = toc;
          fprintf('Finished in %f seconds\n', t);
        case 'fixed stress'
          fprintf('Running fixed-stress split case...\n');
          tic; cur_res = run_fixed_stress_case(model, initState, schedule, rest{:}); t = toc;
          fprintf('Finished in %f seconds\n', t);
        case 'local ppr'
          fprintf('Running localized ppr case...\n');
          tic; cur_res = run_simple_ppr(model, initState, schedule, 'local', ...
                                        opt.ppr_cutoff, opt.compute_mech, ...
                                        'itersolve', opt.ppr_itersolve, rest{:}); t = toc;
          fprintf('Finished in %f seconds\n', t);
        case 'full ppr'
          fprintf('Running full ppr case...\n');
          tic; cur_res = run_simple_ppr(model, initState, schedule, 'full', ...
                                        opt.ppr_cutoff, opt.compute_mech, ...
                                        'savefile', opt.ppr_savefile, ...
                                        'batch_compute', opt.ppr_batch_compute, ...
                                        'itersolve', opt.ppr_itersolve, ...
                                        'renormalize', opt.renormalize,rest{:}); t = toc; 
          fprintf('Finished in %f seconds\n', t);
        case 'full ppr transform pressure'
          fprintf('Running full ppr case with transformed pressure...\n');
          tic; cur_res = run_simple_ppr(model, initState, schedule, 'full', ...
                                        opt.ppr_cutoff, opt.compute_mech, ...
                                        'itersolve', opt.ppr_itersolve, ...
                                        'transform', true,rest{:}); t = toc; 
          fprintf('Finished in %f seconds\n', t);
        case 'downsampled ppr'
          fprintf('Running downsampled ppr case...\n');
          tic; cur_res = run_simple_ppr(model, initState, schedule, 'downsampled', ...
                                        opt.ppr_cutoff, opt.compute_mech, ...
                                        'itersolve', opt.ppr_itersolve,rest{:}); t=toc;
          fprintf('Finished in %f seconds\n', t);
        case 'basis update'
          fprintf('Running incremental basis update case...\n');
          tic; cur_res = run_basis_update(model, initState, schedule); t=toc;
          fprintf('Finished in %f seconds\n', t);
        case 'uncoupled flow only'
          fprintf('Running uncoupled flow case...\n');
          tic; cur_res = run_uncoupled_flow_case(model, initState, schedule, opt, 'none');
          t = toc;
          fprintf('Finished in %f seconds\n', t);
        case 'uncoupled flow full mech'
          fprintf('Running uncoupled flow case with mechanics...\n');
          tic; cur_res = run_uncoupled_flow_case(model, initState, schedule, opt, 'full'); 
          t = toc;
          fprintf('Finished in %f seconds\n', t);
        case 'iterative flow'
          error('deprecated');
          cur_res = run_iterative_case(model, initState, schedule);
        otherwise
          % Assume the option refers to a file with saved responses
          fprintf('Running saved ppr case with responses from %s.\n', run{:});
          tic; cur_res = run_simple_ppr(model, initState, schedule, run{:}, ...
                                        [], opt.compute_mech, ...
                                        'itersolve', opt.ppr_itersolve,rest{:}); t = toc;
          fprintf('Finished in %f seconds\n', t);
      end
      results = [results, cur_res]; %#ok
   end
   
   initState = extendFieldOutsideAquifer(initState, 'pressure', model.GMech, model.Gmap);
end

% ----------------------------------------------------------------------------

function result = run_basis_update(model, initState, schedule)

   tic;
   [~, states] = simulateBasisUpdate(initState, model, schedule);
   t_total = toc;

   states = extendFieldOutsideAquifer(states, 'pressure', model.GMech, model.Gmap);

   result = struct('t_flow', [], 't_mech', [], 't_total', t_total, 'states', ...
                   {states}, 'extra', []);
end

% ----------------------------------------------------------------------------
function result = run_simple_ppr(model, initState, schedule, method, cutoff, ...
                                 compute_mech, varargin)
   opt.transform = false;
   opt.savefile = [];
   opt.batch_compute = true;
   opt.renormalize = true;
   opt.itersolve = false;
   opt.nf_faces = []; % imposed transmissibility

   opt = merge_options(opt, varargin{:});
   
   tic;
   ppr_model = SimplePrecompModel(model                                      , ...
                                  schedule.control(1).bc                     , ...
                                  initState.pressure                         , ...
                                  'savefile'             , opt.savefile      , ...
                                  'batch_compute'        , opt.batch_compute , ...
                                  'renormalize'          , opt.renormalize   , ...
                                  'nonlinearTolerance'   , 1e-13             , ... %@@@
                                  'cutoff'               , cutoff            , ...
                                  'transformed_pressure' , opt.transform     , ...
                                  'response_type'        , method            , ...
                                  'whole_pillars'        , ~strcmpi(method , 'local'));
   
   if ~isempty(opt.nf_faces)
      ppr_model.operators.T_all(opt.nf_faces) = 0;
      int_faces = prod(ppr_model.G.faces.neighbors, 2)~=0;
      ppr_model.operators.T = ppr_model.operators.T_all(int_faces);
   end

   if opt.transform
      initState.pressure = ppr_model.imp_mat \ initState.pressure;
   end
   t_response = toc;
   
   tic;
   if opt.itersolve
      % There may be zeros on the diagonal with regards to the well equations,
      % so do not apply jacobi!
      solver = IterativeLinearSolver('solvertype', @bicgstab, ...
                                     'tolerance' , 1e-6, ...
                                     'is_sticky', false, ...
                                     'precond', 'ilutp');
   else
      solver = [];
   end
   [wsols, states] = simulateScheduleAD(initState, ppr_model, schedule, ...
                                        'LinearSolver', solver);

   t_flow = toc;

   if opt.transform
      % convert all pressures back to regular piecewise constant pressure
      % basis
      for i = 1:numel(states)
         states{i}.pressure = ppr_model.imp_mat * states{i}.pressure;
      end
   end
   
   if compute_mech
      % compute full mechanics
      tic;
      states = compute_full_mechanics(model, initState, states, ppr_model.fbc);
      t_mech = toc;
      t_total = t_flow + t_mech + t_response;
   else
      t_mech = [];
      t_total = t_flow + t_response;
      states = extendFieldOutsideAquifer(states, 'vdiv', model.GMech, model.Gmap);   
   end
   
   states = extendFieldOutsideAquifer(states, 'pressure', model.GMech, model.Gmap);

   result = struct('t_flow', t_flow, 't_mech', t_mech, 't_total', t_total, 'states', ...
                   {states}, 'wellsols', {wsols}, 'extra', ppr_model);
end

% ----------------------------------------------------------------------------

function result = run_fixed_stress_case(model, initState, schedule, varargin)

   tic;
   [wellsol, states] = simulateSplittedFS(initState, model, schedule, varargin{:});
   t_total = toc;
   states = extendFieldOutsideAquifer(states, 'pressure', model.GMech, ...
                                      model.Gmap);
   result = struct('t_flow', [], 't_mech', [], 't_total', t_total, 'states', ...
                   {states}, 'wellsols', {wellsol}, 'extra', []);
   
end

% ----------------------------------------------------------------------------

function result = run_iterative_case(model, initState, schedule)
   
   tic;
   [wellsol, states] = simulateSplittedPoromech(initState, model, schedule);
   t_total = toc;

   states = extendFieldOutsideAquifer(states, 'pressure', model.GMech, model.Gmap);
   
   result = struct('t_flow', [], 't_mech', [], 't_total', t_total, 'states', ...
                   {states}, 'wellsols', {wellsol}, 'extra', []);
   
end


% ----------------------------------------------------------------------------
function result = run_full_case(model, initState, schedule, varargin)

   opt.solvertype = @bicgstab;
   opt.tolerance = 1e-6;
   opt.droptol = 5e-1;
   opt.precond = 'ilutp';
   opt.udiag = 0; % 0 (false) or 1 (true).
   opt = merge_options(opt, varargin{:});
   
   solver = [];
   tic;
   if ~isempty(opt.solvertype)
      solver = IterativeLinearSolver('solvertype', opt.solvertype, ...
                                     'tolerance' , opt.tolerance, ...
                                     'droptol'   , opt.droptol, ...
                                     'udiag'     , opt.udiag, ...
                                     'precond'   , opt.precond);
   end

   try
      [wellsols, states] = simulateScheduleAD(initState, model, schedule, ...
                                              'LinearSolver', solver);
   catch me
      warning(['Proposed linear solver did not work.  Falling back on backslash ' ...
               'solver...']);
      [wellsols, states] = simulateScheduleAD(initState, model, schedule);
   end
   

   t_total = toc;
   states = extendFieldOutsideAquifer(states, 'pressure', model.GMech, model.Gmap);
   result = struct('t_flow', [], 't_mech', [], 't_total', t_total, 'states', ...
                   {states}, 'wellsols', {wellsols}, 'extra', []);
   
end

% ----------------------------------------------------------------------------

function result = run_uncoupled_flow_case(model, initState, schedule, opt, include_mech)

   [~, ~, div_term] = estimateStorativityParams(model, true);
   
   
   % In the construtciont of the 'pvMultR' function below, we assume that the
   % initial divergence (initState.vdiv) was computed at initial pressure
   % (initState.pressure), so that the change in porosity as a consequence of
   % pressure-driven rock expansion/contraction is linearized by adding
   % div_term * (p - initState.pressure).  Since opt.pore_press_top and
   % initState.pressure are not usually identical, this split in two linear
   % terms is necessary.
   g = gravity;
   if model.G.griddim==2
      g = g(2:3);
   end
   
   origMultR = @(p) model.rock.pvMultR(p) - 1;
   
   flowmodel = WaterModel(model.G                                                      , ...
                          rmfield(model.rock, 'pvMultR')                               , ...
                          setfield(model.fluid                                         , ...
                                   'pvMultR'                                           , ...
                                   @(p) 1 + (model.rock.alpha ./ model.rock.poro .*      ...
                                   initState.vdiv(model.Gmap.gc)) +                      ...
                                   origMultR(p) + ...
                                   div_term .* (p - initState.pressure)), ...
                          'gravity', g);
   tic; 
   [wellsols, states] = simulateScheduleAD(initState, flowmodel, schedule);
   t_flow = toc;
   
   if strcmpi(include_mech, 'full')
      % compute full mechanics
      tic;
      states = compute_full_mechanics(model, initState, states);
      t_mech = toc;
      t_total = t_flow + t_mech;
   else
      t_mech = [];
      states = strip_mechanics(states);
      t_total = t_flow;
      % Adding the local approximation of vdiv
      for ix = 1:numel(states)
         states{ix}.vdiv = initState.vdiv(model.Gmap.gc) + div_term .* ...
             (states{ix}.pressure - initState.pressure);
      end
   end

   states = extendFieldOutsideAquifer(states, 'pressure', model.GMech, model.Gmap);
   states = extendFieldOutsideAquifer(states, 'vdiv', model.GMech, model.Gmap);   
   
   result = struct('t_flow', t_flow, 't_mech', t_mech, ...
                   't_total', t_total, 'states', {states}, 'wellsols', {wellsols}, ...
                   'extra', []);

end

% ----------------------------------------------------------------------------

function states = strip_mechanics(states)

   for i = 1:numel(states)
      states{i} = rmfield(states{i}, {'uu','u','vdiv','stress', 'strain'});
   end
end


% ----------------------------------------------------------------------------

function states = compute_full_mechanics(model, initState, states, fbc)

   A = model.operators.mech.A;
   L = model.operators.extra.precond;
   x0 = initState.x;
   tol = 10e-13;
   for i = 1:numel(states)

      rhs = model.operators.mech.rhs + ...
            model.operators.mech.gradP * (model.rock.alpha .* states{i}.pressure) ...
            - fbc;
      states{i}.x = itersolve_JP(A, rhs, 'solver', @pcg, ...
                                 'M1', L, 'M2', L', 'x0', x0, 'tol', tol);
      states{i} = model.addDerivedQuantities(states{i});
      x0 = states{i}.x;
   end
   
end
