function div = batchComputeSelectedPressureResponses(model, cutoff, irmap, ...
                                                     renormalize, varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   opt.tol = 1e-5; % 1e-6;
   opt.R_fac = 3; % 2.5 radii spacing between response functions
   opt = merge_options(opt, varargin{:});
   R = estimate_influence_radius(model, cutoff, irmap, opt.tol, renormalize);

   num_responses = max(irmap);
   remaining = true(num_responses, 1);
   div = sparse(num_responses, num_responses);
   
   % compute batches of geographically separate response functions until
   % there are none left

   %pool = gcp;
   %num_workers = pool.NumWorkers; 
   num_workers = 1;
   
   cellnodes = cellNodes(model.G); % compute once and for all, for efficiency
   while any(remaining)
      saved_responses = cell(num_workers, 1);
      saved_res_ixs = cell(num_workers, 1);
      collissions = cell(num_workers, 1);
      
      % To avoid passing 'model' into the parallel loop (which costs a lot of
      % memory), we extract only those parts we need
      G = model.G;
      A = model.operators.mech.A;
      gradP_op = model.operators.mech.gradP;
      div_op = model.operators.mech.div;
      precond = model.operators.extra.precond;
      alpha = model.rock.alpha;
      
      for i=1:num_workers
      %parfor i=1:num_workers
      
         % choose new impulse_batch of respone functions
         impulse_batch = setup_batch(irmap, remaining, R * opt.R_fac, G, i, cellnodes);
         fprintf('Batch size: %i\n', numel(impulse_batch));
         
         % compute combined response for this impulse_batch
         field = compute_response(G, A, gradP_op, div_op, precond, ...
                                  alpha, irmap, impulse_batch, opt.tol);
         
         % separate individual responses, rinse and repeat
         [responses, response_ixs] = ...
             separate_responses(impulse_batch, field, G, irmap, cutoff, renormalize);

         % div(:, response_ixs) = responses;
         saved_responses{i} = responses;
         
         % register computed responses kept from the batch
         saved_res_ixs{i} = response_ixs;
               
         collissions{i} = numel(response_ixs) < numel(impulse_batch);
         
      end
      
      collissions_found = false;
      for i = 1:num_workers
         responses_ixs = saved_res_ixs{i};
         responses = saved_responses{i};
         
         div(:, responses_ixs) = responses;
         remaining(responses_ixs) = false;
         collissions_found = collissions_found || collissions{i};
      end
      
      if collissions_found
         expand_fac = 1.1;
         fprintf('Expanding radius from %f to %f\n', R, R*expand_fac);
         R = R * expand_fac;
      end

      % Reporting
      fprintf('Computing response functions. %f percent done.\n', ...
              sum(double(~remaining)) / numel(remaining) * 100);
      
   end
   %keyboard;
end

% ----------------------------------------------------------------------------

function [responses, response_ixs] = ...
       separate_responses(batch, field, G, irmap, cutoff, renormalize)

   sorted_batch = sortrows([field(batch(:)), batch(:)]);
   responses = sparse([]);
   response_ixs = [];
   
   pillar_centers = pillar_centers_xy(G, irmap);
   neighborhoods = batch(discrete_voronoi(batch, pillar_centers));
   
   for row = sorted_batch'
      % row(1) is peak value, row(2) is index of impulse center cell
      res = extract_response(field, row(2), irmap, G, pillar_centers, ...
                             neighborhoods(:), cutoff, renormalize);
      if ~isempty(res)
         responses = sparse([responses, res]);
         response_ixs = [response_ixs, row(2)];
      end
   end
   % num_responses = max(irmap);
   % tmp = false(num_responses, 1);
   % tmp(response_ixs) = true;
   % response_ixs = tmp;
end

% ----------------------------------------------------------------------------

function pcoords = pillar_centers_xy(G, irmap)
   
   count = accumarray(irmap, ones(numel(irmap), 1));
   x_avg = accumarray(irmap, G.cells.centroids(:,1)) ./ count;
   y_avg = accumarray(irmap, G.cells.centroids(:,2)) ./ count;
   
   pcoords = [x_avg(:), y_avg(:)];
   
end

% ----------------------------------------------------------------------------

function neigh = discrete_voronoi(centers, points)
% Locate closest center for each point
   
   centers = points(centers, :); % convert centers from indices to actual coords
   
   dist2 = nan(size(points, 1), numel(centers));
   
   % for each center compute distances to all points
   for i = 1:size(centers, 1)
      dist2(:,i) = sum(bsxfun(@minus, points, centers(i,:)).^2, 2);
   end

   [~, neigh] = min(dist2, [], 2);
   
end

% ----------------------------------------------------------------------------

function res = extract_response(field, ix, irmap, G, pillar_centers, neighs, ...
                                cutoff, renormalize)
   
   pillar_vols = accumarray(irmap, G.cells.volumes);   
   peak = abs(field(ix));
   threshold = cutoff * peak;
   total_volchange = field' * pillar_vols;
   
   % keeping only non-negligible values 
   field(abs(field) < threshold) = 0;
   
   % Renormalizing if required
   if renormalize
      cur_volchange = field' * pillar_vols;
      field = field / cur_volchange * total_volchange;
   end
   
   % Restrict kept values to the relevant neighborhood region
   res = field;
   res(neighs~=ix) = 0;
   keeps = abs(res) > 0;
   
   % ensuring that if the response approaches zero locally inside its
   % support, that part is not truncated.
   d2 = sum(bsxfun(@minus, pillar_centers, pillar_centers(ix, :)).^2, 2);
   maxrad2 = max(d2(keeps));
   keeps = d2 <= maxrad2;
   res(keeps) = field(keeps);
   
   
   % Find out if any of the remaining non-negligible values are outside the
   % neighborhood 
   keep_ixs = find(keeps);
   keep_faces = G.cells.faces(mcolon(G.cells.facePos(keep_ixs), ...
                                     G.cells.facePos(keep_ixs+1)-1));
   keep_neighs = unique(reshape(G.faces.neighbors(keep_faces,:), 1, []));
   if keep_neighs(1) == 0
      keep_neighs = keep_neighs(2:end);
   end
   keep_pillars = unique(irmap(keep_neighs));
   
   keep_indicators = zeros(size(field));
   keep_indicators(keep_pillars) = 1;
   
   outside_neigh = ones(size(field));
   outside_neigh(neighs==ix) = 0;
   
   if sum(keep_indicators(:) .* outside_neigh(:)) == 0
      % the extended support of the kept values does not go outside the
      % neighborhood.  We consider this sufficient for accepting this
      % response as isolated from the others.
      res = sparse(res);
   else
      fprintf('Warning: overlapping responses.  Discarding\n');
      %warning('Overlapping responses.  Discarding.');
      res = [];
   end
end

% ----------------------------------------------------------------------------

function impulse_batch = setup_batch(irmap, remaining, dist, G, number, cellnodes)
   rep_ix = find(remaining, number);
   rep_ix = rep_ix(end); % find the number'th occurence of a remaining
                         % function to compute
   len_x = max(G.cells.centroids(:,1)) - min(G.cells.centroids(:, 1));
   len_y = max(G.cells.centroids(:,2)) - min(G.cells.centroids(:, 2));
   
   ref_cell = find(irmap==rep_ix, 1);
   
   x_ref = G.cells.centroids(ref_cell, 1);
   y_ref = G.cells.centroids(ref_cell, 2);
   
   x_neg = fliplr(x_ref:(-dist):(x_ref-len_x-dist));
   x = [x_neg(1:end-1), x_ref:dist:(x_ref+len_x+dist)];
   
   y_neg = fliplr(y_ref:(-dist/sqrt(2)):(y_ref-len_y-(dist/sqrt(2))));
   y = [y_neg(1:end-1), y_ref:dist/sqrt(2):(y_ref + len_y + dist/sqrt(2))];
   
   [xgrid, ygrid] = ndgrid(x, y);
   x_offset = repmat([false;true], numel(y), 1);
   x_offset = x_offset(1:numel(y));
   if x_offset(find(y==y_ref, 1))
      x_offset = ~x_offset; % ensure there is no offset for our reference point
   end
   x_offset = double(x_offset) * dist/2;
   xgrid = bsxfun(@plus, xgrid, x_offset');
   
   xy = [xgrid(:), ygrid(:)];
   
   % identify cells that will be impulse centers
   impulse_batch = []; % start with no cells
   %cellnodes = cellNodes(G); % compute it once here, to save time
   for coord = xy'
      ix = find_laterally_overlapping_cell(coord, G, cellnodes);
      ix = irmap(ix);
      if ~isempty(ix) && remaining(ix)
         % there was a cell containing this coordinate, and its response has
         % not already been computed.
         impulse_batch = [impulse_batch, ix];
      end
   end
end

% ----------------------------------------------------------------------------

function ix = find_laterally_overlapping_cell(xy, G, cn)

   % find closest cell centroid
   d2 = sum(bsxfun(@minus, G.cells.centroids(:,1:2), xy').^2, 2);
   [dmin, ix] = min(d2);
   
   % is cell within lateral bounding box of cell?
   %cn = cellNodes(G);
   cn = cn(cn(:,1)==ix, 3); % node indices of cell indexed 'ix'
   Dx = max(G.nodes.coords(cn, 1)) - min(G.nodes.coords(cn, 1));
   Dy = max(G.nodes.coords(cn, 2)) - min(G.nodes.coords(cn, 2));

   dx = abs(G.cells.centroids(ix, 1) - xy(1));
   dy = abs(G.cells.centroids(ix, 2) - xy(2));
   
   if (dx > Dx/2) || (dy > Dy/2)
      ix = []; % point not covered by closest cell.  Ignore it
   end
end

% ----------------------------------------------------------------------------

function R = estimate_influence_radius(model, cutoff, irmap, tol, renormalize)
   
   % Finding a 'representative' impulse to use in estimating influence radius
   rep_ix = irmap(find_representative_cell(model.G));
   
   %compute radius
   field = compute_response(model.G, ...
                            model.operators.mech.A, ...
                            model.operators.mech.gradP, ...
                            model.operators.mech.div, ...
                            model.operators.extra.precond, ...
                            model.rock.alpha, ...
                            irmap, ...
                            rep_ix, ...
                            tol);
   %field = compute_response(model, irmap, rep_ix, tol);
   [~, R] = truncate_div(field, model.G, cutoff, renormalize, find(irmap == ...
                                                     irmap(rep_ix))); 
end

% ----------------------------------------------------------------------------

function field = compute_response(G, A, gradP, div, L, alpha, irmap, impulses, tol)

   impulse_field = zeros(G.cells.num, 1);
   
   % specifying all impulses (there may be more than one) in the field
   for i = impulses(:)'
      impulse_field(irmap==i) = 1 * alpha(irmap==i);
   end
   
   % computing response
   field = div * itersolve_JP(A, gradP * impulse_field , ...
                              'solver' , @pcg          , ...
                              'tol'    , tol           , ...
                              'M1'     , L             , ...
                              'M2'     , L');
   % aggregating columns
   vols = accumarray(irmap, G.cells.volumes);
   resp = accumarray(irmap, field);
   field = resp./vols; % divergence per volume
end


% ----------------------------------------------------------------------------

function ix = find_representative_cell(G)

   % look for a cell as far removed from the _lateral_ boundary as possible
   
   % identify _lateral_ boundary faces
   lat_faces = unique(G.cells.faces(G.cells.faces(:,2)<=4, 1));
   lat_bnd_faces = lat_faces(prod(G.faces.neighbors(lat_faces, :), 2)==0);
   
   bnd_cells = unique(sum(G.faces.neighbors(lat_bnd_faces,:), 2));

   bcount = inf(G.cells.num, 1);
   
   bcount(bnd_cells) = 1;

   int_faces = G.faces.neighbors(prod(G.faces.neighbors,2) > 0, :);

   while any(isinf(bcount))
      lr = bcount(int_faces); % bcount at left and right of interior face
      
      ix = isinf(lr(:,1));
      lr(ix, 1) = lr(ix, 2) + 1; % if right side is inf, left remains inf
      
      ix = isinf(lr(:,2));
      lr(ix, 2) = lr(ix, 1) + 1; % if left side is inf, right remains inf
      
      bcount = accumarray(int_faces(:), lr(:), size(bcount), @min);
   end
   [~, ix] = max(bcount);
end
