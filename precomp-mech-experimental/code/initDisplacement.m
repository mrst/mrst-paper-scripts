function state = initDisplacement(model, state, uu, varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

    opt = struct('mech_equil', true, ...
                 'pressure'  , []   );
    opt = merge_options(opt, varargin{:});

    if (opt.mech_equil)

        if isprop(model, 'mechModel')
            mechmodel = model.mechModel; 
        else
            mechmodel = MechanicBiotModel(model.G, model.rock, model.mech, ...
                                          'InputModel', model);
        end

        state = mechmodel.setProp(state, 'xd', mechmodel.operators.mech.rhs); % Dummy values, just used
                                                                              % to get the correct dimension.

        if ~isempty(opt.pressure)
            drivingForces.fluidp = opt.pressure;
        else
            drivingForces.fluidp = 0;
        end

        solver = NonLinearSolver(); % The problem is linear but we use the general
                                    % framework. The cost should be
                                    % minimal.
        [state, converged, failure, its, nonlinearReports] = ...
            solveMinistep(solver, mechmodel, state, state, 0, drivingForces);

    else
        error('not checked')
        u = reshape(uu', [], 1);
        state.xd = zeros(size(model.operators.mech.A, 2), 1);
        state.xd = u(~model.operators.mech.isdirdofs);
    end

    state = addDerivedQuantities(mechmodel, state);

end
