function Pb = numIntHStaticPressure(zref, Pref, h, rho_fun)
% Given a reference pressure (Pref) and height (zref),
% compute the hydrostatic pressure at 'zref'+'h', given
% the fluid density function 'rho_fun', which is a function of 
% pressure and temperature, and the temperature as a function of depth ('Tfun')
% NB: Not vectorized; 'h' has to be a scalar.
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   
    Pb = ode23(@(z, p) norm(gravity) * rho_fun(p), ...
               [zref(:), zref(:)+h(:)], ...
               Pref(:));
    % Pb = ode113(@(z, p) norm(gravity) * rho_fun(p, Tfun(z)), ...
    %            [zref(:), zref(:)+h(:)], ...
    %            Pref(:));

end

