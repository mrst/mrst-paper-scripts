function [c_m, zeta, div_term, pvMultOrig] = estimateStorativityParams(model, uniform)

%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
   if uniform
      c_m = compute_geertsma_uniform(model);
   else
      c_m = compute_geertsma_individual(model);
   end
   
   % Estimation of changes in fluid content
   c_fluid_model = model.fluid.bW(1) - model.fluid.bW(0); % assuming linear
   pvMultOrig = model.rock.pvMultR(1) - model.rock.pvMultR(0);
   phi_model = model.rock.poro;
   alpha = model.rock.alpha;
   
   % For an uniaxial (or unbounded) domain, the change in fluid content,
   % zeta, should be proportional to pressure change, with the
   % proportionality constant expressed below.
   % @@@ This zeta is not correct unless reference pressure is the same for
   % original divergence and original porosity (phi_model)
   zeta = alpha .* c_m + phi_model .* (pvMultOrig + c_fluid_model);
   
   % The following term is used to estimate changes in pore space as a
   % consequence of rock expansion.
   div_term = (alpha ./ phi_model) .* c_m;
   
end

% ----------------------------------------------------------------------------

function c_m = compute_geertsma_uniform(model)
   
   % compute poroelastic properties for all cells at once, using a uniform
   % pressure change
   
   dp = 10 * barsa * ones(model.G.cells.num, 1);
   alpha = model.rock.alpha;
   %dx = model.operators.mech.A \ (model.operators.mech.gradP * (alpha .* dp));
   L = model.operators.extra.precond;
   dx = itersolve_JP(model.operators.mech.A, ...
                     (model.operators.mech.gradP * (alpha .* dp)), ...
                     'solver', @pcg, 'M1', L, 'M2', L');
   
   
   
   div = (model.operators.mech.div * dx) ./ model.G.cells.volumes;

   % Estimation of Geertsma's parameter
   c_m = div ./ dp;
      
end

% ----------------------------------------------------------------------------

function c_m = compute_geertsma_individual(model)

   dp        = 10 * barsa;
   alpha     = model.rock.alpha;
   num_cells = model.G.cells.num;
   c_m       = zeros(num_cells, 1);
   p         = zeros(num_cells, 1); 

   for i = 1:num_cells
      p    = p * 0; % reset pressure
      p(i) = dp; % insert impulse

      dx   = model.operators.mech.A \ (model.operators.mech.gradP * (alpha .* p));
      div  = (model.operators.mech.div * dx) ./ model.G.cells.volumes;
   
      % Estimating Geertsma's parameter for impulse cell
      c_m(i) = div(i) / dp;
      
   end
   
end
