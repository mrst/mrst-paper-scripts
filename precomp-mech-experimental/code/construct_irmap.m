function irmap = construct_irmap(G)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   if G.griddim == 2
      [i, j] = ind2sub(G.cartDims, G.cells.indexMap);
      [isort, isort_ix] = sort(i);
      inew = (1:numel(unique(isort)))';
      [~, mult] = rlencode(isort);
      irmap = zeros(numel(inew), 1);
      irmap(isort_ix) = rldecode(inew, mult);
   else 
      assert(G.griddim == 3);
      [i, j, k] = ind2sub(G.cartDims, G.cells.indexMap);
      ij = sub2ind(G.cartDims(1:2), i, j);
      [ijsort, ijsort_ix] = sort(ij);
      ijnew = (1:numel(unique(ijsort)))';
      [~, mult] = rlencode(ijsort);
      irmap = zeros(numel(ijsort), 1);
      irmap(ijsort_ix) = rldecode(ijnew, mult);
   end
end
