classdef IterativeLinearSolver < LinearSolverAD
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   properties
      
      solvertype
      precond
      is_sticky
      x0
      droptol
      udiag
      gmres_restart
      maxit
      thresh
   end
   
   methods
      function solver = IterativeLinearSolver(varargin)
         solver = solver@LinearSolverAD();
         solver.solvertype = @gmres;
         solver.droptol = 1e-2; % for the ilutp preconditioner
         solver.udiag = 0;
         solver.precond = 'jacobi'; 
         solver.is_sticky = false;
         solver.x0 = [];
         solver.gmres_restart = 200;
         solver.maxit = 1000;
         solver.thresh = 0;
         solver = merge_options(solver, varargin{:});
      end
      
      function [result, report] = solveLinearSystem(solver, A, b)
         result = itersolve_JP(A, b, ...
                               'solver', solver.solvertype, ...
                               'tol', solver.tolerance, ...
                               'droptol', solver.droptol, ...
                               'udiag', solver.udiag, ...
                               'precond', solver.precond, ...
                               'thresh', solver.thresh, ...
                               'gmres_restart', solver.gmres_restart, ...
                               'x0', solver.x0, ...
                               'maxit', solver.maxit);
                         
         if solver.is_sticky
            solver.x0 = result;
         end
         
         report = struct();
      end
   end
end
