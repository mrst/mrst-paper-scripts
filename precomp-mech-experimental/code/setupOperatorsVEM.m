function operators = setupOperatorsVEM(G, C, el_bc, load, alpha_scaling, S, ilu_tol)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
    [~, extra] = VEM_linElast(G                    , ...
                              C                   , ...
                              el_bc               , ...
                              load                , ...
                              'alpha_scaling', alpha_scaling , ...
                              'S', S                         , ...
                              'linsolve', @(A, rhs) 0 * rhs); 
    
    operators.mech = extra.disc; 
    
    vdiv    = VEM_div(G); 
    [~, op] = VEM_mrst_vec(G, C);%, 'blocksize', model.GMech.cells.num/10);
    strain  = op.WC' * op.assemb'; 
    stress  = op.D * strain; %op.WC' * op.assemb'; 
    
    % computing and storing useful preconditioner (incomplete Choleski)
    if isnan(ilu_tol)
        fprintf('Skipping ilu\n');
        iL = [];
    else
        iL = shiftedIChol(operators.mech.A, 0.5, 'droptol', ilu_tol, 'type', 'ict'); 
    end
    
    operators.extra = struct('vdiv', vdiv, 'stress', stress, 'strain', ...
                             strain, 'precond', iL); 
    
end