function [transmult, permmult] = makeStrainDependentTransmult(G, rock, p0, vs0, varargin)

% NB: in this function, volumetric strain (vs) should be provided as a
% dimensionless value (i.e. not multiplied by cell volume) 
% transmult returns transmissibility multpliers (associated with faces).  In
% addition, 'permmult' returns relatve changes in permeability (per cell)
% compared with initial configuration.
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   
   alpha_default = 20; %3;
   opt = merge_options(struct('exponent', alpha_default), varargin{:});

   assert(size(rock.perm, 2) == 1); % only handle isotropic permeability for now
   
   Vp        = @(p, vs) rock.poro .* rock.pvMultR(p) + rock.alpha .* vs; % pore volume function
   Vb        = @(p, vs) 1 + vs;                                          % bulk volume function
   phi       = @(p, vs) Vp(p, vs) ./ Vb(p, vs);                          % porosity function
   phi0      = phi(p0, vs0);                                             % initial porosity value
   perm      = @(p, vs) rock.perm .* (phi(p, vs) ./ phi0).^opt.exponent; % permeability  function
   %perm      = @(p, vs) rock.perm .* (1 + (((phi(p, vs) ./ phi0)-1) * 100));
   T         = @(p, vs) compute_trans(G, perm(p, vs));                   % transmissibility function
   T0        = T(p0, vs0);                                               % initial transmissibility
   transmult = @(p, vs) T(p, vs) ./ T0;                                  % trans mult function
   perm0     = perm(p0, vs0);
   permmult  = @(p, vs) perm(p, vs) ./ perm0;                            % relative change in permeability
end

% ----------------------------------------------------------------------------

% We cannot use computeTransTPFA, since it does not support permeabilities
% that are ADI variables.
% The below function assumes istotropic permeability, computed for use with TPFA.
function T = compute_trans(G, perm)
   [C, N, cellNo] = cell_geometry(G);
   T = zeros(size(cellNo));
   for i = 1:size(C, 2)
      T = T + (C(:,i) .* perm(cellNo) .* N(:,i));
   end
   T = T ./ sum (C .* C, 2);
   
   assert(all(T(~isnan(double(T)))>=0));
   
   % Reduce half-face trans for face transmissibility
   cf = G.cells.faces(:,1);
   nf = G.faces.num;      % number of faces
   nh = numel(cf); % number of half-faces
   
   accum_mat = accumarray([cf, (1:nh)'], 1, [nf, nh], [], 0, true);
   T = 1 ./ (accum_mat * (1./T));

   % Keeping only internal faces
   T = T(prod(G.faces.neighbors, 2) > 0);
   
   % accum_mat = accumarray([cf, cellNo], 1, [nf, G.cells.num]);   
   %   T  = 1 ./ accumarray(cf, 1./T, [nf, 1]);
end
% ----------------------------------------------------------------------------

function [C, N, cellNo] = cell_geometry(G)
   % Vectors from cell centroids to face centroids
   cellNo = gridCellNo(G);
   C = G.cells.centroids;
   C = G.faces.centroids(G.cells.faces(:,1), :) - C(cellNo,:);

   % Outward-pointing normal vectors
   cf  = G.cells.faces(:,1);
   sgn = 2*(cellNo == G.faces.neighbors(cf, 1)) - 1;
   N   = bsxfun(@times, sgn, G.faces.normals(cf, :));
end

%--------------------------------------------------------------------------
