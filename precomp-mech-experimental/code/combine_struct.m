function res = combine_struct(s1, s2, bin_op)
% Combine the fields of two structs to create a new struct.  Identical field
% names in the two structs are assumed to have identical types.  Only common
% fields with numerical values (scalars, vectors or matrices) will be retained.
% The way to combine the fields is specified by the binary operator 'bin_op'.
%
% SYNOPSIS:
%   function res = combine_struct(s1, s2, bin_op)
%
% DESCRIPTION:
%
% PARAMETERS:
%   s1     - the first struct
%   s2     - the second struct
%   bin_op - binary operator specifying how the structures will be combined
%
% RETURNS:
%   res - the combined structure
%
% EXAMPLE:  (Add all numeric fields of the two structures)
% sum = combine_struct(struct1, struct2, @plus)
% 
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   res = []; % result is initialized as empty

   common_fields = intersect(fields(s1), fields(s2));
   
   for f = common_fields'

      f = f{:}; % unpack the string contained in the cell array f
      if isnumeric(getfield(s1, f))
         res = setfield(res, f, bin_op(getfield(s1, f), getfield(s2, f)));
      end
   end
end
