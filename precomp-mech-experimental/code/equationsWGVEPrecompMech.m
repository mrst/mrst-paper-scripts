function [problem, state] = equationsWGVEPrecompMech(model, state0, state, dt, drivingForces, varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   opt = struct('reverseMode', false, ...
                'resOnly', false, ...
                'iteration', -1);

   opt = merge_options(opt, varargin{:});

   % assert(isequal(drivingForces.bc, model.flow_bc));  % @@ Should still
   % ensure that the (VE) bc provided is compatible with the (non-VE) bc used
   % when model was constructed
   
   assert(isempty(drivingForces.src)); % unsupported
   W  = drivingForces.W;
   s  = model.operators;
   f  = model.fluid;

   % Extract the current and previous values of all variables to solve for
   [p, sG, wellSol] = model.getProps(state , 'pressure', 'sg', 'wellsol');
   [p0, sG0, sGmax0]       = model.getProps(state0, 'pressure', 'sg', 'sGmax');

   % Stack well-related variables of the same type together
   bhp = vertcat(wellSol.bhp);
   qWs = vertcat(wellSol.qWs);
   qGs = vertcat(wellSol.qGs);

   %% Initialization of independent variables

   if ~opt.resOnly
      % ADI variables needed since we are not only computing residuals
      if ~opt.reverseMode
         [p, sG, qWs, qGs, bhp] = initVariablesADI(p, sG, qWs, qGs, bhp);
      else
         zw = zeros(size(bhp)); % dummy
         [p0, sG0, ~, ~, ~] = initVariablesADI(p0, sG0, zw, zw, zw);
      end
   end
   sGmax = max(sG, sGmax0);

   sW  = 1 - sG;  % for ease of reading, we define an explicit variable
   sW0 = 1 - sG0; % also for water saturation

   %% Preparing various necessary, intermediate values

   % CO2 phase pressure
   pW  = p;
   pW0 = p0;
   pG  = p + f.pcWG(sG, p, 'sGmax', sGmax);

   % multiplier for mobilities ('transMult' will be ignored here)
   [pvMult, ~, mobMult, pvMult0] = ...
       getMultipliers(strip_field(f, 'transMult'), p, p0);

   % relative permeability
   [krW, krG] = model.evaluateRelPerm({sW, sG}, p, 'sGmax', sGmax);
   krW = krW * mobMult;
   krG = krG * mobMult;

   % Transmissibility
   if isfield(f, 'transMult')
      vols = model.G.cells.volumes .* model.G.cells.H;
      p_eff = compute_effective_mech_pressure(model.G, pW,  sG, sGmax, f, model.gravity);
      
      if isempty(model.split_info)
         % we are running a ppr model to include strain
         div = (model.div0 + vols .* (model.response * (p_eff - model.p0))) ./ vols;
      else
         % we are using an operator-splitting technique to include strain
         assert(nnz(model.response) == 0); 
         div = (model.div0 + model.split_info.actual) ./ vols;
      end
      transMult = f.transMult(p_eff, div);
      %transMult = 1; %@@@
      fprintf('Min/max: %f  / %f\n', min(double(transMult)), ...
              max(double(transMult)));
   else
      transMult = 1;
   end   
   
   trans = s.T .* transMult;

   % Gravity gradient per face, including the gravity component resulting
   % from caprock geometry
   gdz = model.getGravityGradient();

   % Evaluate water and CO2 properties
   [vW, bW, mobW, rhoW, upcw] = ...
       getPhaseFluxAndProps_WGVE(model, pW, pG, krW, trans, gdz, 'W', 0, 0);
   [vG, bG, mobG, rhoG, upcg] = ...
       getPhaseFluxAndProps_WGVE(model, pW, pG, krG, trans, gdz, 'G', 0, 0);
   bW0 = f.bW(pW0);
   bG0 = f.bG(pW0); % Yes, using water pressure also for gas here

   % Multiply upstream b-factors by interface fluxes to obtain fluxes at
   % standard conditions
   bWvW = s.faceUpstr(upcw, bW) .* vW;
   bGvG = s.faceUpstr(upcg, bG) .* vG;


   %% Setting up brine and CO2 equations

   p_eff  = compute_effective_mech_pressure(model.G, pW,  sG, sGmax, f, model.gravity);
   p0_eff = compute_effective_mech_pressure(model.G, pW0, sG0, sGmax0, f, model.gravity);
   
   vols = model.G.cells.volumes .* model.G.cells.H;

   div_new = model.div0 + vols .* (model.response * (p_eff - model.p0)); 
   div_old = model.div0 + vols .* (model.response * (p0_eff - model.p0));
   
   alpha = model.rock.alpha;
   poro  = model.rock.poro;

   tot_vol  = ((s.pv .* pvMult) + alpha .* div_new);
   tot_vol0 = ((s.pv .* pvMult0) + alpha .* div_old);
   
   % Water (Brine)
   eqs{1} = (1./dt) .* (tot_vol .* bW .* sW - tot_vol0 .* bW0 .* sW0) + s.Div(bWvW);
                        
   % Gas (CO2)
   eqs{2} = (1./dt) .* (tot_vol .* bG .* sG - tot_vol0 .* bG0 .* sG0) + s.Div(bGvG);
   
   % apply volume correction if applicable
   if ~isempty(model.split_info)
      correction = model.split_info.actual - model.split_info.estimated;
      eqs{1} = eqs{1} + alpha .* correction .* bW .* sW ./ dt;
      eqs{2} = eqs{2} + alpha .* correction .* bG .* sG ./ dt;
   end

   % Include influence of boundary conditions
   eqs = addFluxesFromSourcesAndBC(model, ...
           eqs, {pW, pG}, {rhoW, rhoG}, {mobW, mobG}, {bW, bG}, {sW, sG}, drivingForces);

   %% Setting up well equations
   if ~isempty(W)
      wm = model.wellmodel;
      if ~opt.reverseMode
         
         if isfield(f, 'permMult')
            % permeability varies.  Update well indices accordingly
            pmult = f.permMult(p, div);
            for i = 1:numel(W)            
               W(i).WI = W(i).WI .* pmult(W(i).cells);
            end
         end
         
         wc = vertcat(W.cells);
         [cqs, weqs, ctrleqs, wc, state.wellSol] =                       ...
             wm.computeWellFlux(model, W, wellSol, bhp                 , ...
                                {qWs, qGs}                             , ...
                                p(wc)                                  , ...
                                [model.fluid.rhoWS, model.fluid.rhoGS] , ...
                                {bW(wc), bG(wc)}                       , ...
                                {mobW(wc), mobG(wc)}                   , ...
                                {sW(wc), sG(wc)}                       , ...
                                {}                                     , ...
                                'allowControlSwitching', false         , ...
                                'nonlinearIteration', opt.iteration);
         % Store the separate well equations (relate well bottom hole
         % pressures to influx)
         eqs(3:4) = weqs;

         % Store the control equations (ensuring that each well has values
         % corresponding to the prescribed value)
         eqs{5} = ctrleqs;

         % Add source term to equations.  Negative sign may be surprising if
         % one is used to source terms on the right hand side, but this is
         % the equations on residual form
         eqs{1}(wc) = eqs{1}(wc) - cqs{1};
         eqs{2}(wc) = eqs{2}(wc) - cqs{2};

      else
         [eqs(3:5), names(3:5), types(3:5)] = ...
             wm.createReverseModeWellEquations(model, state0.wellSol, p0);%#ok
      end
   else
      eqs(3:5) = {bhp, bhp, bhp}; % empty ADIs
   end


   %% Setting up problem
   primaryVars = {'pressure' , 'sG'   , 'qWs'      , 'qGs', 'bhp'};
   types = {'cell'           , 'cell' , 'perf'       , 'perf'     , 'well'};
   names = {'water'          , 'gas'  , 'waterWells' , 'gasWells' , 'closureWells'};
   % if isempty(W)
   %    % Remove names/types associated with wells, as no well exist
   %    types = types(1:2);
   %    names = names(1:2);
   % end

   problem = LinearizedProblem(eqs, types, names, primaryVars, state, dt);

end
% ----------------------------------------------------------------------------

function p_eff = compute_effective_mech_pressure(Gt, pW, sG, sGmax, fluid, g)
                                                 
   % @@ Currently, this function assumes no capillary fringe (i.e. assumption of
   % sharp interface).  For a more accurate computation, use capillary
   % pressure instead. 

   rw = fluid.res_water;
   rg = fluid.res_gas;
   rhoW = fluid.rhoWS * fluid.bW(pW);
   rhoG = fluid.rhoGS * fluid.bG(pW);
   drho = rhoW - rhoG;
      
   fsg = free_sg(sG, sGmax, struct('res_gas', rg, 'res_water', rw));
   h = fsg .* Gt.cells.H ./ (1 - rw);

   % Expression obtained by integrating excess pressure (actual pressure
   % minus initial hydrostatic) from top to bottom and dividing by aquifer
   % height.  (CO2 pressure in plume and water pressure elsewhere.  The
   % computed pressure becomes the excess pressure (above hydrostatic) when
   % pW_top is subtracted (which happens in the main function) 
   
   p_eff = pW + (norm(g) * drho .* h.^2) ./ Gt.cells.H / 2;
   
   %p_eff = pW + norm(g)/2 * (rhoW .* Gt.cells.H + drho .* h.^2 ./ Gt.cells.H);
   
   %p_eff = pW; % @@@@
   
end

% ----------------------------------------------------------------------------

function str = strip_field(str, fieldname)
   if isfield(str, fieldname)
      str = rmfield(str, fieldname);
   end
end
