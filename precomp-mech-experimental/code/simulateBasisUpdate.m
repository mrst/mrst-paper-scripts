function [wellsols, states] = simulateBasisUpdate(initState, coupled_model, schedule)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   num_steps = numel(schedule.step.val);
   
   % solution structures
   wellsols = cell(num_steps, 1);
   states = cell(num_steps+1, 1);
   states{1} = initState; % will be removed at end of function

   % Creating model with pressure responses
   ppr_model = SimplePrecompModel(coupled_model, ...
                                  schedule.control.bc, ...
                                  initState.pressure, ...
                                  'response_type', 'local');
   
   % Creating structures for storing historical information
   p_hist = ones(size(ppr_model.response, 1), 1); % one column per computed pressure field
   div_hist = full(diag(ppr_model.response)); % one column per computed divergence field
   
   it_count = 0;
   
   for step = 1:num_steps
      
      cur_control = schedule.control(schedule.step.control(step));
      dt = schedule.step.val(step);
      
      fprintf('------ Now solving timestep: %i -------\n', step);
      
      [wellsols{step}, states{step+1}, ppr_model, p_hist, div_hist, it] = ...
          solve_timestep(ppr_model               , ...
                         states{step}            , ...
                         cur_control             , ...
                         dt                      , ...
                         coupled_model.operators , ...
                         p_hist                  , ...
                         div_hist);
      
      it_count = it_count + it;
   end
   
   states = states(2:end);

   fprintf('Total iterations: %i.\n M dimension is %i\n', it_count, size(div_hist,2));
   
end

% ----------------------------------------------------------------------------

function [wsol, state, model, p_hist, div_hist, iter_count] = ...
       solve_timestep(model, cur_state, control, dt, mech_op, p_hist, div_hist)
   
   finished       = false;
   x_prev         = [];
   LINSOLVE_TOL   = 1e-10;
   EXIT_TOL = 1e-8; % @@ Look into this
   A              = mech_op.mech.A;
   gP             = mech_op.mech.gradP;
   b              = model.rock.alpha;
   L              = mech_op.extra.precond;
   iter_count     = 0;
   l2             = distance_map(model.G);
   l2_inv         = 1./l2; 
   l2_inv(isinf(l2_inv)) = 0; % removing Inf-values at diagonal elements
   
   while ~finished
      iter_count = iter_count+1;
      % solving the flow equation using the current ppr model as-is
      [wsol, state] = simulateScheduleAD(cur_state, model                   , ...
                                         struct('control', control          , ...
                                                'step', struct('control', 1 , ...
                                                               'val', dt)));
      state = state{1};
      dp = state.pressure - model.p0;
      
      % computing the actual divergence resulting from the obtained pressure
      % field
      x = itersolve_JP(A, gP * (b .* dp)                                           , ...
                       'solver', @pcg                                              , ...
                       'M1', L                                                     , ...
                       'M2', L'                                                    , ...
                       'x0', if_else(~isempty(x_prev), x_prev, zeros(size(A,1),1)) , ...
                       'tol', LINSOLVE_TOL);
      x_prev = x;
      div = mech_op.mech.div * x;
      
      err = max(div - model.response * dp)/ max(div);

      if  err < EXIT_TOL
         % if iter_count == 1
             % p_hist   = [dp, p_hist];
             % div_hist = [div, div_hist];
         % end
         
         finished = true;
      else
         %[model, max_change] = update_responses(model, p_hist, div_hist);
         [model, max_change] = update_responses(model, l2_inv, [dp, p_hist], [div, div_hist]);
         %[model, max_change] = update_responses_inc(model, l2_inv, [dp, p_hist], [div, div_hist]);
      end      
   end
   fprintf('Finished timestep in %i iterations\n', iter_count);
end       

% ----------------------------------------------------------------------------

function [model, max_change] = update_responses_inc(model, l2_inv, p_hist, div_hist)

% Changing response functions to be consistent with all previous
% pressure-divergence pairs, while minimizing width of change.

   n = size(p_hist, 1);
   
   max_change = 0;
   
   % normalizing historical impulses for better numerical stability
   colnorms = sqrt(sum(p_hist.^2, 1));
   p_hist = bsxfun(@rdivide, p_hist, colnorms);
   div_hist = bsxfun(@rdivide, div_hist, colnorms);
   
   prev = model.response * p_hist;
   
   response = 0 * model.response;
   
   for j = 1:n
      % computing reponses in cell j

      pscaled = bsxfun(@times, p_hist, l2_inv(:,j)); % n x k
      
      M = pscaled' * p_hist;

      % diagonal element (scalar)
      d_self = (p_hist(j,:) * (M \ (div_hist(j,:) - prev(j,:))')) / ...
             (p_hist(j,:) * (M \ p_hist(j,:)'));
      self = d_self + model.response(j,j);
      
      div_remain = div_hist(j,:) - prev(j,:) - p_hist(j,:) * d_self; % 1 x k
      
      lambda = - M \ div_remain'; % k x 1
      
      phi = model.response(j,:)' - (p_hist * lambda) .* l2_inv(:,j);
      phi(j) = self;% - model.response(j,j);
      
      
      % phi = - p_hist * lambda;  % (n x k) * (k x 1) -> (n x 1)
      % phi = phi .* l2_inv(:,j);
      % phi = phi' + model.response(j,:);
      
      % phi(j) = self;
     
      %max_change = max(max_change, max(abs(model.response(:,j) - res)));
      
      response(j,:) = phi';
      
      %model.response(j,:) = model.response(j,:) + phi';
   end
   model.response = response;
end


% ----------------------------------------------------------------------------

function [model, max_change] = update_responses(model, l2_inv, p_hist, div_hist)

% Minimizing width of individual response functions, under the constraint that
% the new set of responses is consistent with _all_ previous pressure-divergence
% pairs.

   n = size(p_hist, 1);

   % normalizing historical impulses for better numerical stability
   colnorms = sqrt(sum(p_hist.^2, 1));
   p_hist = bsxfun(@rdivide, p_hist, colnorms);
   div_hist = bsxfun(@rdivide, div_hist, colnorms);
   
   max_change = 0;
   
   for j = 1:n
      % computing reponses in cell j
      
      pscaled = bsxfun(@times, p_hist, l2_inv(:,j)); % n x k
      
      M = pscaled' * p_hist;
      %      M_inv = inv(M);  % k x k
      
      % diagonal element (scalar)
      self = (p_hist(j,:) * (M \ div_hist(j,:)')) / ...
             (p_hist(j,:) * (M \ p_hist(j,:)'));
      
      div_remain = div_hist(j,:) - p_hist(j,:) * self; % 1 x k
      
      lambda = - M \ div_remain'; % k x 1
      
      phi = -p_hist * lambda;  % (n x k) * (k x 1) -> (n x 1)
      phi = phi .* l2_inv(:,j);
      phi(j) = self;
     
      %max_change = max(max_change, max(abs(model.response(:,j) - res)));
      
      model.response(j,:) = phi;
   end

end
% ----------------------------------------------------------------------------

% function [model, max_change] = update_responses(model, p_hist, div_hist)

% % Minimizing change from last set of responses, under the constraint that the
% % new set of responses is consistent with _all_ previous pressure-divergence
% % pairs.
   
%    M = p_hist' * p_hist;
   
%    div_residuals = (div_hist - model.response * p_hist)';
  
%    res_change = p_hist * (M \ div_residuals);
   
%    max_change = max(abs(res_change(:))) / max(abs(model.response(:)))
   
%    % Updating model response functions
%    model.response = model.response + res_change';
   
% end

% ----------------------------------------------------------------------------

function l = distance_map(G)

   n = G.cells.num;
   l = zeros(n,n);
   for i = 1:n
      dvec = bsxfun(@minus, G.cells.centroids, G.cells.centroids(i,:));
      l(:,i) = sum(dvec.^2, 2);
   end
end

% ----------------------------------------------------------------------------

function res = if_else(cond, yes, no)
   if cond
      res = yes;
   else
      res = no;
   end
end
