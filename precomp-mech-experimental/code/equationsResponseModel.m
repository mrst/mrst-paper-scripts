function [problem, state] = equationsResponseModel(state0, state, model, dt, drivingForces, varargin)
% Get linearized problem for oil/water system with black oil-style properties

%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
   opt = struct('Verbose'     , mrstVerbose , ...
                'reverseMode' , false       , ...
                'scaling'     , []          , ...
                'resOnly'     , false       , ...
                'iteration'   , -1          , ...
                'stepOptions' , []          , ...
                'addflux'     , false); % Compatibility only

   opt = merge_options(opt, varargin{:}); 

   W = drivingForces.W; 
   assert(isempty(drivingForces.src))
   bc = drivingForces.bc; 

   s = model.operators; 
   G = model.G; 
   f = model.fluid; 

   % suffix 'b2' indicates that pressure is expressed in the second
   % ('smooth') basis
   [p_b2, wellSol] = model.getProps(state, 'pressure', 'wellsol'); 

   [p0_b2] = model.getProps(state0, 'pressure'); 

   pBH = vertcat(wellSol.bhp); 
   qWs = vertcat(wellSol.qWs);

   % ----------------- Initialization of independent variables ---------------

   if ~opt.resOnly, 
      % ADI variables needed since we are not only computing residuals.
      if ~opt.reverseMode, 
         [p_b2, qWs, pBH] = initVariablesADI(p_b2, qWs, pBH); 
      else
         [p0_b2, tmp] = initVariablesADI(p0_b2, sW0,...
                                      zeros(size(qWs)),...
                                      zeros(size(qOs)),...
                                      zeros(size(pBH))); % #ok
      end
   end
   primaryVars = {'pressure', 'qWs', 'bhp'}; 

   % computing pressure in usual basis (cell indicators)
   if ~isempty(model.imp_mat) 
      p = model.imp_mat * p_b2;
      p0 = model.imp_mat * p0_b2;
   else
      p = p_b2;
      p0 = p0_b2;
   end

   if isfield(model, 'p_ref')
      p_ref = model.p_ref; % backwards compatibility with the old PrecompResponeModel
   else
      p_ref = model.p0;
   end

   clear tmp;
   g = model.getGravityVector(); 
   dzg = s.Grad(G.cells.centroids) * g'; 
   
   % ------------------------------------------------------------------------- 
   
   % check for p - dependent tran mult:
   trMult = 1; 
   if isfield(f, 'tranMultR'), trMult = f.tranMultR(p); end

   % check for p - dependent porv mult:
   pvMult = 1; pvMult0 = 1; 
   if isfield(model.rock, 'pvMultR')
      pvMult  = model.rock.pvMultR(p);
      pvMult0 = model.rock.pvMultR(p0);
      if isfield(f, 'pvMultR')
         warning('pvMultR from rock overriding pvMultR specified in fluid object.');
      end
   elseif isfield(f, 'pvMultR')
      pvMult  = f.pvMultR(p); 
      pvMult0 = f.pvMultR(p0); 
   end
   transMult = 1; 
   if isfield(f, 'transMult')
      avg_map = model.irmap;
      div = model.div0 + avg_map' * ...
            (model.pillar_vols .* (model.response * (avg_map * (p_b2 - p_ref))));
      transMult = f.transMult(p, div ./ G.cells.volumes); 
      fprintf('Min/max: %f  / %f\n', min(double(transMult)), max(double(transMult)));
   end
   
   trans = s.T .* transMult; 
   % ------------------------------------------------------------------------- 

   % water props (calculated at oil pressure OK?)
   bW    = f.bW(p); 
   rhoW  = bW .* f.rhoWS; 
   % rhoW on face, average of neighboring cells (E100, not E300)
   rhoWf = s.faceAvg(rhoW); 
   mobW  = trMult ./ f.muW(p); 
   dpW   = s.Grad(p) - rhoWf .* dzg; 
   % water upstream - index
   upc   = (double(dpW) <= 0); 
   bWvW  = -s.faceUpstr(upc, bW .* mobW) .* trans .* dpW; 
   if(opt.addflux)
      state.bWvW = bWvW; 
      state.WvW  = s.faceUpstr(upc, mobW) .* trans .* dpW; 
   end
   
   % ================================= EQUATIONS =================================
   
   % ------------------------------ water equation ------------------------------

   % divergence response matrix is expressed in terms of 'smooth' basis, so
   % we have to use the original 'p_b2' vector rather than 'p'.
   assert(all(abs(unique(sum(model.irmap, 2)) - 1) <= eps(1)));
   avg_map = model.irmap;
   %avg_map = bsxfun(@rdivide, model.irmap, sum(model.irmap, 2));
   
   div_new = model.div0 + avg_map' * ...
             (model.pillar_vols .* (model.response * (avg_map * (p_b2  - p_ref))));
   div_old = model.div0 + avg_map' * ...
             (model.pillar_vols .* (model.response * (avg_map * (p0_b2 - p_ref))));
   alpha   = model.rock.alpha; 
   poro    = model.rock.poro; 

   eqs{1} = (1 ./ dt) .*                                                            ...
            ((poro .* (G.cells.volumes .* pvMult)  + alpha .* div_new) .* bW -      ...
             (poro .* (G.cells.volumes .* pvMult0) + alpha .* div_old) .* f.bW(p0)) ...
            + s.Div(bWvW); 

   % Handling pressure boundary conditions, if present
   % if(~isempty(bc))
   %    if(isfield(bc, 'cell2bcface'))
   %       pX = {p}; rhoX = {rhoW}; mobX = {mobW}; bX = {bW}; 
   %       bXqXbc = pressureBCContribADI(G, s, pX, rhoX, mobX, bX, bc); 
   %       for i = 1:numel(bXqXbc)
   %          eqs{i} = eqs{i} + bc.bcface2cell * bXqXbc{i}; 
   %       end
   %    else
   %       pX = {p}; rhoX = {rhoW}; mobX = {mobW}; bX = {bW}; 
   %       [bXqXbc, bc_cell] = pressureBCContrib(G, s, pX, rhoX, mobX, bX, bc); 
   %       for i = 1:numel(bXqXbc)
   %          eqs{i}(bc_cell) = eqs{i}(bc_cell) + bXqXbc{i}; 
   %       end
   %    end
   % end
   eqs = addFluxesFromSourcesAndBC(model, ...
                                   eqs, ...
                                   {p},...
                                   {rhoW},...
                                   {mobW}, ...
                                   {bW},  ...
                                   {ones(numel(p0),1)}, ...
                                   drivingForces);

   types = {'cell'};
   
   % ------------------------------ well equations ------------------------------
   
   if ~isempty(W)
      if ~opt.reverseMode
         wc   = vertcat(W.cells); 
         pw   = p(wc); 
         rhos = [f.rhoWS]; 
         bw   = {bW(wc)}; 
         mw   = {mobW(wc)}; 
         sat  = {1}; 

         wm = WellModel(); 
         [cqs, weqs, ctrleqs, wc, state.wellSol] =                     ...
             wm.computeWellFlux(model, W, wellSol,                     ...
                                pBH, {qWs}, pw, rhos, bw, mw, sat, {}, ...
                                'nonlinearIteration', opt.iteration,   ...
                                'referencePressureIndex', 1); 
         eqs(2) = weqs; 
         eqs{3} = ctrleqs; 
         
         eqs{1}(wc) = eqs{1}(wc) - cqs{1}; 
         names(2:3) = {'waterWells', 'closureWells'}; 
         types(2:3) = {'perf', 'well'}; 
      else
         % in reverse mode just gather zero - eqs of correct size
         for eqn = 2:3
            nw       = numel(state0.wellSol); 
            zw       = double2ADI(zeros(nw, 1), p0); 
            eqs(2:3) = {zw, zw}; 
         end
         names(2:3) = {'empty', 'empty'}; 
         types(2:3) = {'none', 'none'}; 
      end
   else  % no wells
      eqs(2:3)   = {pBH, pBH}; % empty  ADIs
      names(2:3) = {'empty', 'empty'}; 
      types(2:3) = {'none', 'none'}; 
   end
   
   % ---------------------------- setting up problem ----------------------------
   problem = LinearizedProblem(eqs, types, names, primaryVars, state, dt); 
   problem.iterationNo = opt.iteration; 
end
