function [model, initState, schedule, grid] = setupSimpleTestCase(varargin)
   
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   
   % Defining simulation options
   opt = set_default_options();
   opt = merge_options(opt, varargin{:});

   g = gravity;
   if norm(g) == 0
      warning('Gravity off!');
   end

   % if separate parameters for overburden have not been provided, assume
   % caprock extends all the way up to the surface.  In that case, insert
   % parameters to specify a zero extent overburden
   opt = ensure_overburden_parameters_present(opt);
   
   %% Loading or constructing grid
   if ~isempty(opt.gridfile)
      if isstruct(opt.gridfile)
         % Assume this is a grid
         grid = opt.gridfile;
      else
         % We got a filename to a predefined grid
         grid = load(opt.gridfile);
      end
   else
      switch opt.grid_type
        case 'cartesian'
          grid = setup_cartesian_grid(opt);     
        otherwise
          grid = setup_triangle_grid(opt);
      end
   end

   if grid.G.griddim == 2
      g = g(2:3);
   end
   
   if isempty(opt.bc_fun_t)
      opt.bc_fun_t = if_else(grid.G.griddim==2       , ...
                             {0, opt.top_pressure,0} , ...
                             {0, 0, opt.top_pressure  }); 
   end


   %% If there are no-flow cells, compute initial deformation in advance
   % It is better to do it here than further down, for memory reasons
   if opt.restrict_flow_to_aquifer
      tmp_opt = opt;
      tmp_opt.restrict_flow_to_aquifer = false;
      tmp_opt.ilu_tol = NaN; % do not spend time computing ILU preconditioner here
      optargs = interleave(fields(tmp_opt), struct2cell(tmp_opt));
      [~, tmp_state ] = setupSimpleTestCase(optargs{:});
      uu = tmp_state.uu;
      clear('tmp_state'); % free memory occupied by tmp_state here
   else
      uu = [];
   end
   
   %% Setting up other necessary data structures
   
   [rock, nf_cells]       = setup_rock            (grid, opt);
   fluid                  = setup_fluid           (opt);
   mech                   = setup_mechanics       (opt, grid, nf_cells);
   S                      = higherOrderEnergies(grid.G, mech.Ev, mech.nuv);
   model                  = WaterMechanicModelVEM (grid.G, rock, fluid, mech          , ...
                                                  'gravity'  , g                      , ...
                                                  'S', S, ...
                                                  'ilu_tol', opt.ilu_tol, ...
                                                  'nf_cells' , nf_cells);
   %                       'alpha_scaling', nullspace_scaling , ...
   [initState, bc, model] = setup_state_and_bc(opt, grid, model, nf_cells, opt.ipmodfun, uu);
   schedule               = setup_schedule        (opt, model, initState, bc, grid);

   % @ Increasing convergence tolerance to prevent possible noise effects
   model.nonlinearTolerance = 1e-11;%1e-8;%1e-10; %1e-11;@@@
   
   % Adding strain-dependent permeability, if requested
   if opt.strain_dependent_perm
      vs0 = (model.operators.mech.div * initState.x) ./ model.G.cells.volumes;
      [model.fluid.transMult, model.fluid.permMult] = ...
          makeStrainDependentTransmult(model.G, rock, initState.pressure, vs0);
   end
end

% ----------------------------------------------------------------------------

function opt = set_default_options()

   opt.dim = 2; % should be 2, 3 or -3 (-3 for a 3D grid with only 1 cell in y direction)
   
   % ------------------------ Simulator behavior options ------------------------

   opt.restrict_flow_to_aquifer = true;       % Otherwise, flow will be allowed
                                              % also in over/underburden .
   opt.convert_to_undrained_in_noflow = true; % Whether to convert elastic
                                              % parameters in noflow zones to
                                              % undrained versions.
   opt.zero_initial_displacement = false;     % If 'false', displacements will be
                                              % computed so that the mechanical
                                              % subsystem is at equilibrium
                                              % before simulation start.
   opt.strain_dependent_perm = false;         % Make permeability a function of
                                              % volumetric strain.
   
   % -------------- Reservoir properties [caprock, aquifer, bottom] --------------
   % If a fourth entry is provided, representing an overburden zone (above caprock), it
   % must be provided for all of the following parameters:
   % 'perm', 'poro', 'E', 'nu', 'alpha', 'zres' and 'thickness'
   opt.perm  = [1e-3 , 100 , 1e-3] * milli * darcy; % permeability
   opt.poro  = [0.3  , 0.3 , 0.3 ];                 % porosity
   opt.E     = [10   , 1   , 10  ] * giga * Pascal; % Young's module
   opt.nu    = [0.3  , 0.3 , 0.3 ];                 % Poisson's ratio
   opt.alpha = [1.0  , 1.0 , 1.0 ];                 % Biot's coefficient

   % ------------------------- Other physical properties -------------------------
   
   opt.top_pressure   = 1 * barsa;              % Pressure at z=0 (earlier pore_press_top)
   opt.rho_water      = 1e3;                    % reference water density (kg/m3) 
   opt.rho_rock       = 3e3;                    % rock density (kg/m3)
   opt.c_water        = 4e-5 / barsa;           % water compressibility
   opt.mu_water       = 8e-4 * Pascal * second; % water viscosity
   
   % ------------------ boundary condition-related parameters ------------------
   % -- for mechanics boundary type --
   % 'D'   - Dirichlet type (specified displacement)
   % 'N'   - Neumann type   (specified traction)
   % <fun> - Dirichlet indicator function (takes coordinate, returns 'true' if
   %         Dirichlet boundary conditions should be used here (otherwise
   %         Neumann)
   % -- for mechanics boundary functions -- 
   % <scalar> - constant function
   % <fun>    - function of position

   % In the 2D case, only the two first cell array values below will be used
   opt.bc_l  = {'D', 'D', 'D'}; % boundary type, left side
   opt.bc_r  = {'D', 'D', 'D'}; % boundary type, right side
   opt.bc_fr = {'D', 'D', 'D'}; % boundary type, front (if 3D)
   opt.bc_ba = {'D', 'D', 'D'}; % boundary type, back (if 3D) 
   opt.bc_b  = {'D', 'D', 'D'}; % boundary type, bottom side
   opt.bc_t  = {'N', 'N', 'N'}; % boundary type, top side
   opt.bc_fun_l  = {0, 0, 0};   % boundary functions, left
   opt.bc_fun_r  = {0, 0, 0};   % boundary functions, right
   opt.bc_fun_fr = {0, 0, 0};   % boundary functions, front (if 3D)
   opt.bc_fun_ba = {0, 0, 0};   % boundary functions, back (if 3D)
   opt.bc_fun_b  = {0, 0, 0};   % boundary functions, bottom
   opt.bc_fun_t  = {};          % boundary functions, top (if left empty, set
                                % to opt.top_pressure   during execution)
                                              
   % -- for flow equation --
   % 'D' - constant pressure imposed (Dirichlet)
   % 'N' - no-flow (Neumann)
   opt.bc_flow = {'D', 'D', 'N', 'N'};
   %opt.bc_flow = {'D', 'D', 'D', 'D', 'N', 'N'};  % condition at right and left side,
                                                  % front and back, and top and bottom
                                                  % two middle ones two only
                                                  % used in the 3D case) 

   % -------------------------- well-related parameters --------------------------

   opt.well_pos = [0.5];       %#ok relative position within aquifer, one entry per well
   opt.well_val = [1 * barsa]; %#ok rate, or bhp differential
   opt.well_type = {'bhp'};    % 'rate' or 'bhp'
   opt.well_rad = 0.01;        % well radius
   opt.multi_perf = true;      % should there be perforations at _all_
                               % aquifer cells along vertical axis of
                               % wellbore, or just at the bottom?
   
   % ------------------------------ Grid parameters ------------------------------

   opt.gridfile       = [];          % If this field contains a filename, the grid
                                     % will be loaded from this file rather than
                                     % constructed internally.
   opt.grid_type      = 'cartesian'; % discretization type ('cartesian',
                                     % 'structured_trimesh')
   opt.disturb        = 0;           % Amount of distortion in the grid (currently
                                     % only used for 2D non-cartesian grids)
   opt.ensure_flat_iface = false;    % only relevant for non-cartesian grids
   opt.xres           = 20;          % lateral resolution
   opt.length         = 200;         % phyiscal length (lateral)
   
   opt.zres           = [10, 2, 8]; % [caprock, aquifer, bottom] vertical
                                    % resolution. With a 4-component vector, an
                                    % overburden zone (above caprock) is also
                                    % separately specified.
   opt.thickness      = [10, 2, 8]; % total vertical thickness. With a 4-component
                                    % vector, an overburden zone (above caprock)
                                    % is also separately specified.
   opt.buffercells    = 0;          % if >0, aquifer does not extend all the
                                    % way out to boundary
   
   % ---------------------------- schedule parameters ----------------------------

   opt.T        = 10 * day; % Total simulation time (if zero, will only
                            % initialize state, no timesteps will be run).
                            % This parameter will be ignored if 'tsteps', is
                            % a vector rather than a scalar (see below)
   opt.tsteps   = 10; % Total number of timesteps.  If a vector is provided,
                      % it is interpreted as being the exact times for which
                      % to compute the state.  In that case, 'opt.T', will be ignored.
   
   % ------------------------------- Misc. options -------------------------------
   opt.ipmodfun = @(x) 0; % modification of initial pressure, as function of
                          % (x,z)-coordinate
   opt.ilu_tol = 1e-4; % tolerance for ilu preconditioning computation taking
                       % place when constructing a WaterMechanicModelVEM object.
end

% ----------------------------------------------------------------------------

function opt = ensure_overburden_parameters_present(opt)

   % For backwards compatibility reasons, this function had to be added when
   % introducing a separate overburden layer.
   
   fields = {'zres', 'thickness', 'perm', 'poro', 'E', 'nu', 'alpha'};
   num_zones = numel(opt.zres);
   cellnum = if_else(opt.dim==2 || opt.dim==-3, opt.xres * sum(opt.zres), ...
                                 opt.xres.^2 * sum(opt.zres));
   assert(all((cellfun(@(x) numel(opt.([x])), fields)) == num_zones | ...
              (cellfun(@(x) numel(opt.([x])), fields)) == cellnum));
   
   if num_zones == 4
      % nothing to be done.  Overburden parameters already present.  
      % Just return.
      return
   else
      assert(num_zones == 3); 
      % overburden parameters are lacking.  Insert placeholders
      for f = fields
         f = f{:};
         val = opt.([f]);
         if numel(val) == cellnum;
            continue;
         end
         opt.([f]) = [0, val(:)'];
      end
   end
            
end

% ----------------------------------------------------------------------------

function grid = setup_cartesian_grid(opt)

   zres  = sum(opt.zres);  % total vertical resolution (cap + aquifer + bot)

   if opt.dim == 2
      G = cartGrid([opt.xres, zres], [opt.length, 1]);
      layernodenum = opt.xres + 1;
      layercellnum = opt.xres;
      
      % assigning role of layers
      tmp = zeros(opt.xres, zres);
      buf = opt.buffercells;
      zix = cumsum(opt.zres);
      tmp(:, 1:zix(1)) = 1; % overburden
      tmp(:, (zix(1)+1):zix(3)) = 2; % caprock
      tmp(:, (zix(3)+1):zix(4)) = 4; % bedrock
      tmp(buf+1:end-buf, zix(2)+1:zix(3)) = 3; % aquifer
   else
      assert(abs(opt.dim) == 3)
      yres = if_else(opt.dim > 0, opt.xres, 1);
      ylen = if_else(opt.dim > 0, opt.length, 100);%opt.length/opt.xres);
      G = cartGrid([opt.xres, yres, zres], [opt.length, ylen, 1]); 
      layernodenum = (opt.xres + 1) * (yres + 1);   
      layercellnum = opt.xres * yres;
      
      % assigning role of layers
      tmp = zeros(opt.xres, yres, zres);
      zix = cumsum(opt.zres);
      bufx = opt.buffercells;
      bufy = if_else(opt.dim > 0, bufx, 0);
      tmp(:, :, 1:zix(1)) = 1; % overburden
      tmp(:, :, (zix(1)+1):zix(3)) = 2; % caprock
      tmp(:, :, (zix(3)+1):zix(4)) = 4; % bedrock
      tmp(bufx+1:end-bufx, bufy+1:end-bufy, zix(2)+1:zix(3)) = 3; % aquifer]
   end

   grid.ob_cells = find(tmp(:)==1);
   grid.top_cells = find(tmp(:)==2);
   grid.aq_cells = find(tmp(:)==3);
   grid.bot_cells = find(tmp(:)==4);
   
   z = compute_requested_z_coords(layernodenum, opt.zres, opt.thickness);
   G.nodes.coords(:,end) = z;

   if G.griddim == 3
      G = mcomputeGeometry(G);
   else
      G = computeGeometry(G);
   end
   
   grid.G = G;

   % grid.ob_cells  = [1:(layercellnum*opt.zres(1))]';
   % grid.top_cells = [1:(layercellnum*opt.zres(2))]' + numel(grid.ob_cells);
   % grid.aq_cells  = [1:(layercellnum*opt.zres(3))]' + max(grid.top_cells);
   % grid.bot_cells = [1:(layercellnum*opt.zres(4))]' + max(grid.aq_cells);
                                                      
end

% ----------------------------------------------------------------------------

function grid = setup_triangle_grid(opt)

   assert(opt.dim == 2); % currently only implemented in 2D (but should be
                         % easily extendable)
   assert(opt.buffercells == 0); % buffercell support not yet implemented for
                                 % triangle grids
   zres = sum(opt.zres); 
   TOL = 1e-8;
   
   % create one grid
   G = squareGrid([opt.xres, zres], [opt.length, sum(opt.thickness)], ...
                  'grid_type', opt.grid_type, ...
                  'disturb', opt.disturb);

   grid.G = computeGeometry(G);
   
   field = zeros(G.cells.num, 1);
   zones = cumsum(opt.thickness);
   field(grid.G.cells.centroids(:,2) < zones(4)) = 1;
   field(grid.G.cells.centroids(:,2) < zones(3)) = 2;
   field(grid.G.cells.centroids(:,2) < zones(2)) = 3;
   field(grid.G.cells.centroids(:,2) < zones(1)) = 4;
   
   grid.ob_cells  = find(field == 4);
   grid.top_cells = find(field == 3);
   grid.aq_cells  = find(field == 2);
   grid.bot_cells = find(field == 1);
   
   if opt.ensure_flat_iface
      grid.G = straighenInterfaces(grid.G, field);
   end   
end

% ----------------------------------------------------------------------------

function G = straighenInterfaces(G, field)

   error(['this function may be deprecated since the addition of a separate ' ...
          'overburden zone to the grid model.  It should be reviewed ' ...
          'before continued use.']);
   
   field0 = [0; field];
   neigh_field = reshape(field0(G.faces.neighbors(:)+1), [], 2);
   iface_edges = find((neigh_field(:,1) ~= neigh_field(:,2)) & (prod(neigh_field, 2) ~= 0));
   
   iface1_edges = iface_edges(find((field(G.faces.neighbors(iface_edges,1))==1) | ...
                                   (field(G.faces.neighbors(iface_edges,1))==1)));
   
   iface2_edges = iface_edges(find((field(G.faces.neighbors(iface_edges,1))==3) | ...
                                   (field(G.faces.neighbors(iface_edges,1))==3)));
   
   iface1_cells = G.faces.neighbors(iface1_edges,:);
   iface2_cells = G.faces.neighbors(iface2_edges,:);
   
   
   z1 = mean(G.cells.centroids(iface1_cells(:), end));
   z2 = mean(G.cells.centroids(iface2_cells(:), end));
   
   node1_ix = G.faces.nodes(mcolon(G.faces.nodePos(iface1_edges), ...
                                   G.faces.nodePos(iface1_edges)+1));
   node2_ix = G.faces.nodes(mcolon(G.faces.nodePos(iface2_edges), ...
                                   G.faces.nodePos(iface2_edges)+1));
   G.nodes.coords(node1_ix, end) = z1;
   G.nodes.coords(node2_ix, end) = z2;
   
   G = computeGeometry(G);
end

% ----------------------------------------------------------------------------

function G = shiftGridDepth(G, depth)
   G.nodes.coords(:,end) = G.nodes.coords(:,end) + depth;
   G = computeGeometry(G);
end

% ----------------------------------------------------------------------------
function z = compute_requested_z_coords(layernodenum, zres, zvals)

   % ensure all column vectors
   zvals = zvals(:); zres = zres(:);
   
   stride = zvals(:) ./ zres(:);
   stride(isnan(stride)) = 1; % handle the case of zero-sized overburden
   
   % fix due to roundoff error
   adj = stride .* zres < zvals; 
   stride(adj) = stride(adj) + eps * zvals(adj);
   
   depths = [0; cumsum(zvals)];
   zvals  = unique([mcolon(depths(1:end-1), depths(2:end), stride), depths(end)]);
   z      = rldecode(zvals', layernodenum);
   
end


% ----------------------------------------------------------------------------

function [rock, nf_cells] = setup_rock(grid, opt)
      
   rock.perm  = set_cellvals(opt.perm,  grid);
   rock.poro  = set_cellvals(opt.poro,  grid);
   rock.alpha = set_cellvals(opt.alpha, grid); 

   % identify cells with low enough permeability to be considered no-flow
   nf_cells = false(grid.G.cells.num, 1);
   if opt.restrict_flow_to_aquifer
      nf_cells(grid.ob_cells) = true;
      nf_cells(grid.top_cells) = true;
      nf_cells(grid.bot_cells) = true;
   end

   if any(nf_cells)
      rock.perm    = rock.perm(~nf_cells);
      rock.poro    = rock.poro(~nf_cells);
      rock.alpha   = rock.alpha(~nf_cells);
   end

   % computing pore volume multiplier
   E  = set_cellvals(opt.E, grid);
   nu = set_cellvals(opt.nu, grid);
   
   rock.pvMultR = constructPvMultR(rock.alpha    , ...
                                   E(~nf_cells)  , ...
                                   nu(~nf_cells) , ...
                                   rock.poro     , ...
                                   opt.top_pressure  );
end


% ----------------------------------------------------------------------------

function fluid = setup_fluid(opt)
   
   fluid.rhoWS = opt.rho_water;       
   muW         = opt.mu_water;
   fluid.muW   = @(p) 0 * p + muW; 
   fluid.bW    = @(p) 1 + (p - opt.top_pressure  ) * opt.c_water;
end   

% ----------------------------------------------------------------------------

function mech = setup_mechanics(opt, grid, nf_cells)

   Ev        = set_cellvals(opt.E, grid);
   nuv       = set_cellvals(opt.nu, grid);

   if opt.convert_to_undrained_in_noflow
      % adjusting mechanical properties for no-flow cells (effective lambda will change)
      [lambda, mu]  = ENu2LMu_3D(Ev, nuv); 
      [zeta, alpha] = compute_zetas(opt, Ev, nuv, grid, nf_cells);
      [Ev, nuv]     = LMu2ENu_3D(lambda + alpha .* zeta, mu);
   end
      
   % Setting up boundary conditions
   G = grid.G;
   if G.griddim==2
      el_bc = setMechBC(G, ...
              [opt.bc_l; opt.bc_r; opt.bc_t; opt.bc_b], ...
              [opt.bc_fun_l; opt.bc_fun_r; opt.bc_fun_t; opt.bc_fun_b]);
   else
      el_bc = setMechBC(G, ...
              [opt.bc_l; opt.bc_r; opt.bc_fr; ...
               opt.bc_ba; opt.bc_t; opt.bc_b], ...
              [opt.bc_fun_l; opt.bc_fun_r; opt.bc_fun_fr; ...
               opt.bc_fun_ba; opt.bc_fun_t; opt.bc_fun_b]);
   end
   
   % Load term represents gravity 
   poro    = set_cellvals(opt.poro, grid);
   
   if isfield(G.cells, 'nodePos') %@@@
      nodes_per_cell = diff(G.cells.nodePos); 
      % nodes_per_cell = unique(diff(G.cells.nodePos)); 
      % assert(numel(nodes_per_cell) == 1);
   else
      nodes_per_cell = 2^G.griddim;% @@@
   end
   rho_avg = rldecode((1-poro) * opt.rho_rock + poro * opt.rho_water, nodes_per_cell);
   %rho_avg = project_cellfield_to_nodes(G, (1-poro) * opt.rho_rock + poro * opt.rho_water);
   if grid.G.griddim == 2
      load = @(x) norm(gravity) * [zeros(size(rho_avg)), rho_avg];
   else
      load = @(x) norm(gravity) * [zeros(size(rho_avg)), zeros(size(rho_avg)), rho_avg];
   end

   % Assembling return structure
   mech = struct('Ev', Ev, 'nuv', nuv, 'el_bc', el_bc, 'load', load);
   
end

% % ----------------------------------------------------------------------------

% function nfield = project_cellfield_to_nodes(G, cfield)

%    % first computing face field
%    tmp = [NaN; cfield];
%    ffield = tmp(G.faces.neighbors+1); % original zero-indices become NaNs
%    nanix = isnan(ffield(:,1));
%    ffield(nanix, 1) = ffield(nanix,2); % getting rid of nans in 1st column
%    nanix = isnan(ffield(:,2));
%    ffield(nanix, 2) = ffield(nanix,1); % getting rid of nans in 2nd column
%    ffield = mean(ffield, 2);
   
%    % now computing node field
%    nfield = accumarray(G.faces.nodes, rldecode(ffield, diff(G.faces.nodePos)));
%    count  = accumarray(G.faces.nodes, 1);
%    nfield = nfield ./ count;
   
% end

% ----------------------------------------------------------------------------

function f = memoize1(F)
   
   mapObj = containers.Map('KeyType', 'double', 'ValueType', 'double');
   
   % x = []; % input
   % y = {}; % resulting output
   f = @inner;
   function out = inner(in)
      if mapObj.isKey(in)
         out = mapObj(in);
      else
         out = F(in);
         out = out.y(end);
         mapObj(in) = out;
         end 
      % ind = find(in==x);
      % if isempty(ind)
      %    out = F(in);
      %    x(end+1) = in;
      %    y{end+1} = out;
      % else
      %    out = y{ind};
      % end
   end
end

% ----------------------------------------------------------------------------

function [initState, bc, model] = setup_state_and_bc(opt, grid, model, nf_cells, ipmodfun, uu)
   
   % Function generating hydrostatic pressure 
   
   integrate_pressure = ...
       memoize1(@(z) numIntHStaticPressure(0, ...
                                           opt.top_pressure, ...
                                           z, ...
                                           @(p) model.fluid.bW(p) * model.fluid.rhoWS));
      
   function p = hydrostatic(z)
      
      p = zeros(numel(z),1);
      for i = 1:numel(p)
         if z(i) == 0
            p(i) = opt.top_pressure;
         else
            %tmp = numIntHStaticPressure(0, top_p, z(i), rho_fun); 
            p(i) = integrate_pressure(z(i));
            %p(i) = tmp.y(end); 
         end
      end
   end

   %   p = @(z) opt.top_pressure   + z .* opt.rho_water .* norm(gravity);
   initState.pressure = hydrostatic(model.G.cells.centroids(:,end)) + ...
                                    ipmodfun(model.G.cells.centroids);

   % setting up boundary conditions on left/right side
   bc = [];
   for i = 1:if_else(model.G.griddim == 2, 4, 6)
      if opt.bc_flow{i} == 'D' % constant pressure (otherwise, no-flow)
         bfaces = boundaryFacesAndNodes(model.G, [i==1, i==2, i==3, i==4, i==5, i==6]);
         fpress = hydrostatic(model.G.faces.centroids(bfaces,end)) + ...
                              ipmodfun(model.G.faces.centroids(bfaces,:));
         bc = addBC(bc, bfaces, 'pressure', fpress, 'sat', 1);
         % bcells = sum(model.G.faces.neighbors(bfaces, :), 2);
         % bc = addBC(bc, bfaces, 'pressure', initState.pressure(bcells), 'sat', 1);
      end
   end

   % Setting up initial displacements.  We need the full grid for this, so if
   % no-flow cells are present, we need to rerun the initialization part of the
   % script with all cells activated, and copy the resulting fields.
   if any(nf_cells)
      assert(~isempty(uu)); % should have been precomputed outside function
                            % and passed along in this case
      initState = model.initDisplacement(initState, uu, 'mech_equil', false);
      
      % Modify load term to ensure the initial deformation represent rest
      % state (assume cell-based integration in 'VEM_linElast')
      [   ~, alpha] = compute_zetas(opt, model.mech.Ev, model.mech.nuv, grid, nf_cells);

      model.operators.mech.rhs = ...
          model.operators.mech.A * initState.x - ...
          model.operators.mech.gradP * (alpha(~nf_cells) .* initState.pressure) + ...
          model.addBcFromFluid(bc);
   else
      uu = zeros(model.GMech.nodes.num, model.GMech.griddim);
      initState = model.initDisplacement(initState, uu                   , ...
                                         'pressure' , initState.pressure , ...
                                         'flow_bc'  , bc, ...
                                         'mech_equil', ~opt.zero_initial_displacement);
   end
,end

% ----------------------------------------------------------------------------

function schedule = setup_schedule(opt, model, state, bc, grid)

   % setting up wells   
   W = [];
   
   % expanding well_val and well_type if they are only given as scalars
   num_wells = numel(opt.well_pos);
   if isscalar(opt.well_val) && num_wells > 1
      opt.well_val = repmat(opt.well_val, num_wells, 1);
   end
   if isscalar(opt.well_type) && num_wells > 1
      well_type = opt.well_type;
      opt.well_type = cell(num_wells, 1);
      for i = 1:num_wells
         opt.well_type(i) = well_type;
      end
   end
   
   %% Setting up schedule
   for i = 1:numel(opt.well_pos)
      assert(numel(opt.well_val)  == numel(opt.well_pos));
      assert(numel(opt.well_type) == numel(opt.well_type));

      z_ixs = (sum(opt.zres(1:2)) + 1):(sum(opt.zres(1:3)));
      if ~opt.multi_perf
         z_ixs = z_ixs(end);
      end
            
      % compute well cell indices (in global grid) 
      xres = model.G.cartDims(1);
      yres = model.G.cartDims(2);
      if model.G.griddim==2
         x_ixs = repmat(ceil(xres * opt.well_pos(i)), 1, numel(z_ixs));
         wc = sub2ind(model.G.cartDims, x_ixs, z_ixs);
                                        
         % wc = sub2ind(model.G.cartDims, floor(xres*opt.well_pos(i)), ...
         %                                sum(opt.zres(1:(end-1))));
      else
         pos = opt.well_pos(i, :);
         if pos(1) >= 1 % cell index given
            wc = grid.aq_cells(pos);
         else % relative position given                       
            if numel(pos) == 1  % if only position along x was given, re-use
               pos = [pos pos]; % same position in the y-direction. 
            end
            x_ixs = repmat(ceil(xres * pos(1)), 1, numel(z_ixs));
            y_ixs = repmat(ceil(yres * pos(2)), 1, numel(z_ixs));
            
            wc = sub2ind(model.G.cartDims, x_ixs, y_ixs, z_ixs);
         end
         
      end
      
      % In case flow is only on part of the grid, remap wc to refer to the
      % cells in the flow grid
      wc = find(ismember(model.Gmap.gc,wc));

      add_well = @(W, wcells, val, name) addWell(W, model.G, model.rock, wcells, ...
                     'Type', opt.well_type{i}                           , ...
                     'val'      , val                                   , ...
                     'radius'   , opt.well_rad                          , ...
                     'name'     , name                                  , ...
                     'comp_i'   , 1                                     , ...
                     'refDepth' , model.G.cells.centroids(wcells , end) , ...
                     'sign'     , -1 + (opt.well_val(i) > 0) * 2);
      
      if numel(wc) > 1 && opt.dim == 2
         % Special handling of the case where grid is 2D and well is supposed
         % to have multiple completions.  This is not yet supported properly
         % in MRST, so we go for a 'hack' where we add several wells, one for
         % each grid cell in question
         for ix = 1:numel(wc)
            val = opt.well_val(i) + strcmpi(opt.well_type{i}, 'bhp') * state.pressure(wc(ix));
            W = add_well(W, wc(ix), val, ['P', num2str(i), '_', num2str(ix)]);
         end
      elseif numel(wc)>=1
         % We can setup our wells the 'normal' way
         val  = opt.well_val(i) + strcmpi(opt.well_type{i}, 'bhp') * state.pressure(max(wc));
         W = add_well(W, wc, val, 'P1');
      else
         % no wells
      end
   end

   if numel(opt.tsteps) > 1
      tsteps = diff([0; opt.tsteps(:)]);
   else
      tsteps = opt.T / opt.tsteps * ones(opt.tsteps, 1);
   end
   
   schedule = struct('control', struct('W', W, 'bc', bc), ...
                     'step'   , struct('control', ones(numel(tsteps), 1), ...
                                       'val'    , tsteps));
end

% ----------------------------------------------------------------------------
function [zeta, alpha] = compute_zetas(opt, Ev, nuv, grid, nf_cells)
   
      alpha     = set_cellvals(opt.alpha, grid);
      poros     = set_cellvals(opt.poro, grid);
      Cb        = (3 * (1 - 2 * nuv)) ./ Ev;  % @@ originally the inverse was
                                              % (erroneously) used here, so I
                                              % flipped it.
      zeta      = alpha ./ (poros*opt.c_water + (1 - alpha) .* (alpha - poros) .* Cb);

      % zeta is only relevant for non-flow cells.  Elsewhere, it should be zero.
      zeta(~nf_cells) = 0;
end

% ----------------------------------------------------------------------------

function res = set_cellvals(vals, grid)
   % 'vals' and 'num_layer_cells' are vectors with n elements, representing n
   % layers of the grid.  Return a vector of cell values, constant within
   % each layer.
   % 'vals' can also be a vector with one prescribed value per cell in grid.
   % In that case, it is returned directly again.
   if numel(vals) == grid.G.cells.num
      res = vals;
      return;
   end
   
   res = zeros(grid.G.cells.num, 1);
   res(grid.ob_cells)  = vals(1);
   res(grid.top_cells) = vals(2);
   res(grid.aq_cells)  = vals(3);
   res(grid.bot_cells) = vals(4);
end

% ----------------------------------------------------------------------------

function res = if_else(cond, yes, no)
   if cond
      res = yes;
   else
      res = no;
   end
   
end
