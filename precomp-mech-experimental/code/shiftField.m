function sfield = shiftField(G, field, shift, fill_value)
%Shifts a scalar field defined on a simulation grid a certain number of cells
%in each logical direction.  
% 
% SYNOPSIS:
%   function sfield = shiftField(G, field, shift)
%
% DESCRIPTION:
%
% PARAMETERS:
%   G          - simulation grid.  Needs to have a logical cartesian structure.
%   field      - scalar field defined on G
%   shift      - [i, j] for 2D grid or [i, j, k] for 3D grid - defines the
%                requested shift in each index
%   fill_value - fill-in value where field does not cover the grid after
%                shifting.  If 'fill_value' is empty, then the values from
%                the original field will be used instead.
%
% RETURNS:
%   sfield - the shifted field
%
% EXAMPLE:
%
% SEE ALSO:
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   if G.griddim == 2
      sfield = shift_field_2D(G, field, shift, fill_value);
   else
      sfield = shift_field_3D(G, field, shift, fill_value);
   end
end
  

% ----------------------------------------------------------------------------

function sfield = shift_field_2D(G, field, shift, fill_value)

   if isempty(fill_value)
      sfield = field;
   else
      sfield = fill_value * ones(G.cells.num, 1);
   end
   
   % subscripts according to global grid
   [i, j] = ind2sub(G.cartDims, G.cells.indexMap(1:G.cells.num));
   
   i = i + shift(1);
   j = j + shift(2);
   
   % ensure that all subscript refer to valid logical grid cells, by removing
   % those that don't
   keep = i > 0 & i <= G.cartDims(1) & j > 0 & j <= G.cartDims(2);
   i = i(keep);
   j = j(keep);
   field = field(keep);
   
   % creating map from global index to local index in G
   cmap = nan(prod(G.cartDims), 1);
   cmap(G.cells.indexMap) = 1:numel(G.cells.indexMap);
   
   c_ixs = cmap(sub2ind(G.cartDims, i, j));
   
   remaining = ~isnan(c_ixs);

   sfield(c_ixs(remaining)) = field(remaining);
   
end


% ----------------------------------------------------------------------------

function sfield = shift_field_3D(G, field, shift, fill_value)

   if isempty(fill_value)
      sfield = field;
   else
      sfield = fill_value * ones(G.cells.num, 1);
   end
   
   % subscripts according to global grid
   [i, j, k] = ind2sub(G.cartDims, G.cells.indexMap(1:G.cells.num));
   
   i = i + shift(1);
   j = j + shift(2);
   k = k + shift(3);
   
   % ensure that all subscript refer to valid logical grid cells, by removing
   % those that don't
   keep = i > 0 & i <= G.cartDims(1) & ...
          j > 0 & j <= G.cartDims(2) & ...
          k > 0 & k <= G.cartDims(3);
   i = i(keep);
   j = j(keep);
   k = k(keep);

   field = field(keep);
   
   % creating map from global index to local index in G
   cmap = nan(prod(G.cartDims), 1);
   cmap(G.cells.indexMap) = 1:numel(G.cells.indexMap);
   
   c_ixs = cmap(sub2ind(G.cartDims, i, j, k));
   
   remaining = ~isnan(c_ixs);
   
   sfield(c_ixs(remaining)) = field(remaining);
end

