function [wellsols, states] = simulateSplittedPoromech(initState, coupled_model, schedule)
   
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

    num_steps = numel(schedule.step.val);
   c_m = estimateStorativityParams(coupled_model, true); % Geertsma's parameter

   wellsols = cell(num_steps, 1);
   states = cell(num_steps+1, 1);
   states{1} = initState; % will be removed at end of function
   

   for step = 1:num_steps
      
      cur_control = schedule.control(schedule.step.control(step));
      dt = schedule.step.val(step);
      
      [wellsols{step}, states{step+1}] = ...
          solve_timestep(coupled_model, states{step}, cur_control, dt, c_m);
      
   end
   states = states(2:end);
   
end

% ----------------------------------------------------------------------------

function [wsol, state] = solve_timestep(coupled_model, cur_state, control, dt, c_m)
                                  
   aphi = coupled_model.rock.alpha ./ coupled_model.rock.poro;
   div0 = cur_state.vdiv(coupled_model.Gmap.gc);
   p0   = cur_state.pressure; % We assume div0 and p0 are already in equilibrium
   
   % We start out using Geertsma's parameter as divergence estimator
   cv   = c_m; 
   
   fluid = setfield(coupled_model.fluid, ...
                    'pvMultR', ...
                    @(p) coupled_model.rock.pvMultR(p) + ... % grain compressibility part
                         aphi .* div0 + ... % divergence when pressure equals p0
                         aphi .* cv .* (p - p0)); % estimated change in divergence for p ~= p0
                      
   rock = rmfield(coupled_model.rock, 'pvMultR'); % just to be clear: not used below

   A     = coupled_model.operators.mech.A;
   gP    = coupled_model.operators.mech.gradP;
   rhs   = coupled_model.operators.mech.rhs;
   alpha = coupled_model.rock.alpha;
   L     = coupled_model.operators.extra.precond;
   
   MAX_IT    = 20;
   ERROR_TOL = 1e-9;%1e-10;
   LINSOLVE_TOL = 1e-10;%1e-12;
   x = cur_state.x;
   
   div_actual = [];
   
   for i = 1:MAX_IT
      % compute pressure equation, emulating mechanical response using a
      % tailored version of fluid.pvMultR
      [wsol, state] = ...
          simulateScheduleAD(cur_state, ...
                             WaterModel(coupled_model.G, rock, fluid), ...
                             struct('control', control, ...
                                    'step', struct('control', 1, 'val', dt)));
      
      state = state{1};
      p = state.pressure;
      
      if isempty(div_actual)
         div_simul = div0 + cv .* (p-p0);
      else
         div_simul = div_actual;
      end
      
      % compute actual mechanical response
      x = itersolve_JP(A, rhs  + gP * (alpha .* p), 'solver', @pcg, ...
                       'M1', L, 'M2', L', 'x0', x, 'tol', LINSOLVE_TOL);
      div_actual = coupled_model.operators.mech.div * x;
      div_actual = div_actual ./ coupled_model.G.cells.volumes;
      % div_actual = div_actual(coupled_model.Gmap.gc) ./ ...
      %              coupled_model.G.cells.volumes;
      
      % compare actual response to multiplier-based divergence estimate
      % dp = p-p0;
      % div_estimated = div0 + cv .* (dp);
      
      div_error = max(abs(div_actual - div_simul));

      figure(1); clf; plot(div_actual); hold on; plot(div_simul, 'r');
      figure(2); plot(p);
      fprintf('Div error: %12.8e\n', div_error);
      
      if div_error < ERROR_TOL
         % converged
         break;
      end
      
      % response and divergence estimate differ too much.  Recompute flow
      % based on current response. 
      % div0 = div_actual;
      % p0 = p;

      par = 1;%0.58;
      div_actual = par * div_actual + (1-par) * div_simul;
      fluid.pvMultR = hacked_fun(coupled_model.rock.pvMultR, aphi, p0, div0, ...
                                 div_actual, p, 1*cv);
      %div_actual);
          % @(pp) coupled_model.rock.pvMultR(pp) + ... % grain compressibility part
          %       aphi .* ((double(pp)==p0) .* div0 + (double(pp)~=p0) .* div_actual);
      %aphi .* cv .* (pp - p0); % estimated change in divergence for pp ~= p0
                          
   end
   if i == MAX_IT
      error('Reached max iterations without converging.  Aborting.');
   end
   state = struct('x', x, 'pressure', p);
   %state.x = x;
   state = addDerivedQuantities(coupled_model, state); 
   fprintf('Finished in %i iterations\n', i);
end

% ----------------------------------------------------------------------------

function fun = hacked_fun(f1, aphi, p0, div0, div_actual, p1, cv)
   
   parity = 0;
   
   function res = f(p)
      res = f1(p);
      if parity == 0
         res = res + aphi .* (div_actual + ((p-p1) .* cv));
         parity = 1;
      else
         res = res + aphi .* div0;
         parity = 0;
      end
   end
   
   fun = @(p) f(p);
end

% ----------------------------------------------------------------------------

function res = if_else(cond, yes, no)
   if cond
      res = yes;
   else
      res = no;
   end
end
