function M = higherOrderEnergies(G, E, nu)
% Returns the square matrix E representing the higher order energies
% (i.e. energies associated with the displacement space orthogonal to the
% linear displacements addressed by VEM) of grid G.  The energies are exact
% for gridcells that are really axis-aligned prisms.

%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
   if G.griddim==2
      assert(unique(diff(G.cells.facePos)) == 4); % all cells should have 4 faces
   elseif G.griddim==3
      assert(unique(diff(G.cells.facePos)) == 6); % all cells should have 6 faces
   else
      error('Need 2D or 3D grid.');
   end
   
   [dx, dy, dz] = get_cell_lengths(G); % if 2D, dz will be an empty matrix

   V = G.cells.volumes; % for axis-aligned prisms, should be 8 dx dy dz in
                        % 3D, or 4 dx dy in 2D
   
   % Compute energy matrix 
   M = compute_energy_matrix(dx/2, dy/2, dz/2, V, E, nu);
   
   % Compute projection 
   P = compute_projection(G);
   
   % Computing final answer
   M = P' * M * P / 8;

end

% ----------------------------------------------------------------------------

function P = compute_projection(G)

   if ~isfield(G.cells, 'nodePos')
      % The mapping cell->nodes provided by the following call is the one
      % used in the VEM code, so we must ensure our energy matrix uses the
      % same mappping
      G = mrstGridWithFullMappings(G);
   end
   
   if G.griddim==3
      P = compute_projection_3D(G);
   else
      P = compute_projection_2D(G);
   end
end

% ----------------------------------------------------------------------------

function pv = vectorize_permutations(p)

   n = size(p, 1); % length of each permutation vector
   el_num = size(p, 2); % number of permutation vectors
   
   offset = repmat([0:n:(el_num-1)*n], n,1);
   pv = p(:) + offset(:);
   
end

% ----------------------------------------------------------------------------
function pm = perm_matrix(p)
   pm = sparse(1:numel(p), p, 1, numel(p), numel(p));
end

% ----------------------------------------------------------------------------
function p2 = expand_perm_2D(p)
   % take a permutation and 'expand' it to take the two spatial components of
   % each point into account, keeping the components next to each other
   p2 = [2*(p'-1)+1;2*p'];
   p2 = p2(:);
   
end

% ----------------------------------------------------------------------------
function p3 = expand_perm_3D(p)
   % take a permutation and 'expand' it to take the three spatial components of
   % each point into account, keeping the components next to each other
   
      p3 = [3 * (p'- 1) + 1; ...
            3 * (p'- 1) + 2; ...
            3 * p'];
      p3 = p3(:);
end


% ----------------------------------------------------------------------------
   
function P = compute_projection_2D(G)

   cn = cellNodes(G);
   cn = reshape(cn(:, 3), 4, []); % one column per cell
   
   cnum = G.cells.num;
   inodes = mcolon(G.cells.nodePos(1:(end-1)), G.cells.nodePos(2:end)-1)';
   nodes = G.cells.nodes(inodes)'; % global node indices per cell (4 per cell)
   nodes = reshape(nodes, 4, []);
   

   % Find the permutation that takes a vector whose components is in the
   % order of 'nodes', and rearrange it into the order of 'cn'
   [~, p1] = sort(cn);
   [~, p2] = sort(nodes);
   P1 = perm_matrix(expand_perm_2D(vectorize_permutations(p1))); % cn->sort(cn)
   P2 = perm_matrix(expand_perm_2D(vectorize_permutations(p2))); % nodes->sort(nodes)
   
   permut = P1' * P2; % nodes->cn

   % Projection matrix, assuming cn_{x, y} order
   proj = sparse([1 0 -1  0 -1  0 1 0; ...
                  0 1  0 -1  0 -1 0 1]');
   
   proj_rep = repmat(proj, 1, cnum);
   proj_rep_cell = mat2cell(proj_rep, 8, repmat(2, 1, cnum));
   proj_full = blkdiag(proj_rep_cell{:});
   P = proj_full' * permut;
end
   
% ----------------------------------------------------------------------------

function P = compute_projection_3D(G)

   cn = cellNodes(G); 
   cn = reshape(cn(:,3), 8, []); % one column per cell
   
   cnum = G.cells.num;
   inodes = mcolon(G.cells.nodePos(1:(end-1)), G.cells.nodePos(2:end)-1)';
   nodes = G.cells.nodes(inodes)'; % global node indices per cell (8 per cell)
   nodes = reshape(nodes, 8, []);
   
   % Find the permutation that takes a vector whose components is in the
   % order of 'nodes', and rearrange it into the order of 'cn'
   [~, p1] = sort(cn);
   [~, p2] = sort(nodes);
   P1 = perm_matrix(expand_perm_3D(vectorize_permutations(p1))); % cn->sort(cn)
   P2 = perm_matrix(expand_perm_3D(vectorize_permutations(p2))); % nodes->sort(nodes)
   
   permut = P1' * P2; % nodes->cn
   
   % Projection matrix, assuming cn_{x, y, z} order
   proj = sparse(...
       [1     0     0     1     0     0     1     0     0    -1     0     0; ... 
        0     1     0     0     1     0     0     1     0     0    -1     0; ...
        0     0     1     0     0     1     0     0     1     0     0    -1; ...
       -1     0     0     1     0     0    -1     0     0     1     0     0; ...
        0    -1     0     0     1     0     0    -1     0     0     1     0; ...
        0     0    -1     0     0     1     0     0    -1     0     0     1; ...
       -1     0     0    -1     0     0     1     0     0     1     0     0; ...
        0    -1     0     0    -1     0     0     1     0     0     1     0; ...
        0     0    -1     0     0    -1     0     0     1     0     0     1; ...
        1     0     0    -1     0     0    -1     0     0    -1     0     0; ...
        0     1     0     0    -1     0     0    -1     0     0    -1     0; ...
        0     0     1     0     0    -1     0     0    -1     0     0    -1; ...
        1     0     0    -1     0     0    -1     0     0     1     0     0; ...
        0     1     0     0    -1     0     0    -1     0     0     1     0; ...
        0     0     1     0     0    -1     0     0    -1     0     0     1; ...
       -1     0     0    -1     0     0     1     0     0    -1     0     0; ...
        0    -1     0     0    -1     0     0     1     0     0    -1     0; ...
        0     0    -1     0     0    -1     0     0     1     0     0    -1; ...
       -1     0     0     1     0     0    -1     0     0    -1     0     0; ...
        0    -1     0     0     1     0     0    -1     0     0    -1     0; ...
        0     0    -1     0     0     1     0     0    -1     0     0    -1; ...
        1     0     0     1     0     0     1     0     0     1     0     0; ...
        0     1     0     0     1     0     0     1     0     0     1     0; ...
        0     0     1     0     0     1     0     0     1     0     0     1]);
   
   proj_rep = repmat(proj, 1, cnum);
   proj_rep_cell = mat2cell(proj_rep, 24, repmat(12, 1, cnum));
   
   proj_full = blkdiag(proj_rep_cell{:});
   
   P = proj_full' * permut;
   
end

% ----------------------------------------------------------------------------

function M = compute_energy_matrix(dx, dy, dz, V, E, nu)
   % dx, dy and dz should be half-lengths here

   cellnum = numel(dx);
   if isscalar(E)
      E = repmat(E, cellnum, 1);
   end
   if isscalar(nu)
      nu = repmat(nu, cellnum, 1);
   end
   
   if isempty(dz)  % 2D case
      M = compute_energy_matrix_2D(dx, dy, V, E, nu);
   else
      M = compute_energy_matrix_3D(dx, dy, dz, V, E, nu);
   end
end

% ----------------------------------------------------------------------------

function M = compute_energy_matrix_2D(dx, dy, V, E, nu)

   [dxi, dyi] = deal(1./dx, 1./dy);
   n = 2*numel(dx);
   
   % computing energies
   M_1 = [dxi.^2, dyi.^2]';
   M_2 = [dyi.^2, dxi.^2]';
   
   M_diag = bsxfun(@times, (1-nu'), M_1) + bsxfun(@times, (1-2*nu')/2, M_2);
   
   fac = rldecode(V/3 .*E./((1+nu).*(1-2*nu)), 2);
   M = spdiags(fac .* M_diag(:), 0, n, n);
end

% ----------------------------------------------------------------------------
function M = compute_energy_matrix_3D(dx, dy, dz, V, E, nu)
   
   [dxi, dyi, dzi] = deal(1./dx, 1./dy, 1./dz);
   cellnum = numel(dx);
   n = 12 * cellnum; 
   zero = zeros(cellnum, 1);

   % Computing energies
   M_self_1 = [dxi.^2, dyi.^2, zero, ...
               zero, dyi.^2, dzi.^2, ...
               dxi.^2, zero, dzi.^2, ...
               1/3 * [dxi.^2, dyi.^2, dzi.^2]]';
   
   M_self_2 = [dyi.^2, dxi.^2, dxi.^2 + dyi.^2, ...
               dyi.^2 + dzi.^2, dzi.^2, dyi.^2, ...
               dzi.^2, dxi.^2 + dzi.^2, dxi.^2, ...
               1/3 * [(dyi.^2 + dzi.^2), ...
                      (dxi.^2 + dzi.^2), ...
                      (dxi.^2 + dyi.^2)]]';
   
   M_diag = bsxfun(@times, (1-nu'), M_self_1) + bsxfun(@times, (1-2*nu')/2, M_self_2);
   
   M_cross = [nu.*dxi.*dzi, nu.*dyi.*dzi, nu.*dxi.*dyi,...
              1/2*(1-2*nu).*dxi.*dzi, 1/2*(1-2*nu).*dyi.*dzi, 1/2 *(1-2*nu).*dxi.*dyi];
   
   
   % Constructing global energy matrix
   M_diag = spdiags(M_diag(:), 0, n,n);
   cixs = 1:12:n;

   M_offdiag = sparse(cixs,   cixs+5, M_cross(:,1), n, n) + ...   % (v1, v6)
               sparse(cixs+1, cixs+8, M_cross(:,2), n, n) + ... % (v2, v9)
               sparse(cixs+4, cixs+6, M_cross(:,3), n, n) + ... % (v5, v7)
               sparse(cixs+2, cixs+3, M_cross(:,4), n, n) + ... % (v3, v4)
               sparse(cixs+2, cixs+7, M_cross(:,5), n, n) + ... % (v3, v8)
               sparse(cixs+3, cixs+7, M_cross(:,6), n, n);      % (v4, v8)


   % Assembling complete matrix and giving it the correct scaling
   fac = rldecode(V/3 .*E./((1+nu).*(1-2*nu)), 12);
   M = spdiags(fac, 0, n, n) * (M_diag + M_offdiag + M_offdiag');
end

% ----------------------------------------------------------------------------

function [xlen, ylen, zlen] = get_cell_lengths(G)

   xlen = G.faces.centroids(G.cells.faces(G.cells.faces(:,2)==2),1) - ...
          G.faces.centroids(G.cells.faces(G.cells.faces(:,2)==1),1);
   ylen = G.faces.centroids(G.cells.faces(G.cells.faces(:,2)==4),2) - ...
          G.faces.centroids(G.cells.faces(G.cells.faces(:,2)==3),2);
   if G.griddim==3
      zlen = G.faces.centroids(G.cells.faces(G.cells.faces(:,2)==6),3) - ...
             G.faces.centroids(G.cells.faces(G.cells.faces(:,2)==5),3);
   else
      zlen = [];
   end
end
