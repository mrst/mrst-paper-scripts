classdef SimplePrecompModel < ReservoirModel
% This class is an attempt to simplify and extend the old
% PrecompResponseModel
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   
   properties
      response % matrix of change in divergences for impulse perturabtions
      irmap    % impulse-response map (map cell index to respective response index)
      div0     % divergences for unperturbed system
      p0       % pressure in unperturbed systemm
      flow_bc  % flow boundary conditions used in computing mechanical
               % response (proper to model, must not be changed during simulation)
      fbc      % computed from flow_bc, so also proper to model
      imp_mat  % if nonempty, apply pressure field transformation
      whole_pillars; % if false, response is computed per aquifer cell,
                     % otherwise per vertical cell stacks
      pillar_vols;
   end
   
   methods
      
      function model = SimplePrecompModel(wm_model, flow_bc, p0, varargin)
         
         %% Copy fields from provided model, and setting up the other fields,
         model = model@ReservoirModel(wm_model.G);
         model = copy_common_fields(model, wm_model);
         model.verbose = mrstVerbose();
         model.flow_bc = flow_bc;
         model.fbc = wm_model.addBcFromFluid(flow_bc);
         model.p0 = p0;
         model.div0 = computeDiv0(model, wm_model);
         model.imp_mat = [];
         model.whole_pillars = true;
                  
         %% Determining response functions
         opt.nonlinearTolerance = 1e-14;
         opt.response_type = 'local'; % valid options are 'local', 'full' and 'downsampled'
         opt.cutoff = 0; % truncation value for reponse functions
         opt.whole_pillars = true;
         opt.transformed_pressure = false; % if true, use transformed
                                           % pressure basis that will dampen
                                           % oscillations.  This will however
                                           % converting initial pressure to
                                           % the new basis beforehand, and
                                           % converting pressure for all
                                           % timesteps back after simulation.
         opt.savefile = []; % file where responses are stored/loaded
         opt.batch_compute = true; % whether to compute multiple, spatially distant
                                   % responses at the same time
         opt.renormalize = true; % whether to rescale remaining part of
                                 % truncated response to obtain same total
                                 % integral as the untrunated response
         
         opt = merge_options(opt, varargin{:});

         model.nonlinearTolerance = opt.nonlinearTolerance;
         model.whole_pillars = opt.whole_pillars;
         
         if opt.transformed_pressure
            if strcmpi(opt.response_type, 'local')
               error(['Transformed pressure should only be used with full or ' ...
                      'downsampled response functions.']);
            end
            model.imp_mat = transformed_pressure_basis(model.G);
            model.p0 = model.imp_mat \ model.p0; % from now, all pressures
                                                 % should be represented in
                                                 % new basis
         end
         
         [model.response, irmap] = setup_response(wm_model            , ...
                                                  opt.response_type   , ...
                                                  opt.cutoff          , ...
                                                  model.imp_mat       , ...
                                                  model.whole_pillars , ...
                                                  opt.savefile        , ...
                                                  opt.batch_compute   , ...
                                                  opt.renormalize);

         model.irmap = sparse(irmap, 1:wm_model.G.cells.num, wm_model.G.cells.volumes);
         model.pillar_vols = sum(model.irmap, 2);
         model.irmap = bsxfun(@rdivide, model.irmap, model.pillar_vols);
         
      end
      
      function div0 = computeDiv0(model, wm_model)
         
      % VEM_linElast used here only to setup the dirichlet boundary values. We
      % therefore use a dummy solver
         dummy_solver = @(A, rhs) 0 * rhs;
         el_bc = wm_model.mech.el_bc;
         C = Enu2C(wm_model.mech.Ev, wm_model.mech.nuv, wm_model.GMech);
         uu0 = VEM_linElast(wm_model.GMech, C, el_bc, @(x) 0*x, 'linsolve', dummy_solver);
         
         L = model.operators.extra.precond;
         x = itersolve_JP(wm_model.operators.mech.A, ...
                          (wm_model.operators.mech.rhs + ...
                           wm_model.operators.mech.gradP * ...
                           (wm_model.rock.alpha .* model.p0) - ... 
                           model.fbc), ...
                          'solver', @pcg, ...
                          'M1', L, 'M2', L', 'tol', 5e-12);

         utmp = reshape(uu0', [], 1);

         utmp(~wm_model.operators.mech.isdirdofs) = x;
         div0 = wm_model.operators.mech.ovol_div * utmp;
         flowcells = wm_model.Gmap.gc;
         div0 = div0(flowcells);        
         
      end
      
      function [problem, state] = getEquations(model, state0, state, dt, ...
                                               drivingForces, varargin)
         assert(isequal(drivingForces.bc, model.flow_bc));
         [problem, state] = equationsResponseModel(state0, state, model, dt, ...
                                                   drivingForces, varargin{:});
      end
      
      function [state, report] = updateState(model, state, problem, dx, drivingForces)
         
         [state, report] = ...
             updateState@ReservoirModel(model, state, problem, dx, drivingForces);
         state.vdiv = (model.div0 + ...
                       model.irmap' * (model.pillar_vols .* ...
                                       (model.response * (model.irmap * ...
                                                          (state.pressure - model.p0))))) ./ ...
             model.G.cells.volumes;
         
         % state.vdiv = (model.div0 + model.pillar_vols .* (model.response * ...
         %               (model.irmap * (state.pressure - model.p0)))) ./ model.G.cells.volumes;
      end
   end
end


% ----------------------------------------------------------------------------

function [res, irmap] = setup_response(wm_model, type, cutoff, pbasis, ...
                                       whole_pillars, savefile, batch_compute, ...
                                       renormalize)

   if ~isempty(savefile) && (exist(savefile)==2 || (exist([savefile '.mat'])==2))
      tmp = load(savefile);
      res = tmp.res;
      irmap = tmp.irmap;
      return;
   end
   
   n = wm_model.G.cells.num;

   if whole_pillars
      irmap = construct_irmap(wm_model.G);
   else 
      irmap = (1:n)';
   end
   
   switch type
     case 'local'
       % Local divergence resulting from uniform pressure raise throughout
       % aquifer
       vols = wm_model.G.cells.volumes;
       cellvals = estimateStorativityParams(wm_model, true) .* vols;
       accumvals = accumarray(irmap, cellvals) ./ accumarray(irmap, vols);
       res = spdiags(accumvals, 0, max(irmap), max(irmap));
       %res = spdiags(estimateStorativityParams(wm_model, true) .* vols, 0, n, n);
     case 'full'
       if batch_compute
          res = batchComputeSelectedPressureResponses(wm_model, cutoff, ...
                                                      irmap, renormalize);
       else
          res = computeSelectedPressureResponses(wm_model, 1:max(irmap), ...
                                                 cutoff, pbasis, irmap, ...
                                                 renormalize); 
       end
     case 'downsampled'
       res = computeDownsampledPressureResponse(wm_model, cutoff, pbasis, irmap);
     otherwise
       error('Unknown option');
   end
   
   if ~isempty(savefile)
      save(savefile, 'res', 'irmap');
   end
end

   
% ----------------------------------------------------------------------------

function s1 = copy_common_fields(s1, s2)
   for f = fields(s1)'
      if isprop(s2, f{:})
         s1 = setfield(s1, f{:}, getfield(s2, f{:}));
      end
   end
end

% ----------------------------------------------------------------------------

function imp_mat = transformed_pressure_basis(G)

   n = G.cells.num;
   nix = G.faces.neighbors;       % all faces, with their neighbors
   nix = nix(prod(nix,2) > 0, :); % keep only interior boundary faces
   
   imp_mat = sparse(nix(:,1), nix(:,2), 1, n, n) + ...
             sparse(nix(:,2), nix(:,1), 1, n, n) + ...
             speye(n);
   if (G.griddim == 2)
      % Tests have shown that in this case, we only need one neighbor cell
      imp_mat = tril(imp_mat); % lower triangular part should be enough
   end
end