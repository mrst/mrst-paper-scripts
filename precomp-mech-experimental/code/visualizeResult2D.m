function visualizeResult2D(G, states, initState, slice_ix)
%
%
% SYNOPSIS:
%   function visualizeResult2D(G, states, initState, slice_ix)
%
% DESCRIPTION:
%   Tool for visualizing results from mechanics simulations
%
% PARAMETERS:
%   G         - Grid to visualize (2D or 3D)
%   states    - cell array with computed states
%   initState - initial state
%   slice_ix  - Index of vertical slice of cells to visualize 
%               (only relevant for 3D grids)
%
% RETURNS:
%   nothing
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   
   % If separate initial state is presented, concatenate it to the states variable
   if nargin > 2
      states = {initState, states{:}}';
   end

   % Take vertical slice of grid
   if G.griddim == 3
      [G, states] = extract_slice(G, states, slice_ix);
   end

   % program constants
   deform_mult_max = 1e4;
   
   % Initial values for interactively selectable options
   tstep       = 1;                 % start with first state
   variable    = 1;                 % start with first variable
   deform_mult = 0;                 % default deformation multiplicator when plotting
                                    % deformed grid
   relative    = numel(states) > 1; % whether or not initial deformation should be subtracted
   absolute    = false;             % plot absolute values (instead of signed ones)
   inv_y       = true;              % plot with inverted z axis
      
   % ======================= Setting up interactive window =======================
   h = figure('KeyPressFcn', @(obj, e) parse_keypress(e.Key));
   
   set(h, 'position', [100 300 1400 800]);
   ax = axes('parent', h, 'position', [0.13 0.2 0.85 0.77]);

   % ---------------------- Slider for selecting time step ----------------------
   if numel(states) > 1
      lb = uicontrol('parent', h, 'style', 'text', 'position', ...
                     [200, 40 100, 20], 'string', 'TIMESTEP');
      b = uicontrol('parent', h, 'style', 'slider', 'position', ...
                    [200 10 500  20], 'value', tstep, 'min', 1, 'max', ...
                    numel(states));
      set(b, 'Callback', @(es, ed) set_timestep(floor(get(es, 'Value'))));
      
      ti = uicontrol('parent', h', 'style', 'text', 'position', [710 10 80 20], ...
                     'string', num2str(tstep));
   end
   % ----------------------- Slider for selecting variable -----------------------
   vars = ['Pressure|',                                                        ...
           'u_x|'     , 'u_y|'     , 'norm(u)|'  , 'div(u)|'     , 'curl(u)|', ...
           'sigma_x|' , 'sigma_y|' , 'sigma_xy|' , 'norm(sigma)|',             ...
           'strain_x|', 'strain_y|', 'strain_xy|', 'norm(strain)'];

   s  = uicontrol('parent', h, 'style', 'popup' , 'position', ...
                  [30  11 150  20], 'string', vars, 'value', 1);
   set(s, 'Callback', @(es, ed) set_variable(get(es, 'Value')));
   
   % --------------- slider for controling deformation multiplier ---------------

   ld = uicontrol('parent', h, 'style', 'text', 'position', ...
                  [800, 40 100, 20], 'string', 'DEFORM. MULT');
   
   ds = uicontrol('parent', h', 'style', 'slider', 'position', ...
                  [800 10 500 20], 'value', deform_mult, 'min', 0.0, ...
                  'max', deform_mult_max);
   set(ds, 'Callback', @(es, ed) set_deform_mult(get(es, 'Value')));
   
   di = uicontrol('parent', h, 'style', 'text', 'position', ...
                  [1300, 10, 80, 20], 'string', num2str(deform_mult));

   % ------------------------------ toggle-buttons ------------------------------
   
   b1 = uicontrol('parent', h, 'style', 'togglebutton', 'position', ...
                  [30 200 80 40], 'string', 'Invert y', 'value', inv_y);
   b2 = uicontrol('parent', h, 'style', 'togglebutton', 'position', ...
                  [30 250 80 40], 'string', 'absolute', 'value', absolute);
   b3 = uicontrol('parent', h, 'style', 'togglebutton', 'position', ...
                  [30 300 80 40], 'string', 'rel. deform', 'value', relative);
   set(b1, 'callback', @(es, ed) update_toggles());
   set(b2, 'callback', @(es, ed) update_toggles());
   set(b3, 'callback', @(es, ed) update_toggles());
   
   % ----------------------------------------------------------------------------

   redraw();
   
   % ============================= Helper functions =============================
   
   function redraw()
      field = get_field(variable, states{tstep});
      set(s, 'Value', variable);
      if exist('b')
         set(b, 'Value', tstep);
      end
      % preparing deformation to use for plotting
      uu = field_value(states{tstep}, 'uu', deform_mult, 1:G.griddim);
      if numel(uu) == 1 % not present in state
         uu = zeros(G.nodes.num, 2);
      end
      
      % plotting
      cla;
      if size(field, 1) == G.cells.num
         if numel(field) == 1
            field = ones(G.cells.num, 1) * field;
         end
         if (G.griddim == 3 && any(isnan(field)))
            plotCellDataDeformed(G, zeros(size(field)), uu, 'edgealpha', 0.1); 
         end
         plotCellDataDeformed(G, field, uu, 'edgealpha', 0.1);
      else
         if numel(field) == 1
            field = ones(G.nodes.num, 1) * field;
         end
         if (G.griddim == 3 && any(isnan(field)))
            plotCellDataDeformed(G, zeros(size(field)), uu, 'edgealpha', 0.1); 
         end
         plotNodeDataDeformed(G, field, uu, 'edgealpha', 0.1);
      end
      if (G.griddim == 3)
         view(0,0);
      end
      set(gca, 'fontsize', 15);
      xlabel('x (meters)');
      ylabel('depth (meters)');
      axis tight;
      if inv_y
         set(gca, 'ydir', 'reverse');
         set(gca, 'zdir', 'reverse');
      else
         set(gca, 'ydir', 'normal');
         set(gca, 'zdir', 'normal');
      end
      
      colorbar;
   end
   % ----------------------------------------------------------------------------
   function update_toggles()
      inv_y    = get(b1, 'value');
      absolute = get(b2, 'value');
      relative = get(b3, 'value');
      redraw();
   end
   
   % ----------------------------------------------------------------------------
   function set_timestep(t)
      tstep = max(min(t, numel(states)), 1);
      set(ti, 'string', num2str(tstep));
      redraw();
   end

   % ----------------------------------------------------------------------------
   function set_variable(v)
      variable = max(min(v, 14), 1);
      redraw();
   end
   % ----------------------------------------------------------------------------
   function set_deform_mult(v)
      deform_mult = max(min(v, deform_mult_max), 0);
      set(di, 'string', num2str(deform_mult));
      redraw();
   end

   % ----------------------------------------------------------------------------
   function field = get_field(var, state)
      switch var
        case 1  % pressure
          field = field_value(state, 'pressure', 1/barsa, 1);

        case 2  % u_x
          field = field_value(state, 'uu', 1, 1);
        case 3  % u_y
          field = field_value(state, 'uu', 1, G.griddim);
        case 4  % |u|
          field = field_value(state, 'uu', 1, 1:G.griddim);
          field = sqrt(sum(field.^2, 2));
        case 5  % div(u)
          field = field_value(state, 'vdiv', 1, 1);
        case 6  % curl(u)
          warning('curl(u) unimplemented.');
          field = 0 * state.uu(:,1); % dummy
        case 7  % sigma_x
          field = field_value(state, 'stress', -1/barsa, 1);
        case 8  % sigma_y
          field = field_value(state, 'stress', -1/barsa, G.griddim);
        case 9  % sigma_xy
          field = field_value(state, 'stress', -1/barsa, 3 * (G.griddim - 1));
        case 10 % |sigma|
          field = field_value(state, 'stress', -1/barsa, 1:(3 * (G.griddim - 1)));
          field = sqrt(sum(field .^ 2, 2));
        case 11 % strain_x
          field = field_value(state, 'strain', 1, 1);
        case 12 % strain_y
          field = field_value(state, 'strain', 1, G.griddim);
        case 13 % strain_xy
          field = field_value(state, 'strain', 1, 3 * (G.griddim - 1));
        case 14 % |strain|
          field = field_value(state, 'strain', 1, 1:(3 * (G.griddim - 1)));
          field = sqrt(sum(field .^2, 2));
      end
      if absolute
         field = abs(field);
      end
   end
      
   % ----------------------------------------------------------------------------
   function field =  field_value(state, fname, factor, component)
      
      if ~isfield(state, fname)
         field = nan;
         return;
      end
      
      field = getfield(state, fname);
      if relative
         field = field - getfield(states{1}, fname);
      end
      field = field(:,component) * factor;
   end
   
   % ----------------------------------------------------------------------------
   function parse_keypress(key)
      % dummy, for now
      switch key
        case 'uparrow'
          set_variable(variable+1);
        case 'downarrow'
          set_variable(variable-1);
        case 'leftarrow'
          set_timestep(tstep-1);
        case 'rightarrow'
          set_timestep(tstep+1);
      end         
   end
   
end

% ============================================================================

function [G, states] = extract_slice(G, states, slice_ix)

   [i j k] = meshgrid(1:G.cartDims(1), slice_ix, 1:G.cartDims(3));
   keep_cells = sub2ind(G.cartDims, i(:), j(:), k(:));
   
   % Extracting slice of grid
   [G, gcells, gfaces, gnodes] = extractSubgrid(G, keep_cells);
   
   % Extracting relevant cell- and node values
   for i = 1:numel(states)
      states{i}.pressure = states{i}.pressure(gcells);
      states{i}.vdiv     = states{i}.vdiv(gcells);
      states{i}.stress   = states{i}.stress(gcells,:);
      states{i}.strain   = states{i}.strain(gcells,:);
      states{i}.uu       = states{i}.uu(gnodes,:);
   end
end