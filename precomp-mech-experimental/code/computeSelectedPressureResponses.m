function div = computeSelectedPressureResponses(model, impulse_ixs, cutoff, pbasis, ...
                                                irmap, renormalize, varargin)

%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
   opt.tol = 1e-5; %1e-12;%1e-6;  % @@ To compare with no thresholding, it is good
                   % to have a very tight tolerance here.  But when
                   % thresholding is turned on, it might not matter that much.
   opt = merge_options(opt, varargin{:});
   
   mech = model.operators.mech;
   L = model.operators.extra.precond;
   resnum = numel(impulse_ixs);
   cellnum = numel(irmap);
   
   div = sparse(resnum, resnum);
   
   parfor i = impulse_ixs(:)'

      cells = (irmap == i);
      
      if isempty(pbasis)
         alpha_p = zeros(cellnum, 1);
         alpha_p(cells) = 1 * model.rock.alpha(cells);
         %alpha_p(i) = 1 * model.rock.alpha(i);
      else
         p = sum(pbasis(:, cells), 2);
         alpha_p = model.rock.alpha .* p;
         %alpha_p = model.rock.alpha .* pbasis(:,i);
      end
      
      cur_column = ...
          mech.div * itersolve_JP(mech.A, mech.gradP * alpha_p , ...
                                  'solver', @pcg               , ...
                                  'tol', opt.tol               , ...
                                  'M1', L                      , ...
                                  'M2', L');
      
      if any(cur_column ~= 0)
         vols = accumarray(irmap, model.G.cells.volumes);
         resp = accumarray(irmap, sparse(truncate_div(cur_column, model.G, ...
                                                      cutoff, renormalize, find(cells))));
         div(:,i) = resp./vols;
         fprintf('Basis fun: %i; nnz: %i\n', i, nnz(div(:,i)));
      else 
         %div(:,i) = sparse(cur_column);
      end
   end
end
