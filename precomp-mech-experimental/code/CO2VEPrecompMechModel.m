classdef CO2VEPrecompMechModel < CO2VEBlackOilTypeModel

%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   
% ============================= Class properties =============================
properties
   
   response % matrix of change in divergences for impulse perturbations
   div0     % divergences for initial system
   p0       % initial pressure (typically hydrostatic)
   flow_bc  % flow boundary conditions used in computing response (must not
            % be changed during simulation)
   fbc      % computed from flow_bc; proper to model
   split_info % used by splitting schemes to adjust cell volumes by a
              % correction factor
end
   
% ============================== Public methods ==============================
methods
   
   %% Constructor
   function model = CO2VEPrecompMechModel(ppr_model, VEfluid, strain_dep_perm)
      
      % Argument sanity checks
      assert(isempty(ppr_model.imp_mat)); % No support of other pressure
                                          % basises
      assert(~isfield(VEfluid, 'dis_rate')); % we do not support dissolution
                                             % for now
      
      % extract pillar responses and corresponding irmap
      [response, irmap] = deduce_pillar_responses(ppr_model);
      
      % Constructing parent class part of object
      Gt = topSurfaceGrid(ppr_model.G);
      rock2D = averageRock(ppr_model.rock, Gt);
      if isfield(ppr_model.rock, 'pvMultR')
         if isfield(VEfluid, 'pvMultR')
            warning(['pvMultR function of rock object overriding similar ' ...
            'function in fluid object']);
         end
         VEfluid.pvMultR = average_pvmult(ppr_model.rock.pvMultR, ...
                                          ppr_model.rock.poro, ...
                                          ppr_model.G, ...
                                          irmap);
         %VEfluid.pvMultR = @(p) 1;
      end

      model = model@CO2VEBlackOilTypeModel(Gt, rock2D, VEfluid, ...
                                           'nonlinearTolerance', ppr_model.nonlinearTolerance);

      % Copying other relevant information
      model.response    = response;
      model.div0        = spones(irmap) * ppr_model.div0;

      % compute caprock pressure
      top_cells = sum(Gt.parent.faces.neighbors(Gt.cells.map3DFace,:), 2);
      p_tc = ppr_model.p0(top_cells); % centroid pressure in top cells
      rhow = VEfluid.rhoW(p_tc);
      dz = Gt.parent.cells.centroids(top_cells, 3) - Gt.cells.z;
      model.p0 = p_tc - rhow .* dz .* norm(gravity);
      
      % extract part of ppr_model.p0 that represents pressure below caprock
      remap = sum(diag(1:size(irmap, 1)) * spones(irmap), 1)';
      % model.p0 = accumarray(remap, ppr_model.p0, [size(irmap, 1), 1], @min);
      % %      model.p0          = ppr_model.p0;
      model.fbc         = ppr_model.fbc; % @ probably not going to be used
      
      
      model.rock.alpha = accumarray(remap, ppr_model.rock.alpha .* ppr_model.G.cells.volumes) ...
                         ./ accumarray(remap, ppr_model.G.cells.volumes); 
      
      % Setup transmissibility multipliers, if applicable
      if strain_dep_perm
         [model.fluid.transMult, model.fluid.permMult] = ...
             makeStrainDependentTransmult(Gt, ...
                                          setfield(model.rock, 'pvMultR', VEfluid.pvMultR), ...
                                          model.p0, model.div0./ ...
                                          Gt.cells.volumes./Gt.cells.H);
         
      end
      
      %model.rock.alpha  = ppr_model.rock.alpha;
      
      model.flow_bc     = ppr_model.flow_bc;
      if ~isempty(ppr_model.flow_bc)
         model.flow_bc.sat = [ppr_model.flow_bc.sat, ...
                             zeros(size(ppr_model.flow_bc.sat))];
      end

      % Setting the equation to be used
      model.equation = @equationsWGVEPrecompMech;
      
   end
   
   function model = setVolumeCorrection(model, actual, estimated)
      assert(numel(actual) == model.G.cells.num);
      
      model.split_info.actual = actual(:);
      model.split_info.estimated = estimated(:);
      %model.vol_modifier = correction(:);
   end
   
end % methods

end % classdef

% ----------------------------------------------------------------------------

function pvMultVE = average_pvmult(pvMult, poro, G, irmap)

   % Reverse-engineering pvMult to extract p_ref and fine-scale
   % compressibility 
   mag = 10 * barsa; % test impulse magnitude 
   p_tmp = mag * ones(size(irmap, 2),1);
   resp = (pvMult(p_tmp) - pvMult(0*p_tmp))./mag;

   pvols = poro .* G.cells.volumes;
   remap = sum(diag(1:size(irmap, 1)) * spones(irmap), 1)';
   respVE = accumarray(remap, resp .* pvols) ./ ...
            accumarray(remap, pvols);

   % prevent division by zero (in the cases where it doesn't matter anyway,
   % i.e. incompressible cells)
   resp(resp==0) = 1;
   
   p_ref = p_tmp + (1 - pvMult(p_tmp))./resp;
   assert(numel(unique(p_ref))==1); % should be the same reference pressure everywhere
   p_ref = p_ref(1) * ones(size(respVE));
   
   pvMultVE = @(p) 1 + respVE .* (p - p_ref);
   
end
% ----------------------------------------------------------------------------

function [response, irmap] = deduce_pillar_responses(ppr_model)
   
   if ppr_model.whole_pillars
      response = ppr_model.response;
      irmap = ppr_model.irmap;
   else
      assert(isdiag(ppr_model.response)); % true for 'local' model.  Support
                                          % in the general case not yet
                                          % implemented
      G = ppr_model.G;
      local_responses = diag(ppr_model.response);

      remap = construct_irmap(G);
      irmap = sparse(remap, 1:G.cells.num, G.cells.volumes);
      
      pillarvols = sum(irmap, 2);
      
      response = accumarray(remap, local_responses .* G.cells.volumes) ./ pillarvols;
      response = spdiags(response(:), 0, numel(pillarvols), numel(pillarvols));
   end
end

   