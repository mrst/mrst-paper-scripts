classdef WaterMechanicModelVEM < WaterMechanicModel
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}


% Single phase
   properties
      
   end
   
   methods   
      function model = WaterMechanicModelVEM(G, rock, fluid, mech_problem, varargin)
         
         [opt, rest] = merge_options(struct('alpha_scaling', 1, 'S', [], ...
                                            'ilu_tol', 1e-4), varargin{:});
         
         model = model@WaterMechanicModel(G, rock, fluid, mech_problem, rest{:}); 
         model = model.setupOperators(opt.alpha_scaling, opt.S, opt.ilu_tol); 
      end
      
      function state = initDisplacement(model, state, uu, varargin)
         opt = struct('mech_equil'    , true , ...
                      'pressure'      , []   , ...
                      'flow_bc'       , []);
         opt = merge_options(opt, varargin{:}); 

         % could be done explicitly with the operators
         L = model.operators.extra.precond;
         lsolve = @(A, b) itersolve_JP(A, b, 'solver', @pcg, 'M1', L, 'M2', L');
         
         if(opt.mech_equil)
            % Computing discretization operators.  Do not (yet) compute
            % displacements (so dummy linear solver can safely be passed).
            % The only components of 'uu' that will be used from the call
            % below are those corresponding to dirichlet degrees of freedom,
            % for which the linear solver does not matter.
            uu = VEM_linElast(model.GMech,...
                              model.mech.C,...
                              model.mech.el_bc,...
                              model.mech.load, ...
                              'linsolve', @(a, rhs) 0 * rhs);
            if ~isempty(opt.pressure)
               % recompute non-dirichlet nodes, this time taking pore
               % pressure into account
               fbc = 0;
               if ~isempty(opt.flow_bc)
                  % mapping boundary conditions to faces of mechanical grid
                  fbc = model.addBcFromFluid(opt.flow_bc);
               end
               rhs = model.operators.mech.rhs                                        + ...
                     model.operators.mech.gradP * (model.rock.alpha .* opt.pressure) - ...
                     fbc;
               x = lsolve(model.operators.mech.A, rhs);
            else
               % compute displacements without any pressure influence
               x = lsolve(model.operators.mech.A, model.operators.mech.rhs);
            end
            utmp = reshape(uu', [], 1);
            utmp(~model.operators.mech.isdirdofs) = x;
            uu = reshape(utmp, size(uu, 2), size(uu, 1))';
         end
         u = reshape(uu', [], 1); 
         state.x = zeros(size(model.operators.mech.A, 2), 1); 
         state.x = u(~model.operators.mech.isdirdofs); 
         state.uu = uu; 
         state.u = u; 
         state = addDerivedQuantities(model, state); 
         
      end
      
      function model = setupOperators(model, alpha_scaling, S, ilu_tol)

         % Set up divergence / gradient / transmissibility operators for flow
         model = setupOperators@ReservoirModel(model, model.G, model.rock); 
         
         % Setup operators for linear elasticity.  We want to avoid to spend
         % resources computing displacements here, as these will not be
         % used. We therefore pass a dummy solver.
         [~, extra] = VEM_linElast(model.GMech                    , ...
                                   model.mech.C                   , ...
                                   model.mech.el_bc               , ...
                                   model.mech.load                , ...
                                   'alpha_scaling', alpha_scaling , ...
                                   'S', S                         , ...
                                   'linsolve', @(A, rhs) 0 * rhs); 
         
         model.operators.mech = extra.disc; 
         
         % Changing div- and grad operators to only refer to cells for which we have flow 
         pnum = model.G.cells.num; % one pressure value per cell in the flow grid
         model.operators.mech.gradP = ...
             model.operators.mech.gradP(:, model.Gmap.gc(1:pnum));
         model.operators.mech.div = ...
             model.operators.mech.div(model.Gmap.gc(1:pnum), :);

         vdiv    = VEM_div(model.GMech); 
         C       = Enu2C(model.mech.Ev, model.mech.nuv, model.GMech); 
         [~, op] = VEM_mrst_vec(model.GMech, C);%, 'blocksize', model.GMech.cells.num/10);
         strain  = op.WC' * op.assemb'; 
         stress  = op.D * strain; %op.WC' * op.assemb'; 
         
         % computing and storing useful preconditioner (incomplete Choleski)
         if isnan(ilu_tol)
            fprintf('Skipping ilu\n');
            iL = [];
         else
            iL = shiftedIChol(model.operators.mech.A, 0.5, 'droptol', ilu_tol, 'type', 'ict'); 
         end
         
         model.operators.extra = struct('vdiv', vdiv, 'stress', stress, 'strain', ...
                                        strain, 'precond', iL); 
         
      end
      
      function fbc = addBcFromFluid(model, bc)
         % constructing temporary objects indexing into GMech rather than G
         GMech_rock.alpha = nan(model.GMech.cells.num, 1);
         GMech_rock.alpha(model.Gmap.gc) = model.rock.alpha;
 
         if ~isempty(bc)
            bc.face = model.Gmap.gf(bc.face); % reindexing faces wrt. GMech
         end
         
         % Computing the fluid contributions
         fbc = addFluidContribMechVEM(model.GMech, bc, GMech_rock, ...
                                      model.operators.mech.isdirdofs); 
      end
      
      function state = addDerivedQuantities(model, state)
         u = model.operators.mech.V_dir; % zeros(G.gridim * G.nodes.num, 1); 
         u(~model.operators.mech.isdirdofs) = state.x; 
         state.u = u; 
         state.uu = reshape(u, model.GMech.griddim, [])'; 
         % calculate div, stress, eigen values + + + 
         state.vdiv = model.operators.extra.vdiv * u ./ model.GMech.cells.volumes; 
         if(model.GMech.griddim == 2)
            lin_dim = 3; % dimension of the non trivial linear degree of freedom which 
         elseif(model.GMech.griddim == 3)
            lin_dim = 6; 
         else
            error('Wrong dimsension'); 
         end
         state.stress = reshape(model.operators.extra.stress * u, lin_dim, [])'; 
         state.strain = reshape(model.operators.extra.strain * u, lin_dim, [])'; 
         % [sigm, evec] = calStressEigs(model.GMech, state.stress); 
         % state.sigm = sigm; 
         % state.evec = evec;
      end
   end
end
