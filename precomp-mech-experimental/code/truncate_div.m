function [div, keep_radius] = truncate_div(div, G, cutoff, renormalize, center_cells)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   %renormalize = (cutoff > 0);
   completely_local = isinf(cutoff);
   cutoff = abs(cutoff);

   posdiv = div; posdiv(posdiv < 0) = 0;
   negdiv = div; negdiv(negdiv > 0) = 0;

   posnorm = max(posdiv); %sum(posdiv); @@@
   negnorm = max(abs(negdiv));%abs(sum(negdiv));
   %volchange = posnorm - negnorm;
   volchange = sum(div);
            
   if completely_local
      %[maxval, m_ix] = max(posdiv); % identifying pressured cell
      center_vals = div(center_cells);
      div = div*0;
      div(center_cells) = if_else(renormalize, volchange/numel(center_vals), center_vals);
      div = sparse(div);
      keep_radius = 0;
   else

      cutoff_val = cutoff * posnorm;
      
      % locating all cells with absolute divergence above threshold
      keep = abs(div) > cutoff_val;
      
      % also keep divergence of cells closer to center_cells than these
      d2 = sum(bsxfun(@minus, ...
                      G.cells.centroids(:,1:2), ...
                      G.cells.centroids(center_cells(1),1:2)).^2,2);
      keep_radius_2 = max(d2(keep));
      keep = (d2 <= keep_radius_2);
      div(~keep) = 0;
      %div(abs(div)<cutoff_val) = 0;
      if renormalize
         div = div / sum(div) * volchange;
      end
      div = sparse(div);
      keep_radius = sqrt(keep_radius_2);
   end
end

% ----------------------------------------------------------------------------

function res = if_else(cond, yes, no)
   
   if cond
      res = yes;
   else
      res = no;
   end
end
