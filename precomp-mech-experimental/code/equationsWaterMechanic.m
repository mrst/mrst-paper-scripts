function [problem, state] = equationsWaterMechanic(state0, state, model, dt, drivingForces, varargin)
% Get linearized problem for oil/water system with black oil-style properties
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

    
   opt = struct('Verbose'       , mrstVerbose , ...
                'reverseMode'   , false       , ...
                'scaling'       , []          , ...
                'resOnly'       , false       , ...
                'history'       , []          , ...
                'iteration'     , -1          , ...
                'stepOptions'   , []          , ...
                'addflux'       , false); % Compatibility only

   opt = merge_options(opt, varargin{:});

   if ~isempty(opt.scaling)
      scalFacs = opt.scaling;
   else
      scalFacs.rate = 1; scalFacs.pressure = 1;
   end

   W = drivingForces.W;
   assert(isempty(drivingForces.src))
   bc = drivingForces.bc;

   s = model.operators;
   G = model.G;
   f = model.fluid;

   hst = opt.history;

   [p, x, wellSol] = model.getProps(state, 'pressure', 'x', 'wellsol');

   [p0, x0] = model.getProps(state0, 'pressure', 'x');

   pBH = vertcat(wellSol.bhp);
   qWs = vertcat(wellSol.qWs);

   % ----------------- Initialization of independent variables ---------------

   if ~opt.resOnly,
      % ADI variables needed since we are not only computing residuals.
      if ~opt.reverseMode,
         [p, x, qWs, pBH] = initVariablesADI(p, x, qWs, pBH);
      else
         [p0, tmp, tmp] = initVariablesADI(p0, x0, sW0,...
                                           zeros(size(qWs)),...
                                           zeros(size(qOs)),...
                                           zeros(size(pBH))); % #ok
      end
   end
   primaryVars = {'pressure', 'x', 'qWs', 'bhp'};

   clear tmp;
   g = model.getGravityVector();
   dzg = s.Grad(G.cells.centroids) * g';

   % -------------------------------------------------------------------------

   % check for p - dependent tran mult:
   trMult = 1;
   if isfield(f, 'tranMultR'), trMult = f.tranMultR(p); end

   % check for p - dependent porv mult:
   pvMult = 1; pvMult0 = 1;
   if isfield(model.rock, 'pvMultR')
      pvMult  = model.rock.pvMultR(p);
      pvMult0 = model.rock.pvMultR(p0);
      if isfield(f, 'pvMultR')
         warning('pvMultR from rock overriding pvMultR specified in fluid object.');
      end
   elseif isfield(f, 'pvMultR')
      pvMult  = f.pvMultR(p);
      pvMult0 = f.pvMultR(p0);
   end
   transMult = 1;
   if isfield(f, 'transMult')
      vs = (s.mech.div * x) ./ G.cells.volumes; % @@ could be combined with 'div_new' below
      transMult = f.transMult(p, vs);
      fprintf('Min/max: %f  / %f\n', min(double(transMult)), max(double(transMult)));
   end

   trans = s.T .* transMult;
   % -------------------------------------------------------------------------

   % water props (calculated at oil pressure OK?)
   bW    = f.bW(p);
   rhoW  = bW .* f.rhoWS;
   % rhoW on face, average of neighboring cells (E100, not E300)
   rhoWf = s.faceAvg(rhoW);
   mobW  = trMult ./ f.muW(p);
   dpW   = s.Grad(p) - rhoWf .* dzg;
   % water upstream - index
   upc   = (double(dpW) <= 0);
   bWvW  = -s.faceUpstr(upc, bW .* mobW) .* trans .* dpW;
   if(opt.addflux)
      state.bWvW = bWvW;
      state.WvW  = s.faceUpstr(upc, mobW) .* trans .* dpW;
   end

   % ================================= EQUATIONS =================================

   % ------------------------------ water equation ------------------------------

   div_new = s.mech.div * x; % @@ need to me modified for non x = 0 boundary conditions
   div_old = s.mech.div * x0;

   alpha   = model.rock.alpha;
   poro    = model.rock.poro;

   % CC volume term: alpha * (out.divD * u + out.stabDelta * (alpha * p))
   eqs{1} = (1 ./ dt) .*                                                            ...
            ((poro .* (G.cells.volumes .* pvMult)  + alpha .* div_new) .* bW -      ...
             (poro .* (G.cells.volumes .* pvMult0) + alpha .* div_old) .* f.bW(p0)) ...
            + s.Div(bWvW);

   % Handling pressure boundary conditions, if present
   % if(~isempty(bc))
   %    if(isfield(bc, 'cell2bcface'))
   %       pX = {p}; rhoX = {rhoW}; mobX = {mobW}; bX = {bW};
   %       bXqXbc = pressureBCContribADI(G, s, pX, rhoX, mobX, bX, bc);
   %       for i = 1:numel(bXqXbc)
   %          eqs{i} = eqs{i} + bc.bcface2cell * bXqXbc{i};
   %       end
   %    else
   %       pX = {p}; rhoX = {rhoW}; mobX = {mobW}; bX = {bW};
   %       [bXqXbc, bc_cell] = pressureBCContrib(G, s, pX, rhoX, mobX, bX, bc);
   %       for i = 1:numel(bXqXbc)
   %          eqs{i}(bc_cell) = eqs{i}(bc_cell) + bXqXbc{i};
   %       end
   %    end
   % end
   eqs = addFluxesFromSourcesAndBC(model, ...
                                   eqs, ...
                                   {p},...
                                   {rhoW},...
                                   {mobW}, ...
                                   {bW},  ...
                                   {ones(numel(p0),1)}, ...
                                   drivingForces);

   % ---------------------------- mechanics equation ----------------------------

   % force from pressure on boundary on structure
   fbc = model.addBcFromFluid(bc);

   % CC pressure force term: out.gradP(alpha * p)
   eqs{2} = s.mech.A * x - s.mech.gradP * (alpha .* p) - (s.mech.rhs - fbc);

   names = {'water', 'disp'};
   types = {'cell' , 'disp_dofs'};

   % ------------------------------ well equations ------------------------------

   if ~isempty(W)
      if ~opt.reverseMode
         wc   = vertcat(W.cells);
         pw   = p(wc);
         rhos = [f.rhoWS];
         bw   = {bW(wc)};
         mw   = {mobW(wc)};
         sat  = {1};

         if isfield(f, 'permMult')
            % permeability varies.  Update well indices accordingly
            pmult = f.permMult(p, vs);
            for i = 1:numel(W)
               W(i).WI = W(i).WI .* pmult(W(i).cells);
            end
         end

         wm = WellModel();
         [cqs, weqs, ctrleqs, wc, state.wellSol] =                     ...
             wm.computeWellFlux(model, W, wellSol,                     ...
                                pBH, {qWs}, pw, rhos, bw, mw, sat, {}, ...
                                'nonlinearIteration', opt.iteration,   ...
                                'referencePressureIndex', 1);
         eqs(3) = weqs;
         eqs{4} = ctrleqs;

         eqs{1}(wc) = eqs{1}(wc) - cqs{1};
         names(3:4) = {'waterWells', 'closureWells'};
         types(3:4) = {'perf', 'well'};
      else
         % in reverse mode just gather zero - eqs of correct size
         for eqn = 3:4
            nw       = numel(state0.wellSol);
            zw       = double2ADI(zeros(nw, 1), p0);
            eqs(3:4) = {zw, zw};
         end
         names(3:4) = {'empty', 'empty'};
         types(3:4) = {'none', 'none'};
      end
   else  % no wells
      eqs(3:4)   = {pBH, pBH}; % empty  ADIs
      names(3:4) = {'empty', 'empty'};
      types(3:4) = {'none', 'none'};
   end

   % Rescaling equations
   % eqs{1} = eqs{1} * dt;
   % % eqs{3} = eqs{3} * dt;
   % % eqs{4} = eqs{4} * dt;

   % tmp = cat(eqs{1:2});
   % fac = 1 / max(abs(tmp.jac{1}(:)));
   %   fac = 1;
   fac =  1 / (1e6 * mean(G.cells.volumes));
   eqs{2} = eqs{2} * fac;

   % eqs{1} = eqs{1} * fac;
   % %eqs{2} = eqs{2} / dt;


   % ---------------------------- setting up problem ----------------------------
   problem = LinearizedProblem(eqs, types, names, primaryVars, state, dt);
   problem.iterationNo = opt.iteration;
end