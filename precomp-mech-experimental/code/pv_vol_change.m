function pv_change = pv_vol_change(state, istate, model, varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   % If provided an explicit response matrix, will use that to
   % compute/estimate local divergence term, not the computed value in the
   % state variable.
   opt.ppr_model = [];
   opt = merge_options(opt, varargin{:});

   % NB: This function only provides correct results if all cells in a vertical
   % stack have the same cell volume.  Otherwise, the vertical average will not
   % be correctly computed.
   
   % relative change in pore volume 
   gc = model.Gmap.gc;
   pv = model.rock.pvMultR(ones(model.G.cells.num, 1)) - ...
        model.rock.pvMultR(zeros(model.G.cells.num, 1));
   poro = model.rock.poro;
   alpha = model.rock.alpha;
   
   % Initial porosity (pore volume divided by bulk volume)
   if isempty(opt.ppr_model)
      init_div = istate.vdiv(gc);
      cur_div  =  state.vdiv(gc);
   else
      % compute pore volume according to supplied ppr model
      avg_map = opt.ppr_model.irmap;
      p_ref = opt.ppr_model.p0;
      init_div = opt.ppr_model.div0 + ...
                 avg_map' * (opt.ppr_model.pillar_vols .* (opt.ppr_model.response ...
                             * (avg_map * (istate.pressure(gc) - p_ref))));
      cur_div  = opt.ppr_model.div0 + ...
                 avg_map' * (opt.ppr_model.pillar_vols .* (opt.ppr_model.response ...
                             * (avg_map * (state.pressure(gc) - p_ref))));
      init_div = init_div ./ opt.ppr_model.G.cells.volumes;
      cur_div  = cur_div  ./ opt.ppr_model.G.cells.volumes;
   end
   
   v_init = (poro .* (1 + pv .* istate.pressure(gc))) + (alpha .* init_div);
   
   % Current porosity
   v_curr = (poro .* (1 + pv .* state.pressure(gc))) + (alpha .* cur_div);
   
   pv_change = (v_curr - v_init);% ./ v_init;
   
   % taking vertical average
   if model.G.griddim == 3
      pv_change = mean(reshape(pv_change, ...
                                  model.G.cartDims(1), ...
                                  model.G.cartDims(2), []), 3);
   else
      assert(model.G.griddim==2)
      pv_change = mean(reshape(pv_change, ...
                                  model.G.cartDims(1), []), 2);
   end
   
end
   
