classdef WaterMechanicModel < ReservoirModel
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    
    % Two phase oil/water system without dissolution
    properties
        mech;
        Gmap;
        GMech;
    end
    
    methods
        function model = WaterMechanicModel(G, rock, fluid, mech_problem, varargin)
                
           opt.nf_cells = [];
           opt.gravity = []; % capture gravity argument to avoid warning
           opt = merge_options(opt, varargin{:});
           if isempty(opt.nf_cells)
              % use the full grid to compute flow
              Gsub = G;
              % map from flow grid cells/faces/nodes (G) to those of the complete grid (GMech)
              Gmap = struct('gc', 1:G.cells.num, 'gf', 1:G.faces.num, 'gn', G.nodes.num);
           else
              % use only a part of the grid to compute flow
              [Gsub, gc, gf, gn] = extractSubgrid(G, ~opt.nf_cells);
              assert(numel(rock.poro) == sum(~opt.nf_cells)); 
              
              % map from flow grid cells/faces/nodes (G) to those of the complete grid (GMech)
              Gmap = struct('gc', gc, 'gf', gf, 'gn', gn);
           end

           % call superclass constructor
           model = model@ReservoirModel(Gsub, rock, fluid, varargin{:});
           model.Gmap = Gmap;
           
           model.GMech = G; % use the full grid to compute mechanic deformations

           % Saving enhanced grid structure within model
           model.GMech = mrstGridWithFullMappings(model.GMech);
           model.GMech = computeGeometryCalc(model.GMech);

           % physical properties of rock and fluid
           model.mech  = mech_problem;
           model.rock  = rock;
           model.fluid = fluid;

           % Compute stiffness tensor C, if not given
           if ~isfield(model.mech, 'C') 
              model.mech.C = Enu2C(model.mech.Ev, model.mech.nuv, model.GMech);
           end
            
           % only include water in model
           model.oil = false;
           model.gas = false;
           model.water = true;

           % Blackoil -> use CNV style convergence 
           model.useCNVConvergence = false;
           
           model.saturationVarNames = {'sw'};
           model.wellVarNames = {'qWs', 'bhp'};
           
        end
        
        function [problem, state] = getEquations(model, state0, state, dt, drivingForces, varargin)
            [problem, state] = equationsWaterMechanic(state0, state, model,...
                                                      dt, ...
                                                      drivingForces,...
                                                      varargin{:});
            
        end
       
        function [fn, index] = getVariableField(model, name)
            % Get the index/name mapping for the model (such as where
            % pressure or water saturation is located in state)
            switch(lower(name))
                case {'x'}
                    fn = 'x';
                    index = 1;                
                otherwise
                    % This will throw an error for us
                    [fn, index] = getVariableField@ReservoirModel(model, name);                    
            end
        end
        
        function [state, report] = updateState(model, state, problem, dx, drivingForces)
            % Parent class handles almost everything for us
            [state, report] = updateState@ReservoirModel(model, state, problem, dx, drivingForces);
            
            % Update wells based on black oil specific properties
            saturations = model.saturationVarNames;
            wi = strcmpi(saturations, 'sw');
            oi = strcmpi(saturations, 'so');
            gi = strcmpi(saturations, 'sg');

            W = drivingForces.W;
            state.wellSol = assignWellValuesFromControl(model, state.wellSol, W, wi, oi, gi);
            % add extra model states things from mechanics
            state = addDerivedQuantities(model,state);
        end
        function state = addDerivedQuantities(model,state)
           error('Virtuel'); 
        end
      
    end
end
