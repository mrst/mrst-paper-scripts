function el_bc  = setMechBC(G, types, funs)
%
% Construct complete elastic boundary condition structure for use with
% VEM_linElast to setup discretized linear elasticity system.
% 
% SYNOPSIS:
%   function el_bc  = setMechBC(G, types, funs)
%
% DESCRIPTION:
%
% PARAMETERS:
%   G     - grid
%   types - 2N x N cell structure (where N is the dimensionality of grid G).
%           Entry (i, j) specify the boundary condition type on side i
%           along direction j.  Possible types are 'D' (Dirichlet, fixed
%           displacement) and 'N' (Neumann, fixed force).
%   funs  - 2N x N cell structure (where N is the dimensionality of grid G).
%           Entry (i, j) specify the value of the boundary condition on side
%           i along direction j.  The entry can either be a numeric value, or
%           a function taking a list of coordinates and returning the
%           corresponding values.
%
% RETURNS:
%   el_bc - Boundary condition structure that can be used with VEM_linElast.
%
% EXAMPLE:
%
% SEE ALSO:
% VEM_linElast
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
    

   [n, f, u, m, t] = deal([]);
   
   for side = 1:(2*G.griddim)
      [n, f, u, m, t] = ...
          bc_conditions(side, G, types(side,:), funs(side,:), n, f, u, m, t);          
   end
   
   el_bc = struct('disp_bc' , struct('nodes', n, 'uu', u, 'mask', m), ...
                  'force_bc', struct('faces', f', 'force', t));
end

% ----------------------------------------------------------------------------

function [n f u m t] = bc_conditions(side, G, isDirichlet, valfuns, n, f, u, m, t)
   
   % Replacing user shorthands with actual functions
   valfuns     = cellfun(@replace_shorthand, valfuns,     'UniformOutput', false);
   isDirichlet = cellfun(@replace_shorthand, isDirichlet, 'UniformOutput', false);

   % Extracting relevant nodes and faces
   sides = [0 0 0 0 0 0]; sides(side) = 1;
   [faces, nodes] = boundaryFacesAndNodes(G, sides);
   
   % remove already-specified nodes (i.e. corner nodes)
   %nodes = setdiff(nodes, n);
   
   % Determining displacement values, and mapping which nodes
   % have Dirichlet-type coordinate(s) 
   ncoord = G.nodes.coords(nodes, :);
   if G.griddim == 2
      disp   = [valfuns{1}(ncoord),      valfuns{2}(ncoord)];
      mask   = [isDirichlet{1}(ncoord) , isDirichlet{2}(ncoord)];
   else
      disp   = [valfuns{1}(ncoord),      valfuns{2}(ncoord), valfuns{3}(ncoord)];
      mask   = [isDirichlet{1}(ncoord) , isDirichlet{2}(ncoord), isDirichlet{3}(ncoord)];
   end      

   % adding nodes with at least one Dirichlet condition to output vectors
   has_dirichlet = any(mask, 2);

   n = [n; nodes(has_dirichlet)   ];
   m = [m; mask(has_dirichlet,  :)];
   u = [u; disp(has_dirichlet,  :)];
   
   % Identifying Neumann faces and retaining only those
   fcoord = G.faces.centroids(faces, :);
   if G.griddim == 2
      isNeumann = ~(isDirichlet{1}(fcoord) | isDirichlet{2}(fcoord));
   else
      isNeumann = ~(isDirichlet{1}(fcoord) | isDirichlet{2}(fcoord) | isDirichlet{3}(fcoord));   
   end
   
   % Retaining only Neumann faces, and computing traction values
   faces  = faces(isNeumann);
   fcoord = fcoord(isNeumann, :);
   
   if G.griddim == 2
      trac = [valfuns{1}(fcoord), valfuns{2}(fcoord)];
   else
      trac = [valfuns{1}(fcoord), valfuns{2}(fcoord), valfuns{3}(fcoord)];
   end
   
   f = [f; faces];
   t = [t; trac];
   
end

% ----------------------------------------------------------------------------

function fn = replace_shorthand(fn)

   % If 'fn' is a shorthand, replace it with the function it is a shorthand
   % for.  Otherwise (if it is already a function), return it as it is.
   if ~isa(fn, 'function_handle')
      if isa(fn, 'double')
         % we will create a constant function to be used for boundary conditions
         val = fn;
         fn = @(coords) repmat(val, size(coords,1), 1); % return function constant equal to 'val'
      elseif strcmpi(fn, 'D')
         % return Dirichlet indicator function, always true
         fn = @(coords) true(size(coords, 1), 1);
      elseif strcmpi(fn, 'N')
         % return Dirichlet indicator function, always false
         fn = @(coords) false(size(coords, 1), 1);
      else
         error('Illegal value for ''fn''');
      end
   end
end   
