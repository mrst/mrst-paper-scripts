This directory contains the source coded used to generate the results published
in the paper:

Andersen, O., Nilsen, H.M., Gasda, S.E., "Vertical Equilibrium Flow Models with
Fully Coupled Geomechanics for CO2 Storage Modeling, Using Precomputed Response
Functions", Energy Procedia
(2017). https://doi.org/10.1016/j.egypro.2017.03.1440

Note that this code is not actively maintained, but has been tested to run with
MRST version 2016a.

In order to run, the code should be added as a module to MRST, by placing it in
a proper subdirectory under `MRST/modules` and running `mrstModule add
precomp-mech-experimental` after having started MRST in MATLAB.  Also, in order
for the included examples to run, the small initialization script `mystartup.m`
needs to be executed first (will ensure other necessary modules are loaded).
