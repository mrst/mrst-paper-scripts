function example_2D(varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   opt.ppr_tol = 1e-2;
   opt = merge_options(opt, varargin{:});
   
   %% Illustrate error and show impact of nonlocality on a simple 2D injection example

   xres = 31;

   [model, initState, schedule] = setupSimpleTestCase(...
       'dim'       , 2               , ...
       'bc_l'      , {'N', 'N', 'N'}  , ...
       'bc_r'      , {'N', 'N', 'N'}  , ...
       'bc_fr'     , {'N', 'N', 'N'}  , ...  
       'bc_ba'     , {'N', 'N', 'N'}  , ...
       'bc_t'      , {'N', 'N', 'N'}  , ...
       'bc_b'      , {'D', 'D', 'D'}  , ...
       'well_val'  , 10*barsa         , ...
       'xres'      , xres             , ...
       'length'    , 5 * kilo * meter , ...
       'zres'      , [10 5 10]        , ... % [10 5 10]
       'T'         , 50 * day         , ...
       'tsteps'    , 50               , ...
       'thickness' , [1000 100 800]); %@@ Fix to support addWell properly!

   [model, istate, schedule, res] = ...
       compare_solvers(struct('model', model         , ...
                              'initState', initState , ...
                              'schedule', schedule)  , ...
                       'approaches', {'full'         , ...
                       'local ppr'    , ...
                       'full ppr'}    , ...
                       'ppr_cutoff', opt.ppr_tol, ...
                       'compute_mech', true);

   %% Plot comparisons
   plot_comparisons(res, istate, model, 1, '1 day', xres, 'diff_with_prev', false); %true);
   plot_comparisons(res, istate, model, 5, '5 days', xres, 'diff_with_prev', false);% true);
   plot_comparisons(res, istate, model, 50, '50 days', xres, 'diff_with_prev', false);%true);
   
   %% Plot colorfield
   % (NB: note in particular the errors in stress)
   %keyboard;
   % Plot results from fully coupled simulation
   visualizeResult2D(model.GMech, res(1).states, istate);

   % Plot error from local model
   visualizeResult2D(model.GMech, states_diff(res(1).states, res(2).states), ...
                     istate);

   % Plot error from ppr model
   visualizeResult2D(model.GMech, states_diff(res(1).states, res(3).states), istate);

end
% ----------------------------------------------------------------------------

function plot_comparisons(res, istate, model, step, label, xres, varargin)
   
   opt = merge_options(struct('diff_with_prev', false), varargin{:});
      
   p_full = res(1).states{step}.pressure(model.Gmap.gc);
   p_lppr = res(2).states{step}.pressure(model.Gmap.gc);
   p_fppr = res(3).states{step}.pressure(model.Gmap.gc);

   use_prev = opt.diff_with_prev && (step > 1);
   prev_step = if_else(step > 1, step-1, 1);
   
   pv_full = pv_vol_change(res(1).states{step}, if_else(use_prev, res(1).states{prev_step}, istate), model);
   pv_lppr = pv_vol_change(res(2).states{step}, if_else(use_prev, res(2).states{prev_step}, istate), model, ...
                           'ppr_model', res(2).extra);
   pv_fppr = pv_vol_change(res(3).states{step}, if_else(use_prev, res(3).states{prev_step}, istate), model, ...
                           'ppr_model', res(3).extra);

   %% Plot comparison in pressure profile between fully coupled, local and full ppr

   figure;
   subplot(1,2,1);
   xvals = linspace(-2.5, 2.5, xres);
   plot(xvals,    p_full(1:xres)/1e6, 'ko', 'linewidth', 1); hold on;
   plot(xvals, p_lppr(1:xres)/1e6, 'b', 'linewidth', 1);
   plot(xvals, p_fppr(1:xres)/1e6, 'r', 'linewidth', 1);
   xlabel('km'); ylabel('MPa');
   set(gca, 'fontsize', 13);
   legend('fully coupled', 'local', 'PR');
   title(['Pressure, ', label]);
   axis tight;

   subplot(1,2,2);
   xvals = linspace(-2.5, 2.5, xres);
   plot(xvals, pv_full(1:xres), 'ko', 'linewidth', 1); hold on;
   plot(xvals, pv_lppr(1:xres), 'b', 'linewidth', 1);
   plot(xvals, pv_fppr(1:xres), 'r', 'linewidth', 1);
   xlabel('km'); ylabel('Pore volume change / volume');
   set(gca, 'fontsize', 13);
   legend('fully coupled', 'local', 'PR');
   title(['Pore vol. change, ', label]);
   set(gcf, 'position', [684 192 1140 474]);
   axis tight;

   % We note that the local solution misses out on the local 'pressure
   % undershoot' effect.
   pspan = max(   p_full(1:xres)) - min(   p_full(1:xres));
   l_err_inf = max(abs(p_lppr(1:xres) -    p_full(1:xres)));
   f_err_inf = max(abs(p_fppr(1:xres) -    p_full(1:xres)));
   l_err_l2  = sqrt(sum((p_lppr(1:xres) -    p_full(1:xres)).^2)/xres); % @@ divide here?
   f_err_l2  = sqrt(sum((p_fppr(1:xres) -    p_full(1:xres)).^2)/xres);

   fprintf(['Plotting info for timestep: ', label]);
   fprintf('Total pressure variation: %4.2f MPa\n', pspan/mega);
   fprintf('Inf-error using local model: %4.2f Pa, or %4.2f percent.\n', l_err_inf, ...
           l_err_inf/pspan * 100);
   fprintf('Inf-error using ppr model: %4.2f Pa, or %4.2f percent.\n', f_err_inf, ...
           f_err_inf/pspan * 100);
   fprintf('l2-error using local model: %4.2f Pa, or %4.2f percent.\n', l_err_l2, ...
           l_err_l2/pspan * 100);
   fprintf('l2-error using ppr model: %4.2f Pa, or %4.2f percent.\n', f_err_l2, ...
           f_err_l2/pspan * 100);
end

% ----------------------------------------------------------------------------
function res = if_else(cond, yes, no)

   if cond
      res = yes;
   else
      res = no;
   end
end
