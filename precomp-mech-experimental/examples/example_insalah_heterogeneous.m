function example_insalah_heterogeneous()
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}

   xres  = 31; % 21
   aq_res = 3;
   zres = [3 4 aq_res, 7];
   perm_mean = 13 * milli * darcy;
   
   % Spelling out original E-field
   E = zeros(xres, xres, sum(zres));
   E(:,:,1:zres(1)) = 1.5 * giga * Pascal;
   E(:,:,(zres(1)+1):sum(zres(1:2))) = 20 * giga * Pascal;
   E(:,:,(sum(zres(1:2))+1):sum(zres(1:3))) = 6 * giga * Pascal;
   E(:,:,(sum(zres(1:3))+1):sum(zres(1:4))) = 20 * giga * Pascal;      
   
   % adding 40% random variation to aquifer layer
   Emult = gaussianField([xres, xres, 1], [0.7, 1.3], [7 7 7], 100);
   Emult = reshape(repmat(Emult(:), aq_res, 1), [xres, xres, aq_res]);
   E(:,:,(sum(zres(1:2))+1):sum(zres(1:3))) = ...
       E(:,:,(sum(zres(1:2))+1):sum(zres(1:3))) .* Emult;

   
   
   perm = heterogeneous_field(xres, [zres(1)+zres(2), zres(3), zres(4)], perm_mean);
   
   % ensure decent permeability for wellcells
   
   wcells = [481 1442 2403]; % for xres=31 case
   %wcells = [221 662 1103]; % for xres =21 case
   perm(wcells) = perm_mean;
   
   save('heterogeneous_stuff', 'perm', 'E');
   
   example_insalah('xres', xres, 'E', E, 'perm', perm);
end


function [perm, poro] = heterogeneous_field(xres, layer_res, meanval)

   %%  uniform porosity
   poro = ones(xres, xres, sum(layer_res)) * 0.1;
   
   %% Quasi-gaussian log-normal permeability
   ex = gaussianField([xres, xres, 1], ...
                      [-4 -1], [5 5 5], 50);
   perm = poro;

   
   ex = reshape(repmat(ex(:), layer_res(2), 1), [xres, xres, layer_res(2)]);
   
   tmpfield = (10.^ex) * darcy * 2;
   tmpfield = tmpfield ./ mean(tmpfield(:)) * meanval;

   
   perm(1:xres, 1:xres, (1:layer_res(2))+layer_res(1)) = tmpfield;


end
