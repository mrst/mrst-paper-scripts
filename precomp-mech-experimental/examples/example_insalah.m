function example_insalah(varargin)
%{
Copyright 2020-2023 SINTEF Digital, Mathematics & Cybernetics.

This file is part of The MATLAB Reservoir Simulation Toolbox (MRST).

MRST is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MRST is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MRST.  If not, see <http://www.gnu.org/licenses/>.
%}
   opt.scenario_savefile = 'insalah_scenario.mat';
   opt.results_savefile = 'insalah_results.mat';
   opt.ppr_savefile = 'ppr_insalah_e-03.mat';
   opt.VE_results_savefile = 'insalah_results_VE.mat';
   opt.E = [1.5, 20, 6, 20] * giga * Pascal;
   opt.perm = [1e-17, 1e-21, 1.3e-14, 1e-19];
   opt.force_recalc = false;
   opt.wellrate = 0.02;
   opt.xres = 31;%21;%31;
   opt.aq_res = 3;
   opt.alternative_scenario = false; % if true, run short-term, alernating
                                     % injection scenario instead of original
                                     % long-term scenario
   opt.brine_mu = 3.2e-4; % corresponds to 90 degrees and 18 MPa
   
   opt = merge_options(opt, varargin{:});
   
%% Setup InSalah test model 

% "Standard conditions" : 1 bar, 15.6 deg. C
% - CO2 density at these conditions: 1.84 kg/m3
% "Aquifer conditions" : 17.9 MPa, 90 deg. C
% - CO2 density at these conditions: 472.4 kg/m3

% Injection rate: 15 MMscf per day = 424750 m3 (4.92 m3/sec)
% This represents a mass of 781540.0 kg CO2 (i.e. 0.28 MT/year)
% The volume in the aquifer of this mass would be 1654 m3 (daily)
% If we inject a similar volume of water daily (assuming almost
% incompressible), the rate would be 0.019 m3/sec
% This seems very low...

% Instead, assume an injection of 0.75 MT/year
% This amounts to 2000000 kg/day
% This amounts to an aquifer volume of 4234 m3/day
% Per second, this is a volume of 0.05 m3/sec

   H = 20; % aquifer thickness
   %L = 10 * kilo * meter;
   L = 4 * kilo * meter;
   ilu_tol = 1e-3;
   %tsteps = [day, 30 * day, 0.5 * year, 1 * year, 2 * year, 3 * year];
   tsteps = [day, cumsum(year/12 * ones(1, 12*3))];
   %tsteps = [day, cumsum(year/12 * ones(1, 12*1))];
   
   if opt.force_recalc
      if exist(opt.scenario_savefile)==2
         delete(opt.scenario_savefile);
      end
      if exist(opt.results_savefile) == 2
         delete(opt.results_savefile);
      end
      if exist(opt.ppr_savefile) == 2
         delete(opt.ppr_savefile)
      end
      if exist(opt.VE_results_savefile) == 2
         delete(opt.VE_results_savefile)
      end
   end
   
   %tsteps=6;
   if exist(opt.scenario_savefile) == 2
      load(opt.scenario_savefile);
   else
      
      % Explicit definition of the 'perm' and 'E' fields below allows manual
      % modifications before creating the model, if desired.

      % E = zeros(opt.xres, opt.xres, 17);
      % E(:,:,1:3) = 1.5 * giga * Pascal;
      % E(:,:,4:7) = 20 * giga * Pascal;
      % E(:,:,8:10) = 6 * giga * Pascal;
      % E(:,:,11:17) = 20 * giga * Pascal;
      
      % perm = 0 * E + 1.3e-14;
      
      [model, initState, schedule] = ...
          setupSimpleTestCase('dim'       , 3                                   , ...
                              'length'    , L                                   , ...
                              'thickness' , [900, 1800-900, H, 4000 - 1800 - H] , ...
                              'xres'      , opt.xres                            , ...
                              'ilu_tol'   , ilu_tol                             , ...
                              'zres'      , [3, 4, opt.aq_res, 7]               , ...
                              'E'         , opt.E(:),... %[1.5, 20, 6, 20] * giga * Pascal    , ...
                              'nu'        , [0.2, 0.15, 0.2, 0.15]              , ...
                              'poro'      , [0.1, 0.01, 0.17, 0.01]             , ...
                              'perm'      , opt.perm(:), ...%[1e-17, 1e-21, 1.3e-14, 1e-19]      , ...
                              'convert_to_undrained_in_noflow', true, ...%false   , ...
                              'alpha'     , [1, 1, 1, 1]                        , ...
                              'bc_flow'   , {'D', 'D', 'D', 'D', 'N', 'N'}      , ...
                              'bc_l'      , {'N', 'N', 'N'}                     , ...
                              'bc_r'      , {'N', 'N', 'N'}                     , ...
                              'bc_ba'     , {'N', 'N', 'N'}                     , ...
                              'bc_fr'     , {'N', 'N', 'N'}                     , ...
                              'well_type' , {'rate'}                            , ...
                              'well_val'  , opt.wellrate                        , ... 
                              'well_rad'  , 0.05                                , ...
                              'mu_water'  , opt.brine_mu                        , ...
                              'T'         , 3 * year                            , ...
                              'tsteps'    , tsteps);
      
      if opt.alternative_scenario
         % make alternative, short-time schedule
         tsteps = ones(18, 1) * 5 * day; % period of 90 days, divided by 5 day intervals
         schedule.control = [schedule.control(1), schedule.control(1)];
         schedule.control(2).W.val = 0; % shut off well
         schedule.step.val = tsteps;
         schedule.step.control = [repmat([1 1 2 2], 1, 4), [1 1]];
      end

      scenario.model = model;
      scenario.initState = initState;
      scenario.schedule = schedule;
      save(opt.scenario_savefile, 'scenario');
   end
   
   %% Simulating water injection
   
   if exist(opt.results_savefile) == 2 
      load(opt.results_savefile)
   else
      [model, istate, schedule, res] = ...
          compare_solvers(scenario, ...
                          'approaches', {'fixed stress', ... %'full', 'fixed stress', ...
                          'full ppr', ...
                          'local ppr'}, ...
                          'ppr_cutoff', 1e-3, ...
                          'compute_mech', true, ...
                          'ppr_savefile', opt.ppr_savefile, ...
                          'ppr_itersolve', true);
      res(2).extra = [];
      res(3).extra = [];
      save(opt.results_savefile, 'istate', 'schedule', 'res', '-v7.3');
   end
   
   column_labels = {'fully coupled', 'PR model (error)', 'local model (error)'};
   %keyboard;
   visualize_result(scenario.model, istate, res, column_labels, scenario.schedule);
   
   %% Simulating CO2 injection
   if exist(opt.VE_results_savefile)
      load(opt.VE_results_savefile)
   else
      [co2_res_full, co2_res_loc] = compute_co2_injection(scenario, opt.ppr_savefile);
      save(opt.VE_results_savefile, 'co2_res_full', 'co2_res_loc');
   end
   column_labels = {'PR model', 'local model (error)'};
   %keyboard;
   visualize_result(scenario.model, istate, [co2_res_full, co2_res_loc], column_labels, scenario.schedule)
end

% ----------------------------------------------------------------------------

function carray = build_fields(res, fname, t_ixs, space_ixs, subtr, varargin)

   opt.second_ix = 3; % only used in the case of arrays with two indices
   opt.fun = [];
   opt = merge_options(opt, varargin{:});
   
   carray = cell(numel(res) * numel(t_ixs) * size(space_ixs, 2), 1);
   count = 1;
   for s_ix = space_ixs
      for t_ix = t_ixs(:)'
         for r = 1:numel(res)
            field = res(r).states{t_ix}.([fname]);
            if size(field, 2) > 1
               field = field(s_ix, opt.second_ix);
            else
               field = field(s_ix);
            end
            if ~isempty(subtr)
               if size(subtr.([fname]), 2) > 1 % multi-index
                  field = field - subtr.([fname])(s_ix, opt.second_ix);
               else
                  field = field - subtr.([fname])(s_ix);
               end
            end
            if ~isempty(opt.fun) % transformation function
               field = opt.fun(field);
            end
            carray{count} = field;
            count = count+1;
         end
      end
   end
end


% ----------------------------------------------------------------------------

function visualize_result(model, istate, res, column_labels, schedule)
% Designed to visualize key information for the long-term scenario
   
   cnum = prod(model.G.cartDims(1:2));
   nnum = (sqrt(cnum)+1)^2;
   sc = (1:cnum)'; % surface cells
   sn = (1:(nnum))';
   gc = model.Gmap.gc(1:cnum); % topmost layer of aquifer (cells)
   gn = model.Gmap.gn(1:nnum); % topmost layer of aquifer (nodes)
   cvol = model.G.cells.volumes;
   end_ix = numel(res(1).states);
   alternative = (sum(schedule.step.val) < year/2);

   tsteps = if_else(alternative, [1 3 end_ix], [1 2 end_ix]);
   row_labels = if_else(alternative, {'5 days', '15 days', '90 days'}, ...
                                     {'1 day', '1 month', '3 years'});
   
   
   % visualize pressure field
   
   plot_panel('Pressure field (aquifer top)'       , ...
              column_labels                        , ...
              row_labels                           , ...
              build_fields(res, 'pressure', tsteps, gc, []));

   % Plot max pressure field change as function of time
   pchange = @(state) state.pressure(gc) - istate.pressure(gc);
   if numel(res) == 3
      plot_max_timecurve({'full', 'PR model', 'local model'}, res, schedule, ...
                         pchange, 'Max overpressure', 'Pascal');
   else
      assert(numel(res) == 2);
      plot_max_timecurve({'PR model', 'local model'}, res, schedule, pchange, ...
                         'Max overpressure', 'Pascal');
   end
   
               
   % Visualize vertical displacement
   plot_panel('Vertical displacement', ...
              column_labels, ...
              {'Surface', 'Aquifer top'}, ...
              build_fields(res, 'uu', end_ix, [sn, gn], istate));
   
   % Visualize volume change
   fields = {};
   for t_ix = tsteps
      for res_ix = 1:numel(res)
         f = pv_vol_change(res(res_ix).states{t_ix}, istate, model);
         fields = [fields, {f}];
      end
   end
   
   plot_panel('Vertically integrated pore volume change', ...
              column_labels, row_labels, fields);

   % Plot max pore volume change as a function of time
   pvchange = @(state) pv_vol_change(state, istate, model);
   cur_labels = if_else(numel(res)==3, {'full', 'PR model', 'local model'}, ...
                                       {'PR model', 'local model'});
   plot_max_timecurve(cur_labels, res, schedule, ...
                      pvchange, 'Max pore volume change', ...
                      'fractional change');   
   % Visualize effective stress change, reservoir top
   fields = {};
   for stress_dir = 1:3
      for res_ix = 1:numel(res)
         f =  - (res(res_ix).states{end}.stress(gc, stress_dir) - istate.stress(gc,stress_dir));
         fields = [fields, {f}];
      end
   end
   
   plot_panel('Effective stress change (aquifer top)', ... 
              column_labels, {'\sigma_x', '\sigma_y', '\sigma_z'}, fields);

   % Visualize effective vertical stress change for three timesteps, reservoir top
   plot_panel('Effective vertical stress change (aquifer top)', ...
              column_labels, ...
              row_labels   , ...
              cellfun(@(x) -x,  build_fields(res, 'stress', tsteps, gc, istate), 'uniformoutput', false)); 
   
   
   % visualize saturation if present
   if numel(res) == 2
      assert(isfield(res(1).states{1}, 's'));
      plot_panel('CO2 saturation, caprock', column_labels, ...
                 row_labels, ...
                 build_fields(res, 's', tsteps, [1:numel(gc)]', [], 'second_ix', 2));
   end
      
end


% ----------------------------------------------------------------------------

function plot_max_timecurve(column_labels, res, schedule, transfun, plot_title, ...
                            yaxis_label)

   figure;
   max_time = sum(schedule.step.val);
   time_unit  = if_else(max_time > (year/2), year/12, day);
   time_label = if_else(time_unit == day, 'days', 'months');
   time_axis = cumsum(schedule.step.val) / time_unit;
   
   % months = cumsum(schedule.step.val) / (year/12); 
   curves = nan(numel(res), numel(time_axis));
   
   for i = 1:numel(res)
      for j = 1:numel(time_axis)
         %change = pv_vol_change(res(i).states{j}, istate, model);
         change = transfun(res(i).states{j});
         curves(i, j) = max(change(:));
      end
   end
   if numel(res) == 2
      plot(time_axis, curves);
   else
      plot(time_axis, curves(1,:), '-o', ...
           time_axis, curves(2,:), '-', ...
           time_axis, curves(3,:), '-');
   end
   
   legend(column_labels{:});
   title(plot_title);
   %title('Max pore volume change');
   set(gca, 'fontsize', 13);
   xlabel(time_label)
   %ylabel('fractional change');
   ylabel(yaxis_label);
   set(gca, 'fontsize', 14);
   set(gcf, 'color', 'white');
end

% ----------------------------------------------------------------------------
function plot_panel(plottitle, colnames, rownames, fields)
   
   colnum = numel(colnames);
   rownum = numel(rownames);
   xres = floor(sqrt(numel(fields{1})));
   assert(xres * xres == numel(fields{1}));
   
   figure;
   for i = 1:(colnum*rownum)
      row = 1 + floor((i-1)/colnum);
      col = i - (row-1) * colnum;
      subplot(rownum, colnum, i);
      data = reshape(fields{i}, xres, xres);
      if col > 1
         ref_field = fields{i - (col-1)};
         data = data - reshape(ref_field, xres, xres);
         data = data / (max(ref_field(:)) - min(ref_field(:)));
      end
      surf(data, 'edgecolor', 'none'); view(0, 90); colorbar; axis tight; 
      set(gca, 'ytick', []);
      set(gca, 'xtick', []);
      set(gca, 'fontsize', 13);
      if row == 1
         title(colnames{col}, 'fontsize', 13);
      end
      if col==1
         ylabel(rownames{row});
         set(get(gca, 'ylabel'), 'fontweight', 'bold');
         set(get(gca, 'ylabel'), 'fontsize', 13);
      end
   end
   sgtitle(plottitle, 'fontweight', 'bold', 'fontsize', 15);
   pos = get(gcf,'position');
   set(gcf, 'position', [pos(1:2), 1100, 780]);
   set(gcf, 'color', 'white');
end

% ----------------------------------------------------------------------------

function [res1, res2] = compute_co2_injection(scenario, ppr_savefile)
   
%% Simulating CO2 injection
   ppr_model = SimplePrecompModel(scenario.model, ...
                                  scenario.schedule.control(1).bc, ...
                                  scenario.initState.pressure, ...
                                  'nonlinearTolerance', 1e-13, ...
                                  'cutoff', 1e-3, ...
                                  'savefile', ppr_savefile, ...
                                  'response_type', 'full');
   
   ppr_model_loc = SimplePrecompModel(scenario.model, ...
                                      scenario.schedule.control(1).bc, ...
                                      scenario.initState.pressure, ...
                                      'nonlinearTolerance', 1e-13, ...
                                      'response_type', 'local');
   
   Gt            = topSurfaceGrid(ppr_model.G);
   xres          = ppr_model.G.cartDims(1);
   mu_brine      = scenario.model.fluid.muW(1); % assumed constant
   cnum          = xres^2;
   caprock_p     = scenario.initState.pressure(1:cnum);
   caprock_p     = caprock_p - norm(gravity) * ...
       (scenario.model.fluid.rhoWS .* ...
        scenario.model.fluid.bW(caprock_p) ...
        .* (Gt.cells.H / 2));
   
   initStateVE   = struct('pressure', caprock_p, ...
                          's', [ones(cnum, 1), zeros(cnum, 1)], ...
                          'sGmax', zeros(cnum, 1));
   rock2D        = averageRock(ppr_model.rock, Gt);
   
   VEfluid       = makeVEFluid(Gt, rock2D, 'sharp interface', ...
                               'fixedT', 273.15 + 90, ...
                               'residual', [0.3, 0.05], ...
                               'wat_mu_ref', mu_brine);
   VEfluid       = rmfield(VEfluid, 'pvMultR'); % in any case, we will use that from the rock object

   VEbc = pside(...
       pside(...
           pside(...                    
               pside([], Gt, 'xmin', caprock_p(1), 'sat', [1 0]), ...
               Gt, 'xmax', caprock_p(1), 'sat', [1 0]), ...
           Gt, 'ymin', caprock_p(1), 'sat', [1 0]), ...
       Gt, 'ymax', caprock_p(1), 'sat', [1 0]);
   
   for i = 1:numel(scenario.schedule.control)
      VEWell = convertwellsVE(scenario.schedule.control(i).W, scenario.model.G, Gt, rock2D);
      VEWell.compi  = [0 1];
      VEWell.val    = if_else(i==1, 781540.0 / VEfluid.rhoGS / 24 / 60 / 60, 0);
      VEControl(i) = struct('W', VEWell, 'bc', VEbc);
   end
   
   VEschedule    = struct('control', VEControl, 'step', scenario.schedule.step);
   
   strain_dep_perm = false;
   VEmodel_full = CO2VEPrecompMechModel(ppr_model, VEfluid, strain_dep_perm);
   VEmodel_loc  = CO2VEPrecompMechModel(ppr_model_loc, VEfluid, strain_dep_perm);
   
   solver = IterativeLinearSolver('solvertype', @bicgstab, ...
                                  'tolerance', 1e-6, ...
                                  'is_sticky', false, ...
                                  'precond', 'ilutp');
   
   tic; [wellSolsVE, statesVE]         = simulateScheduleAD(initStateVE, VEmodel_full,VEschedule, 'LinearSolver', solver); toc
   tic; [wellSolsVE_loc, statesVE_loc] = simulateScheduleAD(initStateVE, VEmodel_loc, VEschedule, 'LinearSolver', solver); toc
   
   states1 = convert_to_3D(statesVE,     Gt, VEfluid, scenario, ppr_model.fbc);
   states2 = convert_to_3D(statesVE_loc, Gt, VEfluid, scenario, ppr_model.fbc);   

   res1 = struct('wellsols', {wellSolsVE}, 'states', {states1});
   res2 = struct('wellsols', {wellSolsVE_loc}, 'states', {states2});
   
end

% ----------------------------------------------------------------------------
function states = convert_to_3D(statesVE, Gt, VEfluid, scenario, fbc)
   
   GMech     = scenario.model.GMech;
   Gmap      = scenario.model.Gmap;
   initState = scenario.initState;
   states    = cell(numel(statesVE), 1);
   A         = scenario.model.operators.mech.A;
   rhs0      = scenario.model.operators.mech.rhs;
   gradP     = scenario.model.operators.mech.gradP;
   L         = scenario.model.operators.extra.precond;
   x0        = initState.x;
   tol       = 10e-13;
   alpha     = scenario.model.rock.alpha;
   
   for i = 1:numel(statesVE)
      
      % computing saturation
      
      sol.h = statesVE{i}.s(:,2) .* Gt.cells.H;
      sol.h_max = sol.h; % @@ we assume they are the same, as no migration has taken place!
      sat = height2Sat(sol, Gt, struct('sr', VEfluid.res_gas, 'sw', VEfluid.res_water));
      
      states{i}.s = [1-sat, sat];
      
      % computing "effective" pressure (weighted average of CO2 and water
      % pressure, which is what is needed to compute impact on mechanics)
      
      dh = bsxfun(@minus, ...
                  reshape(Gt.parent.cells.centroids(:,3), Gt.cells.num, []), Gt.cells.z);
      rgh_wat = bsxfun(@times, VEfluid.rhoW(statesVE{i}.pressure) * norm(gravity), dh);
      p_wat = bsxfun(@plus, rgh_wat, statesVE{i}.pressure);
      
      drho = VEfluid.rhoW(statesVE{i}.pressure) - VEfluid.rhoG(statesVE{i}.pressure);
      p_co2_top = statesVE{i}.pressure + (drho * norm(gravity) .* sol.h);
      
      rgh_co2 = bsxfun(@times, VEfluid.rhoG(p_co2_top) * norm(gravity), dh);
      p_co2 = bsxfun(@plus, rgh_co2, p_co2_top);
      
      states{i}.pressure = states{i}.s(:,1) .* p_wat(:) + states{i}.s(:,2) .* p_co2(:);
      
      % Add mechanics
      rhs = rhs0 + gradP * (alpha .* states{i}.pressure) - fbc;
      states{i}.x = itersolve_JP(A, rhs, 'solver', @pcg, 'M1', L, 'M2', L', ...
                                 'x0', x0, 'tol', tol);
      states{i} = scenario.model.addDerivedQuantities(states{i});

      states{i} = extendFieldOutsideAquifer(states{i}, 'pressure', GMech, Gmap);
      %states{i} = extendFieldOutsideAquifer(states{i}, 'vdiv', GMech, Gmap);
   end
end

% ----------------------------------------------------------------------------
function res = if_else(cond, yes, no)
   if cond
      res = yes;
   else
      res = no;
   end
end
