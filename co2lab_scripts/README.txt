This directory contains files used to generate figures for specific published
research papers using the co2lab module in MRST.

They are not actively maintained but have been tested to run with mrst version
mrst2020a.


Subdirectories correspond to the following research papers or thesis:

CAGEO-75/   : Spill Point Analysis and Structural Trapping Capacity in Saline
            Aquifers Using MRST-co2lab

COMG-1/   : Robust Simulation of Sharp-Interface Models for Fast Estimation of
            CO2 Trapping Capacity

COMG-2/   : Fully-Implicit Simulation of Vertical-Equilibrium Models with
            Hysteresis and Capillary Fringe 

CAGEO-79/ : Analysis of CO2 Trapping Capacities and Long-Term Migration for
            Geological Formations in the Norwegian North Sea Using MRST-co2lab (*)

EP-2017/  : Using sensitivities and vertical-equilibrium models for
            parameter estimation of CO2 injection models with application
            to Sleipner data

IJGGC-75/ : Using simplified methods to explore the impact of parameter
            uncertainty on CO2 storage estimates with application to the
            Norwegian Continental Shelf

DISSERTATION2017/ : Simplified models for numerical simulation of geological
                    CO2 storage (O. Andersen, 2017)



The directories contain MATLAB code that can be used with co2lab to generate the
examples within. 

In some of the paper directories, there is a Matlab script called
'runAllExamples_paperX'. This script documents the exact calls to other scripts
and functions used to generate the diagrams/graphs of each specific figure in
that paper. The script is thus not intended to be run through from beginning to
end, but to document how each specific figure is generated.
